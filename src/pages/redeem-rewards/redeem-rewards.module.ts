import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RedeemRewardsPage } from './redeem-rewards';

@NgModule({
  declarations: [
    RedeemRewardsPage,
  ],
  imports: [
    IonicPageModule.forChild(RedeemRewardsPage),
  ],
})
export class RedeemRewardsPageModule {}
