import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { TermsandconditionsPage } from '../termsandconditions/termsandconditions'; 

/**
 * Generated class for the LanguagedropshowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-languagedropshow',
  templateUrl: 'languagedropshow.html',
})
export class LanguagedropshowPage { 

  abbrname
  name 

  items=[];
  Permanentitems=[];
  LangSrc:boolean=false;

  default_lang
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,public security:SecurityProvider, public toastController : ToastController) {
      this.items=this.navParams.get("languages"); 
     //this.items.push({id: 1, short_name: "en", lang_name: "English", status: 1, created_date: null, updated_date: null});  
  //   this.security.GetDefaultLang().subscribe(res =>{
  //     if(res.status==200) { 
  //       if(res.success) {
  //             this.items=res.data;
  //             this.Permanentitems=res.data;
  //             return;
  //       }
  //      }
  //      else {  
  //         this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();
  //         return;
  //     }
  // }, err => { 
  //     console.error("err==",err); 
  // });
  }

  SearchBtn() { 
    this.LangSrc=!this.LangSrc;
  }

  getItems(ev: any) {
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.lang_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {   
      this.items=this.Permanentitems;
    }
  }

  savebtn(index) {
    //let data = { status: true,getArr:this.items[index] };
    //this.viewCtrl.dismiss(data);
    this.default_lang=this.items[index].short_name
  }

  NextBtn() { 
    if(this.default_lang=="" || this.default_lang==null )  { 
      this.toastController.create({ message: "Please select a Language", duration: 5000, position: 'bottom' }).present();
      return;
    } 
    this.security.LangUpdate(this.default_lang).subscribe(res =>{
      if(res.status==200) { 
        if(res.success) { 
          localStorage.setItem("username",res.data.display_name);      
          this.navCtrl.push(TermsandconditionsPage,{ terms_conditions : res.data.terms_conditions });
          return;
        }
       } 
       else {  
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();
          return;
      }
  }, err => { 
      console.error("err==",err); 
  });
  }

  closebtn() {
    let data = { status: false };
    this.viewCtrl.dismiss(data);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LanguagedropshowPage');
  }

}
