import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, ToastController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { LanguagedropshowPage } from '../languagedropshow/languagedropshow';
import { TermsandconditionsPage } from '../termsandconditions/termsandconditions'; 

/**
 * Generated class for the LanguagedropPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-languagedrop',
  templateUrl: 'languagedrop.html',
})
export class LanguagedropPage {  

  default_lang:any=null;
  default_langTxt:any=null;
  constructor(public navCtrl: NavController, public navParams: NavParams,public event:Events,public modalController:ModalController,public toastController:ToastController,public security:SecurityProvider) {

  }
  public changeLanguage() : void {
   this.event.publish('user:created',this.default_lang,Date.now());
   localStorage.setItem("applanguage",this.default_lang);
  }

  OpenModal() {
    const modal = this.modalController.create(LanguagedropshowPage,{ language: this.default_langTxt,langAbbr:this.default_lang });
    modal.present();
    modal.onDidDismiss(data => {
      if(data.status){
        this.default_lang=data.getArr.short_name
        this.default_langTxt=data.getArr.lang_name
        this.changeLanguage(); 
      }
    });  
  }

  SaveNext() {
    if(this.default_lang=="" || this.default_lang==null )  { 
      this.toastController.create({ message: "Please select a Language", duration: 5000, position: 'bottom' }).present();
      return;
    } 
    this.security.LangUpdate(this.default_lang).subscribe(res =>{
      if(res.status==200) { 
        if(res.success) { 
          this.navCtrl.push(TermsandconditionsPage);
          return;
        }
       } 
       else {  
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();
          return;
      }
  }, err => { 
      console.error("err==",err); 
  });
     
  }

  public ionViewDidLoad() : void {
    console.log('ionViewDidLoad LanguagedropPage');
 }



}
