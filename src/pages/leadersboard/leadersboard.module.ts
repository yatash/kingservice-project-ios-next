import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeadersboardPage } from './leadersboard';

@NgModule({
  declarations: [
    LeadersboardPage,
  ],
  imports: [
    IonicPageModule.forChild(LeadersboardPage),
  ],
})
export class LeadersboardPageModule {}
