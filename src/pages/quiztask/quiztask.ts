import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController, ViewController, Platform } from 'ionic-angular'; 
import { SecurityProvider } from '../../providers/security/security';
import { LoginPage } from '../login/login';
import { YouhaverecvdPage } from '../youhaverecvd/youhaverecvd';
import { Moduledetails1Page } from '../moduledetails1/moduledetails1';
import { LearningdevelopmentPage } from '../learningdevelopment/learningdevelopment';

/**
 * Generated class for the QuiztaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiztask',
  templateUrl: 'quiztask.html',
})
export class QuiztaskPage {

  total_quiz_count:any;
  course_quiz_details:any=[];
  CourseArr:any=[];
  PreviousIndex:any=0;

  course_id:any;
  course_quiz_id:any;
  course_quiz_option_id:any=null;
  optionsAns:number=0

  SelectContentArr:any;
  CurentQuestion:any;
  TotalQuestion:any;
  PreviousAnsIndex:any=null;

  is_right_option:boolean=false;
  subindex;
  quiz_type;

  QuizAnswer:boolean=false;

  buttonDisabled:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController : ToastController,public modalCtrl:ModalController, public viewCtrl:ViewController, public platform:Platform) {
      let backAction = platform.registerBackButtonAction(() => { 
      },1);
  }

  ionViewWillEnter() {
    this.course_id=this.navParams.get("course_id");
    this.SelectContentArr=this.navParams.get("SelectContentArr");
    if(this.course_id==undefined) {
      this.course_id="1";
    }
    this.GetData();
  }

  GetData() { 
    this.security.CourseQuiz(this.course_id).subscribe(res => { 
      if(res.status==200) {   
        if(res.success) { 
          this.total_quiz_count=res.data.total_quiz_count;
          this.course_quiz_details=res.data.course_quiz_details;
          this.CourseArr = this.course_quiz_details.filter((item,index) => {
              return index ==this.PreviousIndex;
          });
          var TotalPart=this.total_quiz_count.split("/");
          this.TotalQuestion=TotalPart[1];
          this.CurentQuestion = (100/this.TotalQuestion)*TotalPart[0]; 
          return; 
        }    
      } 
      else {   
        this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();  
        return; 
      }
    }, err => { 
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.setRoot(LoginPage);
        return; 
      }  
      if(err.status==422) {     
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      } 
    });
  }

  RowOptionBtn(course_quiz_option_id,is_right_option,subindex,quiz_type) {
    //if(!this.QuizAnswer) {
      this.course_quiz_option_id=course_quiz_option_id;
      this.is_right_option=is_right_option;
      this.subindex=subindex;
      this.quiz_type=quiz_type;
      this.optionsAns=0;
      if(this.quiz_type=='text') {
        if(this.PreviousAnsIndex !=null) {
          document.getElementById('btnans_'+this.PreviousAnsIndex).setAttribute("class", "quiz");
          document.getElementById('txtans_'+this.PreviousAnsIndex).setAttribute("class", "txtspan");
        } 
        this.PreviousAnsIndex=this.subindex;
        document.getElementById('btnans_'+this.subindex).setAttribute("class", "btnSelectQuiz");
        document.getElementById('txtans_'+this.subindex).setAttribute("class", "txtSelectQuiz");
      } 
      if(this.quiz_type=='image') {
        if(this.PreviousAnsIndex !=null) {
          document.getElementById('cardimg_'+this.PreviousAnsIndex).setAttribute("class", "cardimage");
        }
        this.PreviousAnsIndex=this.subindex;
        document.getElementById('cardimg_'+this.subindex).setAttribute("class", "imgcardSelect");
      }
    //}
    //this.QuizAnswer=true;  
  }

  SubmitBtn() {
    if(this.course_quiz_option_id == undefined || this.course_quiz_option_id == null) {
      this.toastController.create({message:"please select the option",duration:5000,position:'bottom'}).present();  
      return; 
    } 
    //this.QuizAnswer=false;
    if(this.is_right_option) {
      if(this.quiz_type=='text') {
        if(this.PreviousAnsIndex !=null) {
          document.getElementById('btnans_'+this.PreviousAnsIndex).setAttribute("class", "quiz");
          document.getElementById('txtans_'+this.PreviousAnsIndex).setAttribute("class", "txtspan");
        } 
        this.PreviousAnsIndex=this.subindex;
        document.getElementById('btnans_'+this.subindex).setAttribute("class", "btnans");
        document.getElementById('txtans_'+this.subindex).setAttribute("class", "txtans");
      } 
      if(this.quiz_type=='image') {
        if(this.PreviousAnsIndex !=null) {
          document.getElementById('cardimg_'+this.PreviousAnsIndex).setAttribute("class", "cardimage");
        }
        this.PreviousAnsIndex=this.subindex;
        document.getElementById('cardimg_'+this.subindex).setAttribute("class", "imgright");
      }
      this.optionsAns=1; 
    }
    if(!this.is_right_option) {
      if(this.quiz_type=='text') {
        if(this.PreviousAnsIndex !=null) {
          document.getElementById('btnans_'+this.PreviousAnsIndex).setAttribute("class", "quiz");
          document.getElementById('txtans_'+this.PreviousAnsIndex).setAttribute("class", "txtspan"); 
        }
        this.PreviousAnsIndex=this.subindex;
        document.getElementById('btnans_'+this.subindex).setAttribute("class", "btnanswrong");
        document.getElementById('txtans_'+this.subindex).setAttribute("class", "txtanswrong");
      } 
      if(this.quiz_type=='image') {
        if(this.PreviousAnsIndex !=null) {
          document.getElementById('cardimg_'+this.PreviousAnsIndex).setAttribute("class", "cardimage");
        }
        this.PreviousAnsIndex=this.subindex;
        document.getElementById('cardimg_'+this.subindex).setAttribute("class", "imgwrong");
      } 
      this.optionsAns=2; 
    }
    if(this.optionsAns==1) {
      this.course_id=this.course_quiz_details[this.PreviousIndex].course_id;
      this.course_quiz_id=this.course_quiz_details[this.PreviousIndex].course_quiz_id;
      this.buttonDisabled=true;
      this.security.DoCourseQuiz(this.course_id,this.course_quiz_id,this.course_quiz_option_id).subscribe(res => { 
        if(res.status==200) {  
          if(res.success) {
            this.buttonDisabled=false;  
            this.PreviousAnsIndex=null;
            if(res.data.completed_course_flag) {    
      let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:this.SelectContentArr, points:res.data.points }); 
      modal.onDidDismiss((views) => {  
      if(this.SelectContentArr.course_type=="video") { 
        this.navCtrl.pop(); 
        this.navCtrl.push(LearningdevelopmentPage);    
      }
      if(this.SelectContentArr.course_type=="text") {   
      this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3)); 
      }      
      });  
      modal.present();
            }
            else {
              this.total_quiz_count=res.data.total_quiz_count;
              this.course_quiz_details=res.data.course_quiz_details;
              this.CourseArr = this.course_quiz_details.filter((item,index) => {
                  return index ==this.PreviousIndex;  
              });
              var TotalPart=this.total_quiz_count.split("/");
              this.TotalQuestion=TotalPart[1];
              this.CurentQuestion = (100/this.TotalQuestion)*TotalPart[0];
              this.course_quiz_option_id=null; 
              this.optionsAns=0;  
            }  
            return; 
          }    
        } 
        else {   
          this.toastController.create({message:res.data.message,duration:5000,position:'bottom'}).present(); 
          return; 
        }
      }, err => { 
        if(err.status==401) {    
          this.toastController.create({message:JSON.parse(err._body).data.message,duration:5000,position: 'bottom'}).present();
          localStorage.clear();    
          this.navCtrl.setRoot(LoginPage);
          return; 
        }  
        if(err.status==422) {     
          this.toastController.create({message:JSON.parse(err._body).data.message,duration:5000,position: 'bottom'}).present();  
          return; 
        } 
      });  
    } 
    
  }

  backBtn() {  
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuiztaskPage');
  }

}
