import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MysteryshopperPage } from './mysteryshopper';

@NgModule({
  declarations: [
    MysteryshopperPage,
  ],
  imports: [
    IonicPageModule.forChild(MysteryshopperPage),
  ],
})
export class MysteryshopperPageModule {}
