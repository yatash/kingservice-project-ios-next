import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController, AlertController, Events } from 'ionic-angular';
import { OrdersummaryPage } from '../ordersummary/ordersummary';
import { SecurityProvider } from '../../providers/security/security';
import { LoginPage } from '../login/login';
import { RevieworderPage } from '../revieworder/revieworder';

/**
 * Generated class for the RewardscartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rewardscart',
  templateUrl: 'rewardscart.html',
})
export class RewardscartPage { 
  itemlist:any=[];
  total_points:any=0;

  itemlistFinal:any=[]; 
  total_pointsFinal:any=0;

  CartStatus:boolean=false;

    DataCartValue:any=[];
    BlankArrStatus:boolean=false;
    CartItemArray:any=[];

    ShowCartSet:boolean=false;
    QuantityArr:any=[];
    products:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController:ToastController,public modalCtrl:ModalController, public alertCtrl:AlertController,public event:Events) {

    if(this.navParams.get("ShowCartSet") !=undefined) {
      this.ShowCartSet=this.navParams.get("ShowCartSet");
    }
     
    this.security.ViewCart().subscribe(res => { 
      if(res.status==200) {  
        if(res.success) {
        this.CartStatus=true;
        this.DataCartValue=res.data;
        if(this.DataCartValue.length==0) {
          localStorage.removeItem("CartAdd"); 
          localStorage.removeItem("CartAddTotalPoints"); 
          this.BlankArrStatus =true;
          return;
        }
        this.BlankArrStatus =false;
          for(let i=1;i<11;i++) { 
            this.QuantityArr.push(i);
          }
          this.itemlist=res.data.products;  
          this.total_points=res.data.total_points; 
          this.itemlistFinal=res.data.products;  this.total_pointsFinal=res.data.total_points;
          if(!this.ShowCartSet) {
            for(let i=0;i<this.itemlist.length;i++) { 
              this.CartItemArray.push({reward_product_id:this.itemlist[i].reward_product_id,qty:this.itemlist[i].qty,points:this.itemlist[i].points,denominations:this.itemlist[i].denominations}); 
            }
            localStorage.setItem("CartAddTotalPoints",this.total_points); 
            localStorage.setItem("CartAdd",JSON.stringify(this.CartItemArray)); 
          }
        this.CartItemArray=[]; 
        return;  
        }    
      } 
    }, err => {
      this.CartStatus=false;
      console.error("err==",err); 
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.setRoot(LoginPage);
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      else {
        this.toastController.create({ message: "No internet connection!", duration: 5000, position: 'bottom' }).present();
        return;  
      } 
    }); 


  }

  onChange() {
    this.total_points=0; 
    this.CartItemArray=[];
    for(let i=0;i<this.itemlist.length;i++) { 
      this.total_points=this.total_points+(parseInt(this.itemlist[i].qty)*parseInt(this.itemlist[i].denominations)); 
      this.CartItemArray.push({reward_product_id:this.itemlist[i].reward_product_id,qty:this.itemlist[i].qty,points:parseInt(this.itemlist[i].points),denominations:this.itemlist[i].denominations});   
    } 
    let CartAdd=JSON.stringify({products:this.CartItemArray,total_points:this.total_points}); 
    this.security.SaveCart(CartAdd).subscribe(res => { 
      if(res.status==200) { 
        if(res.success) { 
          this.itemlistFinal=this.itemlist;  this.total_pointsFinal=this.total_points;
          localStorage.setItem("CartAdd",JSON.stringify(this.CartItemArray));  
          localStorage.setItem("CartAddTotalPoints",this.total_points); 
          this.event.publish('CartAdd:CartAddTotalPoints',JSON.stringify(this.CartItemArray),this.total_points);  
          this.CartItemArray=[];
        } 
      } 
    }, err => { 
      this.itemlist=this.itemlistFinal; this.total_points=this.total_pointsFinal; this.CartItemArray=[];
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      if(err.status==500) {
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return;  
      }
      this.toastController.create({ message: "No internet connection.", duration: 5000, position: 'bottom' }).present();
      return; 
    });
  }

  CartAmount(qty,points) {
    return parseInt(qty)*parseInt(points);
  }

  RemoveCart(i) {
    let reward_product_id=this.itemlist[i].reward_product_id;   
    this.security.DeleteCartByProd(reward_product_id).subscribe(res => { 
      if(res.status==200) {  
        if(res.success) { 
          this.itemlist.splice(i,1);
          if(this.itemlist.length==0) { 
            localStorage.removeItem("CartAdd"); 
            localStorage.removeItem("CartAddTotalPoints"); 
            this.BlankArrStatus =true; 
          }
          return;  
        }    
      } 
    }, err => {
      console.error("err==",err); 
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.setRoot(LoginPage);
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      else {
        this.toastController.create({ message: "No internet connection!", duration: 5000, position: 'bottom' }).present();
        return;  
      } 
    }); 
  }

  checkout() { 
    let alert=this.alertCtrl.create({
      message: 'Do you want to proceed to balance points?',
      buttons: [
      {
      text: 'NO',
      role: 'cancel',
      handler: () => { 
      }
      },
      {
      text: 'YES',
      handler: () => {
        this.SendToData();
      }
      }
      ]
      });
      alert.present();  
  }

  SendToData() {
    this.security.Checkout().subscribe(res => {  
      if(res.status==200) {  
        if(res.success) {
          localStorage.removeItem("CartAdd"); 
          localStorage.removeItem("CartAddTotalPoints"); 
          this.navCtrl.push(OrdersummaryPage);
        }    
      } 
    }, err => {
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.setRoot(LoginPage);
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      } 
      if(err.status==500) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
        this.toastController.create({ message: "No internet connection.", duration: 5000, position: 'bottom' }).present();
        return;  
    }); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardscartPage');
  }

}