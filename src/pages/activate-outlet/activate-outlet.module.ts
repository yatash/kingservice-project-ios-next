import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivateOutletPage } from './activate-outlet';

@NgModule({
  declarations: [
    ActivateOutletPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivateOutletPage),
  ],
})
export class ActivateOutletPageModule {}
