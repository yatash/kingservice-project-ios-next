import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtpcodePage } from './otpcode';

@NgModule({
  declarations: [
    OtpcodePage,
  ],
  imports: [
    IonicPageModule.forChild(OtpcodePage),
  ],
})
export class OtpcodePageModule {}
