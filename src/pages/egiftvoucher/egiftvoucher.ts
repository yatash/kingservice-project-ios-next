import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,Events } from 'ionic-angular';
import { RewardscartPage } from '../rewardscart/rewardscart'; 
import { LoginPage } from '../login/login'; 
import { SecurityProvider } from '../../providers/security/security';

/**
 * Generated class for the EgiftvoucherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-egiftvoucher',
  templateUrl: 'egiftvoucher.html',
})
export class EgiftvoucherPage {
  DataServer:any=[];
  productID:any;
  denominations_dropdown:any=[];
  prod_name
  mobileImage
  mrp
  description
  cartdetails:any=[];
  points:any=0;
  Quantity:any=1;
  products:any=[];
  total_points:any=0;
  isProductInCart:boolean=false;
  ProductListID:any;
  CallServerStatus:boolean=false;

  Denomination:any;

  QuantityArr:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController: ToastController,public event:Events) {
    this.productID=this.navParams.get("productID");
  }
  
  ionViewWillEnter() {   
    this.GetQuizActivityFun();
  }
 
  GetQuizActivityFun() {
    this.security.productdetails(this.productID).subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
          console.log("res==",res); 
          this.CallServerStatus=true;  
            this.ProductListID=res.data.product_lists.id;
            this.isProductInCart=res.data.isProductInCart;
            this.DataServer=res.data.product_lists;
            this.denominations_dropdown=res.data.denominations_dropdown;
            this.prod_name=res.data.product_lists.prod_name;
            this.mobileImage=res.data.product_lists.mobile;
            for(let i=1;i<11;i++) {
              if(i==1)  {
                this.QuantityArr.push({valueArr:i,SelectedValue:true});
              }
              else {
                this.QuantityArr.push({valueArr:i,SelectedValue:false});
              }
            }
            if(res.data.product_lists.mobile=="" || res.data.product_lists.mobile==null) {
              this.mobileImage="assets/imgs/noimage.png";
            }
            this.mrp=res.data.product_lists.mrp;
            this.description=res.data.product_lists.description;
            if(res.data.product_lists.points != null) {
              this.points=res.data.product_lists.points;
            }
        } 
      } 
    }, err => { 
      this.CallServerStatus=false;
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      else {
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return;  
      }
    });
  }

CartBtn() { 
  this.event.subscribe('CartAdd:CartAddTotalPoints',(CartAdd,CartAddTotalPoint)=>{
    localStorage.setItem("CartAdd",JSON.stringify(CartAdd));  
    localStorage.setItem("CartAddTotalPoints",CartAddTotalPoint);       
  });
this.products=[];
if(this.Quantity=="" || this.Quantity==0) {
this.toastController.create({message:"Please choose at least 1 quantity.",duration:5000,position:'bottom'}).present();
return; 
}
if(this.Denomination=="" || this.Denomination==undefined) {
this.toastController.create({message:"Please select Denomination.",duration:5000,position:'bottom'}).present();
return; 
}
 
//let denominations_dropdown=this.denominations_dropdown.filter((item,index)=>{return this.Quantity-1==index;});

   if(localStorage.getItem("CartAdd") != null || localStorage.getItem("CartAdd")=="") {
    var stringParse=JSON.parse(localStorage.getItem("CartAdd")); 
    if(this.isProductInCart) {
        for(let i=0;i<stringParse.length;i++) {
          if(stringParse[i].reward_product_id==this.productID) {
            this.products.push({reward_product_id:this.productID,qty:parseInt(stringParse[i].qty)+parseInt(this.Quantity),points:this.points,denominations:this.Denomination}); 
          }
        }
    }
    else {
      this.products=stringParse;
      this.products.push({reward_product_id:this.productID,qty:this.Quantity,points:this.points,denominations:this.Denomination});
    }
   }
   else { 
    this.products.push({reward_product_id:this.productID,qty:this.Quantity,points:this.points,denominations:this.Denomination}); 
   } 

   if(localStorage.getItem("CartAddTotalPoints") != null || localStorage.getItem("CartAddTotalPoints")=="")  {
    if(this.isProductInCart) {
      this.total_points=parseInt(this.Quantity)*parseInt(this.Denomination);
    }
    else {
      this.total_points=parseInt(localStorage.getItem("CartAddTotalPoints"))+(parseInt(this.Quantity)*parseInt(this.Denomination));
    }
   }
   else {  
    this.total_points=parseInt(this.Quantity)*parseInt(this.Denomination); 
   } 
 
   let CartAdd=JSON.stringify({ products:this.products, total_points:this.total_points }); 

    this.security.SaveCart(CartAdd).subscribe(res => { 
      if(res.status==200) { 
        if(res.success) { 
          localStorage.setItem("CartAdd",JSON.stringify(this.products)); 
          localStorage.setItem("CartAddTotalPoints",this.total_points);   
          this.navCtrl.push(RewardscartPage,{ ShowCartSet:true });
        } 
      } 
    }, err => { 
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      if(err.status==500) {
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return;  
      }
      this.toastController.create({ message: "No internet connection.", duration: 5000, position: 'bottom' }).present();
      return; 
    });
  } 

  ionViewDidLoad() {
    console.log('ionViewDidLoad EgiftvoucherPage');
  }

}