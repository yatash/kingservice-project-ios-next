import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, AlertController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { RewardscartPage } from '../rewardscart/rewardscart'; 
import { RewarddescPage } from '../rewarddesc/rewarddesc';
import { DashboardPage } from '../dashboard/dashboard';
import { PointsSummaryPage } from '../points-summary/points-summary';
import { UsrprofilePage } from '../usrprofile/usrprofile';
import { LoginPage } from '../login/login';

/**
 * Generated class for the RewardcategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rewardcategory',
  templateUrl: 'rewardcategory.html',
})
export class RewardcategoryPage {
  categorylist:any=[];
  TabIndex
  LandingPage:any;

  DataCartValue:any=[];

  CartItemArray=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastController:ToastController,public security:SecurityProvider,public platform:Platform,public alertCtrl:AlertController) {
    this.LandingPage=this.navParams.get("LandingPage"); 
    this.TabIndex=this.navParams.get("TabIndex");  
    this.platform.registerBackButtonAction(() => {
      if(this.LandingPage == "loyality") {
        this.navCtrl.pop();
      }
      else {
        this.navCtrl.setRoot(DashboardPage,{TabIndex:1});   
      }
    },1);
    this.categorylist=[];
  }

  CloseBtn() {   
    if(this.LandingPage == "loyality") {
      this.navCtrl.pop();
    }
    else {
      this.navCtrl.setRoot(DashboardPage,{TabIndex:1});   
    }
  }

  CartBtn() {
    this.navCtrl.push(RewardscartPage);
  }

  NextBtn(index) {
    this.navCtrl.push(RewarddescPage,{categorylist:this.categorylist[index]});
  }

  ionViewWillEnter() {

    this.security.ViewCart().subscribe(res => { 
      if(res.status==200) {  
        if(res.success) {
        this.DataCartValue=res.data;
        if(this.DataCartValue.length==0) {
          localStorage.removeItem("CartAdd"); 
          localStorage.removeItem("CartAddTotalPoints"); 
          return;
        }
        for(let i=0;i<res.data.products.length;i++) { 
          this.CartItemArray.push({reward_product_id:res.data.products[i].reward_product_id,qty:res.data.products[i].qty,points:res.data.products[i].points,denominations:res.data.products[i].denominations}); 
        } 
        localStorage.setItem("CartAddTotalPoints",res.data.total_points); 
        localStorage.setItem("CartAdd",JSON.stringify(this.CartItemArray)); 
        this.CartItemArray=[];
        return;  
        }    
      } 
    }, err => {
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.setRoot(LoginPage);
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      } 
      if(err.status==500) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      else {
        this.toastController.create({ message: "No internet connection!", duration: 5000, position: 'bottom' }).present();
        return;  
      } 
    }); 

    this.security.categories().subscribe(res => { 
      if(res.status==200) {  
        if(res.success) {    
        this.categorylist=res.data;  
        return; 
        }    
      } 
    }, err => {
      console.error("err==",err); 
         console.log("error"); 
    });  
  }

  TabIcon(TabIndex) {  
    if(TabIndex == 1) {
      this.navCtrl.setRoot(DashboardPage,{ TabIndex:TabIndex });
    }
    if(TabIndex == 2) {
     this.navCtrl.setRoot(PointsSummaryPage,{ TabIndex:TabIndex });
    }
    if(TabIndex == 3) {
     //this.navCtrl.setRoot(RewardcategoryPage,{ TabIndex:TabIndex });
    }
    if(TabIndex == 4) {
     this.navCtrl.setRoot(UsrprofilePage,{ TabIndex:TabIndex });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardcategoryPage');
  }

}