import { Component, ɵConsole } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, Platform } from 'ionic-angular';

import {LoyaltyPage} from '../loyalty/loyalty';
import {SecurityProvider} from '../../providers/security/security'

/**
 * Generated class for the TargetsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-targets',
  templateUrl: 'targets.html',
})
export class TargetsPage {
  data:any=[];
  points_structure_data:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public platform:Platform) {
    this.platform.registerBackButtonAction(() => { 
      this.navCtrl.pop();   
    },1);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TargetsPage');
  }


  ionViewWillEnter() {  

    this.security.targets().subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
          console.log(res)
         this.points_structure_data=res.data.points_structure_data;
          this.data= res.data.target_vs_achieved;
          console.log(this.data)
        } 
   
      } 
 
    }, err => { 
      console.error("err==",err); 
      alert("err=="+JSON.stringify(err)); 
    });
   }

  
}
