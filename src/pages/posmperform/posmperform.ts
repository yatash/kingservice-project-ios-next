import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ToastController, Platform,LoadingController, ActionSheetController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { PosmperformpopPage } from '../posmperformpop/posmperformpop'; 
import { PosmpopupPage } from '../posmpopup/posmpopup'; 
import { PosmauditPage } from '../posmaudit/posmaudit';  

import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file'; 
import { BarcodeScanner } from '@ionic-native/barcode-scanner'; 
import { Geolocation } from '@ionic-native/geolocation'; 
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';

/**
 * Generated class for the PosmperformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-posmperform',
  templateUrl: 'posmperform.html',
})
export class PosmperformPage {

  isEnabled:boolean=false;
  EnableStore:boolean=false;
  EnableRegion:boolean=false;
  store:any=null;
  region:any=null;

  RegionList=[];
  StoreList=[];

  imageData; 
  selectAuditData:any=[]; 
  longitude:any=0;
  latitude:any=0;
  posm_type:any=1;

  imageMain:any;
  constructor(public navCtrl: NavController,public navParams: NavParams,public modalCtrl:ModalController,public security:SecurityProvider, public toastController : ToastController,private file: File,private camera: Camera,public barcodeScanner:BarcodeScanner,public platform:Platform,public geolocation:Geolocation,public loadingCtrl:LoadingController,public actionSheetCtrl:ActionSheetController) { 
    platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    },1);

    platform.ready().then((readySource) => {  
      if(readySource=='dom') {    
      }
      else if(readySource=='cordova'){
        this.geolocation.getCurrentPosition().then((position)=>{
        console.log(position);
        this.latitude=position.coords.latitude;
        this.longitude=position.coords.longitude;
        });
      }
      else{
      }
    });
    
    this.selectAuditData=this.navParams.get("auditdata");
    console.log("selectAuditData==",this.selectAuditData); 
    this.imageMain= this.selectAuditData.posm_image_banner; 
  }

  ActionSheetFile() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Choose a Picture',
      buttons: [
        {
          text: 'From Gallery',
          role: 'destructive',
          handler: () => {
            this.gallery();
            console.log('Destructive clicked');
          }
        },{
          text: 'From Camera',
          handler: () => {
            this.cameratest();
            console.log('Archive clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  gallery() {
    this.camera.getPicture({
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL, 
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 1100,
      targetWidth: 1100,
      saveToPhotoAlbum: true,
      correctOrientation: true
    }).then((imageData) => {
      this.imageData='data:image/jpeg;base64,' + imageData; 
      this.OpenPopUp();
    }, (err) => {
    });
  }

  cameratest() {
    this.camera1();
  } 

  camera1() { 
    this.camera.getPicture({
      quality: 100,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 1100,
      targetWidth: 1100,
      saveToPhotoAlbum: true,
      correctOrientation: true
    }).then((imageData) => {
      this.imageData='data:image/jpeg;base64,' + imageData; 
      this.OpenPopUp();
    }, (err) => {
    });
  }

    OpenPopUp() {  
    let logmodal1=this.modalCtrl.create(HomePage,{selectAuditData:this.selectAuditData,imageData:this.imageData,successmsg:false,posmBool:true,latitude:this.latitude,longitude:this.longitude,DataTransaction:false}); 
    logmodal1.onDidDismiss((views) => { 
      if(views) { 
        let logmodal2=this.modalCtrl.create(PosmpopupPage,{selectAuditData:this.selectAuditData,imageData:this.imageData,successmsg:false,posmBool:true,latitude:this.latitude,longitude:this.longitude,DataTransaction:false}); 
        logmodal2.onDidDismiss((views) => { 
          if(views) { 
            this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 2)); 
          } 
        });  
        logmodal2.present();
      } 
    });  
    logmodal1.present();
  }

  QRCodeScan() { 
    this.qrscanner();
  } 

  qrscanner() {    
    this.barcodeScanner.scan().then(barcodeData => {
      var code=barcodeData.text; 
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present(); 
      this.posm_type=1; 
      this.security.postmantrans(this.latitude,this.longitude,this.posm_type,code,this.selectAuditData.posm_audit_typeid,"image").subscribe(res =>{  
        loading.dismiss();
        if(res.status==200) { 
         if(res.success) {
          let DataTransaction=res.data.Transaction;  
           // Start of Modal Popup.. 
          let logmodal1=this.modalCtrl.create(PosmpopupPage,{selectAuditData:this.selectAuditData,successmsg:true,posmBool:true,latitude:this.latitude,longitude:this.longitude,DataTransaction:DataTransaction}); 
          logmodal1.onDidDismiss((views) => {  
            if(views) { 
              this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 2)); 
            } 
          });  
          logmodal1.present(); 
          // End of Modal Popup..
          return false;
          }
         }
    }, err => { 
        loading.dismiss();
        if(err.status==422) { 
            this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
            return false;
        }
        else { 
            console.error("err==",err); 
        } 
  }); 
     }).catch(err => {
       var code=err.text;
       this.toastController.create({ message: "Cancelled scan", duration: 5000, position: 'bottom' }).present();
     });
  }


  ngAfterViewInit(): void { 
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) { Object.keys(tabs).map((key) => { tabs[key].style.display = 'none'; }); }
  }
  
  ionViewWillLeave() {  
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) { Object.keys(tabs).map((key) => { tabs[key].style.display = 'flex'; }); }
  } 
  
  CloseBtn() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PosmperformPage');
  }

}
