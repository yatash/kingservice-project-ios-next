import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewardfilterPage } from './rewardfilter';

@NgModule({
  declarations: [
    RewardfilterPage,
  ],
  imports: [
    IonicPageModule.forChild(RewardfilterPage),
  ],
})
export class RewardfilterPageModule {}
