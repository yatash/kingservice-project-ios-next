import { NgModule } from '@angular/core';
import { EnterotpPipe } from './enterotp/enterotp';
@NgModule({
	declarations: [EnterotpPipe],
	imports: [],
	exports: [EnterotpPipe]
})
export class PipesModule {}
