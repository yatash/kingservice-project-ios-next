import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuiztaskPage } from './quiztask';

@NgModule({
  declarations: [
    QuiztaskPage,
  ],
  imports: [
    IonicPageModule.forChild(QuiztaskPage),
  ],
})
export class QuiztaskPageModule {}
