import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser';
/*
import { RewardcategoryPage } from '../rewardcategory/rewardcategory';
import { RewarddescPage } from '../rewarddesc/rewarddesc'; 
import{RewardscartPage} from '../rewardscart/rewardscart' 
*/

/**
 * Generated class for the RewarddetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rewarddetail',
  templateUrl: 'rewarddetail.html',
})
export class RewarddetailPage {
  ETapUrl:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private sanitizer: DomSanitizer) {
    this.ETapUrl=this.sanitizer.bypassSecurityTrustResourceUrl("http://staging12.bigcityexperiences.co.in/"); 
  }
  CloseBtn() {
    this.navCtrl.pop();
  }
  /*
  CatBtn() {  
    this.navCtrl.push(RewardscartPage);     
  }
  CategoryBtn() {  
    this.navCtrl.push(RewardcategoryPage);  
  }
  RecentlyBtn() {
    this.navCtrl.push(RewarddescPage);  
  }
  */
  ionViewDidLoad() {
    console.log('ionViewDidLoad RewarddetailPage');
  }
}