"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var Winwheel = require("Winwheel");
var tweenlite = require("tweenlite");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        // hardcode prizes
        this.segments = {
            general: [
                { 'fillStyle': '#d4f3fc', 'text': '100', 'value': 100 },
            ],
            normal: [
                { 'fillStyle': '#a9e8fa', 'text': '200', 'value': 200 },
            ],
            rare: [
                { 'fillStyle': '#7dddf8', 'text': '300', 'value': 300 },
            ],
            ultimate: [
                { 'fillStyle': '#52d2f6', 'text': '400', 'value': 400 }
            ]
        };
        this.config = {
            'canvasId': 'spinwheel',
            'outerRadius': 150,
            'textFontSize': 24,
            'textOrientation': 'vertical',
            'textAlignment': 'outer',
            'numSegments': 12,
            'segments': this.getSegments(),
            'rotationAngle': -15,
            'animation': {
                'type': 'spinToStop',
                'direction': 'clockwise',
                'propertyName': 'rotationAngle',
                'easing': 'Power4.easeOut',
                'duration': 15,
                'spins': 3
            }
        };
        this.title = 'Winwheel Spinner with Angular';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.draw();
    };
    AppComponent.prototype.getSegments = function () {
        var segments = this.segments;
        var result = [];
        result = result.concat(segments.ultimate);
        for (var x = 0; x < 5; x++) {
            // general
            result = result.concat(segments.general);
            // normal
            if (x < 4) {
                result = result.concat(segments.normal);
            }
            // rare
            if (x === 0 || x === 2) {
                result = result.concat(segments.rare);
            }
        }
        return result;
    };
    AppComponent.prototype.draw = function () {
        this.wheel = new Winwheel.Winwheel(this.config);
        this.wheel.draw();
    };
    AppComponent.prototype.spinningSound = function (cb) {
        // let audio = new Audio();
        // audio.src = '/assets/files/spinning-bicycle-wheel.mp3';
        // audio.load();
        this.audio.play();
        return cb();
    };
    AppComponent.prototype.spin = function () {
        var _this = this;
        var onComplete = function () {
            console.log('Done spinning!');
        };
        var winwheelAnimationLoop = function () {
            console.log('spining~');
            var wheel = _this;
            var self = wheel.wheel || wheel;
            if (self) {
                if (self.animation.clearTheCanvas != false) {
                    self.ctx.clearRect(0, 0, self.canvas.width, self.canvas.height);
                }
                self.draw(false);
            }
        };
        this.wheel.computeAnimation();
        //this.spinningSound(() => {
        var animation = this.wheel.animation, properties = {
            yoyo: animation.yoyo,
            repeat: animation.repeat,
            ease: animation.easing,
            onUpdate: winwheelAnimationLoop,
            onUpdateScope: this.wheel,
            onComplete: onComplete
        };
        properties["" + animation.propertyName] = animation.propertyValue;
        this.wheel.tween = tweenlite.to(this.wheel, animation.duration, properties);
        //});
        console.log('tweenlite', tweenlite);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n    <h3>{{title}}</h3>\n    <div>\n        <button class=\"button\" (click)=\"draw()\"> Reset </button>\n        <button class=\"button\" (click)=\"spin()\"> Spin </button>\n    </div>\n    <div>\n        <canvas id=\"spinwheel\" height=\"400px\" width=\"350px\"></canvas>\n    </div>\n  \n  "
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule
            ],
            declarations: [
                AppComponent
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(AppModule);