import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ToastController, Platform,LoadingController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { PosmperformpopPage } from '../posmperformpop/posmperformpop'; 
import { PosmpopupPage } from '../posmpopup/posmpopup'; 
import { PosmauditPage } from '../posmaudit/posmaudit';  

import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file'; 
import { BarcodeScanner } from '@ionic-native/barcode-scanner'; 
import { Geolocation } from '@ionic-native/geolocation'; 
import { TabsPage } from '../tabs/tabs';
import { DashboardPage } from '../dashboard/dashboard'; 
import { FortunewheelPage } from '../fortunewheel/fortunewheel'; 
import { YouhaverecvdPage } from '../youhaverecvd/youhaverecvd';
import {ActivitiesPage} from '../activities/activities'

/**
 * Generated class for the ImagetaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-imagetask',
  templateUrl: 'imagetask.html',
})
export class ImagetaskPage  implements OnInit{
  imageData: string;
  image:boolean=false;
  no_image=true;
  task_id: any;
  CorrectAnswer:boolean=false;
  CorrectAnswerOption:any=null;
  is_answer_check:boolean=false;
  PreviousOption:any=null;
  correct_option:string;
  task_question_id
  quiz_type
  is_subtask:boolean=false;

  GetTaskList:boolean=true;
  GetTaskSub:boolean=false;
  questions:any=[];
  subtask_question_id:any=null;
right_answer: any;
selectedOption: any;
isright: boolean=false;
iswrong: boolean=false;
succ_points: any;
has_subtask: any;
wof_enable: any;
SelectContentArr:any={course_title:"Image"}
disabled:boolean=false;

  constructor(public navCtrl: NavController,public navParams: NavParams,public modalCtrl:ModalController,public security:SecurityProvider, public toastController : ToastController,private file: File,private camera: Camera,public barcodeScanner:BarcodeScanner,platform:Platform,public geolocation:Geolocation,public loadingCtrl:LoadingController
    
    ) {
  }
ngOnInit(){
  console.log("image:"+this.image);
  console.log("no_image:"+this.no_image);
  this.task_id=this.navParams.get("task_id");
  this.GetQuizActivityFun();
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad ImagetaskPage');
  }

  cameratest() {
    this.camera1();
  } 

  GetQuizActivityFun(){
    this.security.GetQuizActivity(this.task_id).subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
          console.log(res)
          
this.is_subtask=false;
this.quiz_type=2;
this.task_question_id=res.data.quizdata.task_question_id

        } 
        else { 
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();  return; 
       }  
      } 
       else { 
         this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();  return; 
      }
    }, err => { 
      console.error("err==",err); 
      alert("err=="+JSON.stringify(err)); 
    });
  }


  camera1() {  
    this.camera.getPicture({
      quality: 75,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 500,
      targetWidth: 500,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }).then((imageData) => {

      //this.imageData=imageData;
        console.log("imageData==",imageData);  
      this.imageData='data:image/jpeg;base64,' + imageData; 
      this.image=true;
      this.no_image=false;
     
    }, (err) => {
      this.toastController.create({ message:"Image upload fail, Please try again.", duration: 3000, position: 'bottom' }).present(); 
    })
  }

  NextBtn1() {
    this.disabled = true;
       this.security.DoTaskActivity(this.task_id,this.is_subtask,this.quiz_type,this.task_question_id,this.imageData).subscribe(res => {
        if(res.status==200) { 
          if(res.success) {
            console.log(res);
            let wof_eb=res.data.wof_enable;
            if(wof_eb==true){
                  let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:this.SelectContentArr, points:100 }); 
                modal.onDidDismiss((views) => {   
                  this.navCtrl.setRoot(FortunewheelPage,{LandQuestion:true});    
                });  
                modal.present();
            }
            else{
              let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:this.SelectContentArr, points:100 }); 
              modal.onDidDismiss((views) => {   
                this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true});    
              });  
              modal.present();
            }
                  } 
        } 
      
      }, err => { 
        console.error("err==",err); 
        alert("err=="+JSON.stringify(err)); 
      });
    
 
    
    }

}
