import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mysteryshopper5Page } from './mysteryshopper5';

@NgModule({
  declarations: [
    Mysteryshopper5Page,
  ],
  imports: [
    IonicPageModule.forChild(Mysteryshopper5Page),
  ],
})
export class Mysteryshopper5PageModule {}
