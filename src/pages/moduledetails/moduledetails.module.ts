import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModuledetailsPage } from './moduledetails';

@NgModule({
  declarations: [
    ModuledetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ModuledetailsPage),
  ],
})
export class ModuledetailsPageModule {}
