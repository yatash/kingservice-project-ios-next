import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, ToastController, Events, Platform } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';

import { AnnouncementPage } from '../announcement/announcement'; 
import { ActivateOutletPage } from '../activate-outlet/activate-outlet';  
import { RetailerOutletPage } from '../retailer-outlet/retailer-outlet'; 

import { OnInit, HostListener, Inject } from '@angular/core';  
import { DOCUMENT } from '@angular/common';

import { ActivitiesPage } from '../activities/activities';  
import { FortunewheelPage } from '../fortunewheel/fortunewheel';  
import { LoyaltyPage } from '../loyalty/loyalty';
import { PosmauditPage } from '../posmaudit/posmaudit'; 
import { PosmperformPage } from '../posmperform/posmperform';
import { RewarddetailPage } from '../rewarddetail/rewarddetail'; 

import { OutletnotificationPage } from '../outletnotification/outletnotification';
import { LoginPage } from '../login/login';  
import {QuestionsPage} from '../questions/questions';
import {ActivationsPage} from '../activations/activations'
import {StartQuizPage} from '../start-quiz/start-quiz'

import { LearningdevelopmentPage } from '../learningdevelopment/learningdevelopment';

import { MysteryshopperPage } from '../mysteryshopper/mysteryshopper';  
import {PointsSummaryPage} from '../points-summary/points-summary';
import { PosmperformpopPage } from '../posmperformpop/posmperformpop';
import { TargetsPage } from '../targets/targets';

import { UsrprofilePage } from '../usrprofile/usrprofile'; 
import { RewardcategoryPage } from '../rewardcategory/rewardcategory';
import { SchemesPage } from '../schemes/schemes';
import { LeadersboardPage } from '../leadersboard/leadersboard';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html', 
})
export class DashboardPage {
  
  items=[] 
  modules=[];
  store_details;

  UserName;
  UsrPoints;
  milstone_tracker:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,@Inject(DOCUMENT) document,public security:SecurityProvider,public alertCtrl: AlertController, public toastController : ToastController,public event:Events,public platform:Platform) {
   this.milstone_tracker=[];

 
  //  this.platform.registerBackButtonAction(() => { 
  //   let alert=this.alertCtrl.create({
  //     message: 'Are you sure you want to exit?',
  //     buttons: [
  //     {
  //     text: 'NO',
  //     role: 'cancel', 
  //     handler: () => {
  //     }
  //     },
  //     {
  //     text: 'YES',
  //     handler: () => {
  //     platform.exitApp();
  //     }
  //     }
  //     ]
  //     });
  //     alert.present();  
  // },1);

  }

// @HostListener('window:scroll', ['$event'])
//   onWindowScroll(e) {
//      if (window.pageYOffset > 550) {
//        let element = document.getElementById('myHeader'); 
//        element.classList.add('sticky');
//      } else {
//       let element = document.getElementById('myHeader'); 
//         element.classList.remove('sticky'); 
//      }
//   }

TargetNext() {  
  this.navCtrl.push(TargetsPage);
}  

points() {
  this.navCtrl.setRoot(PointsSummaryPage);
}

  Notification() {
    this.navCtrl.push(OutletnotificationPage);
  }

  ionViewDidLoad()  {
    console.log('ionViewDidLoad DashboardPage');
  }

  DivWidth(evDiv) {
    let divvalue=100;
    if(evDiv<100) {
      divvalue=evDiv;
    }
    if(evDiv>100) {
      divvalue=100;
    }
    if(evDiv==100) {
      divvalue=100;
    }
    return divvalue;
  }   

   ionViewWillEnter() {   
    this.security.UserDashboardNew().subscribe(res => {
      console.log("res==",res);  
      console.log("res success==",res.success); 
          this.UserName= res.data.display_name;
          this.UsrPoints=res.data.total_points; 
          this.modules=res.data.modules;    
          this.milstone_tracker=res.data.milstone_tracker;
          if(res.status==200) {  
          if(res.success) { 
          return; 
          }    
      } 
       else { this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();  return;  }
    }, err => { 
      console.error("err==",err); 
      console.error("err status==",err.status);  
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
        return; 
      }  
    });    
   } 

   NextPage(i) {
    //if(this.modules[i].enable_flag)   {
      if(this.modules[i].module_indicate=="loyalty")  {
        this.navCtrl.push(LoyaltyPage);     
      }
      if(this.modules[i].module_indicate=="engagement")  {
        this.navCtrl.push(ActivitiesPage);    
      }
      if(this.modules[i].module_indicate=="visibility")  {
        this.navCtrl.push(PosmperformpopPage);    
      }
      if(this.modules[i].module_indicate=="content_learning")  {
        this.navCtrl.push(LearningdevelopmentPage);     
      }
      if(this.modules[i].module_indicate=="mystery_audit")  {
        this.navCtrl.push(MysteryshopperPage);     
      }
      if(this.modules[i].module_indicate=="etap")  {
        this.navCtrl.push(RewarddetailPage);   
      }
      if(this.modules[i].module_indicate=="tertiary_programs")  {
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: 'You don’t have access to this module',
          buttons: [
            {
              text: 'BACK',
              handler: () => {
              }
            }
          ],
          enableBackdropDismiss:false,
        });
        alert.present();
      }   
      if(this.modules[i].module_indicate=="menu")  {
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: 'You don’t have access to this module',
          buttons: [
            {
              text: 'BACK',
              handler: () => { 
              }
            }
          ],
          enableBackdropDismiss:false,
        });
        alert.present();    
      }
    // }
    // else {
    //   let alert = this.alertCtrl.create({
    //     title: 'Oops!',
    //     subTitle: 'You don’t have access to this module',
    //     buttons: [
    //       {
    //         text: 'BACK',
    //         handler: () => {
    //         }
    //       }
    //     ],
    //     enableBackdropDismiss:false,
    //   });
    //   alert.present(); 
    //  } 
   }

   TabIcon(TabIndex) {  
     if(TabIndex == 1) {
      
     }
     if(TabIndex == 2) {
      this.navCtrl.setRoot(PointsSummaryPage,{ TabIndex:TabIndex });
     }
     if(TabIndex == 3) {
      this.navCtrl.setRoot(RewardcategoryPage,{ TabIndex:TabIndex });
     }
     if(TabIndex == 4) {
      this.navCtrl.setRoot(UsrprofilePage,{ TabIndex:TabIndex });
     }
   }

   UserProfile() {
    this.navCtrl.push(UsrprofilePage);
   }
   
}