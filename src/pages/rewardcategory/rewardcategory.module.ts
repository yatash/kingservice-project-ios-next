import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewardcategoryPage } from './rewardcategory';

@NgModule({
  declarations: [
    RewardcategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(RewardcategoryPage),
  ],
})
export class RewardcategoryPageModule {}
