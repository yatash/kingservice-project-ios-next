import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Mysteryshopper3Page } from '../mysteryshopper3/mysteryshopper3'; 

/**
 * Generated class for the Mysteryshopper2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper2',
  templateUrl: 'mysteryshopper2.html',
})
export class Mysteryshopper2Page {
  outlet_datas:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.outlet_datas=this.navParams.get("outlet_datas");
  }

  NextBtn() {
    this.navCtrl.push(Mysteryshopper3Page, { outlet_datas:this.outlet_datas });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Mysteryshopper2Page');
  }

}
