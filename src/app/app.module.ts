import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, IonicPageModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FCM } from '@ionic-native/fcm';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { File } from '@ionic-native/file';  
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { SmsRetriever } from '@ionic-native/sms-retriever/ngx';
import { Geolocation } from '@ionic-native/geolocation';
import { Base64 } from '@ionic-native/base64';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { SecurityProvider } from '../providers/security/security';


import { HomePage } from '../pages/home/home'; 
import { LoginPage } from '../pages/login/login';
import { LanguagedropPage } from '../pages/languagedrop/languagedrop';
import { LanguagedropshowPage } from '../pages/languagedropshow/languagedropshow';
import { OtpcodePage } from '../pages/otpcode/otpcode';  
 
import { MoredetailsPage } from '../pages/moredetails/moredetails';    
import { MobilenoPage } from '../pages/mobileno/mobileno';     
import { StartedPage } from '../pages/started/started';   
import { TermsandconditionsPage } from '../pages/termsandconditions/termsandconditions';   
import { WelcomePage } from '../pages/welcome/welcome';     

import { ActivateOutletPage } from '../pages/activate-outlet/activate-outlet';    
import { ClickPicturePage } from '../pages/click-picture/click-picture';  
import { RetailerOutletPage } from '../pages/retailer-outlet/retailer-outlet';  

import { DashboardPage } from '../pages/dashboard/dashboard'; 
import { AnnouncementPage } from '../pages/announcement/announcement'; 
import { AnnouncementsPage } from '../pages/announcements/announcements'; 
 
import { VisitDetails1Page } from '../pages/visit-details1/visit-details1'; 
import { VisitDetailsPage } from '../pages/visit-details/visit-details'; 
import { SelectStorePage } from '../pages/select-store/select-store';      

import { RevenuePage } from '../pages/revenue/revenue';      
import { LogsalesPage } from '../pages/logsales/logsales';  
import { VoucherPage } from '../pages/voucher/voucher';  

import { ActivationsPage } from '../pages/activations/activations';       
import { OutletDetailsPage } from '../pages/outlet-details/outlet-details';  

import { CreatepasswordPage } from '../pages/createpassword/createpassword';  

import { PosmauditPage } from '../pages/posmaudit/posmaudit';  
import { PosmpopupPage } from '../pages/posmpopup/posmpopup';  
import { PosmperformPage } from '../pages/posmperform/posmperform'; 
import { PosmperformpopPage } from '../pages/posmperformpop/posmperformpop';      

import { RewardstorePage } from '../pages/rewardstore/rewardstore';  
import { RewardcategoryPage } from '../pages/rewardcategory/rewardcategory';
import { RewarddescPage } from '../pages/rewarddesc/rewarddesc'; 
import { RewardfilterPage } from '../pages/rewardfilter/rewardfilter'; 

import { RewarddetailPage } from '../pages/rewarddetail/rewarddetail';  



import { EgiftvoucherPage } from '../pages/egiftvoucher/egiftvoucher';
import { CartoninsertPage } from '../pages/cartoninsert/cartoninsert';
import { RevieworderPage } from '../pages/revieworder/revieworder';
import { OrdersummaryPage } from '../pages/ordersummary/ordersummary';
import { RewardscartPage } from '../pages/rewardscart/rewardscart';  
import { ChillerputtyPage } from '../pages/chillerputty/chillerputty';
import { OutletnotificationPage } from '../pages/outletnotification/outletnotification';
import { LearningdevelopmentPage } from '../pages/learningdevelopment/learningdevelopment';
import { ModuledetailsPage } from '../pages/moduledetails/moduledetails';
import { Moduledetails1Page } from '../pages/moduledetails1/moduledetails1';
import { QuiztaskPage } from '../pages/quiztask/quiztask';
import { YouhaverecvdPage } from '../pages/youhaverecvd/youhaverecvd'; 
 
import { MysteryshopperPage } from '../pages/mysteryshopper/mysteryshopper';  
import { Mysteryshopper1Page } from '../pages/mysteryshopper1/mysteryshopper1'; 
import { Mysteryshopper2Page } from '../pages/mysteryshopper2/mysteryshopper2'; 
import { Mysteryshopper3Page } from '../pages/mysteryshopper3/mysteryshopper3'; 
import { Mysteryshopper3onePage } from '../pages/mysteryshopper3one/mysteryshopper3one'; 
import { Mysteryshopper3twoPage } from '../pages/mysteryshopper3two/mysteryshopper3two'; 
import { Mysteryshopper4Page } from '../pages/mysteryshopper4/mysteryshopper4'; 
import { Mysteryshopper5Page } from '../pages/mysteryshopper5/mysteryshopper5'; 
import { Mysteryshopper6Page } from '../pages/mysteryshopper6/mysteryshopper6'; 
import { Mysteryshopper7Page } from '../pages/mysteryshopper7/mysteryshopper7'; 

//another page 
// import { ActivitiesPage } from '../pages/activities/activities'; 
import { StartQuizPage } from '../pages/start-quiz/start-quiz';
import { UsrprofilePage } from '../pages/usrprofile/usrprofile';  
import { LoyaltyPage } from '../pages/loyalty/loyalty'; 
import { QuizTaskPage } from '../pages/quiz-task/quiz-task'; 
import { QuestionsPage } from '../pages/questions/questions';  
import { FortunewheelPage } from '../pages/fortunewheel/fortunewheel';        


import {SchemesPage} from '../pages/schemes/schemes';
import {RedeemRewardsPage} from '../pages/redeem-rewards/redeem-rewards';
import {Schme1Page} from '../pages/schme1/schme1';
import {LeadersboardPage} from '../pages/leadersboard/leadersboard';
import {FinishedLeaderboardPage} from '../pages/finished-leaderboard/finished-leaderboard';
import {ProfilePage} from '../pages/profile/profile';
import {EditProfilePage} from '../pages/edit-profile/edit-profile';
import {MyWinningsPage} from '../pages/my-winnings/my-winnings';
import {SettingsPage} from '../pages/settings/settings';
import {AppFeedbackPage} from '../pages/app-feedback/app-feedback';
import {ContactusPage} from '../pages/contactus/contactus';
import {PointsBruntPage} from '../pages/points-brunt/points-brunt';
import {HelpPage} from '../pages/help/help';
 
import {StarsPage} from '../pages/stars/stars';
import {ImagetaskPage} from '../pages/imagetask/imagetask';  
  
import { StarRatingModule } from 'ionic3-star-rating';
import { ActivitiesPageModule } from '../pages/activities/activities.module';
import { TargetsPageModule } from '../pages/targets/targets.module';
import { PointsSummaryPageModule } from '../pages/points-summary/points-summary.module';
import { QuizDescPage} from '../pages/quiz-desc/quiz-desc'

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {  
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}   

@NgModule({
  declarations: [
    MyApp,
    LoginPage, HomePage,LanguagedropPage,LanguagedropshowPage,OtpcodePage,LoyaltyPage,QuizDescPage,
    MoredetailsPage,MobilenoPage,StartedPage,TermsandconditionsPage,WelcomePage,
    ActivateOutletPage,ClickPicturePage,QuestionsPage,QuizTaskPage,RetailerOutletPage,
    DashboardPage,AnnouncementPage,AnnouncementsPage,
    VisitDetails1Page,VisitDetailsPage,SelectStorePage,
    LogsalesPage,VoucherPage,RevenuePage,
    ActivationsPage,OutletDetailsPage,
    FortunewheelPage,
    CreatepasswordPage,
  
    PosmauditPage,PosmpopupPage,PosmperformPage,PosmperformpopPage,
    UsrprofilePage,
    RewardstorePage,RewardcategoryPage,RewarddescPage,RewardfilterPage,RewarddetailPage,
    TabsPage,EgiftvoucherPage,CartoninsertPage,RevieworderPage,OrdersummaryPage,RewardscartPage,
    ChillerputtyPage,OutletnotificationPage,LearningdevelopmentPage,ModuledetailsPage,Moduledetails1Page,StartQuizPage,
    QuiztaskPage,YouhaverecvdPage,
    MysteryshopperPage,Mysteryshopper1Page,Mysteryshopper2Page,Mysteryshopper3Page,Mysteryshopper3onePage,Mysteryshopper3twoPage,Mysteryshopper4Page,Mysteryshopper5Page,Mysteryshopper6Page,Mysteryshopper7Page,
    SchemesPage,RedeemRewardsPage,Schme1Page,LeadersboardPage,FinishedLeaderboardPage,ProfilePage,EditProfilePage,MyWinningsPage,SettingsPage,ContactusPage,PointsBruntPage,HelpPage,AppFeedbackPage,
 
    ImagetaskPage,
    StarsPage
  ],
  imports: [ 
  
    TargetsPageModule,
    PointsSummaryPageModule,
    ActivitiesPageModule,
    BrowserModule,
    StarRatingModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: false,
    }), 
    HttpClientModule,
    HttpModule, 
    ReactiveFormsModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,LoginPage,LanguagedropPage,LanguagedropshowPage, OtpcodePage,LoyaltyPage,QuizDescPage,
    MoredetailsPage,MobilenoPage,StartedPage,TermsandconditionsPage,WelcomePage,  
    ActivateOutletPage,ClickPicturePage,QuestionsPage,QuizTaskPage,RetailerOutletPage, 
    DashboardPage,AnnouncementPage,AnnouncementsPage,
    VisitDetails1Page,VisitDetailsPage,SelectStorePage,
    LogsalesPage,VoucherPage,RevenuePage, 
    ActivationsPage,OutletDetailsPage, 
    FortunewheelPage, 
    CreatepasswordPage,

    PosmauditPage,PosmpopupPage,PosmperformPage,PosmperformpopPage,
    UsrprofilePage,
    RewardstorePage,RewardcategoryPage,RewarddescPage,RewardfilterPage,RewarddetailPage,
    TabsPage,EgiftvoucherPage,CartoninsertPage,RevieworderPage,OrdersummaryPage,RewardscartPage,ChillerputtyPage,OutletnotificationPage,LearningdevelopmentPage,ModuledetailsPage,Moduledetails1Page,StartQuizPage,
    QuiztaskPage,YouhaverecvdPage,
    MysteryshopperPage,Mysteryshopper1Page,Mysteryshopper2Page,Mysteryshopper3Page,Mysteryshopper3onePage,Mysteryshopper3twoPage,Mysteryshopper4Page,Mysteryshopper5Page,Mysteryshopper6Page,Mysteryshopper7Page,
    SchemesPage,RedeemRewardsPage,Schme1Page,LeadersboardPage,FinishedLeaderboardPage,ProfilePage,EditProfilePage,MyWinningsPage,SettingsPage,ContactusPage,PointsBruntPage,HelpPage,AppFeedbackPage,
   
    ImagetaskPage,
    StarsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,  
    File,
    Camera,
    AndroidPermissions,
    SmsRetriever,
    Geolocation,
    Base64,
    ScreenOrientation,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SecurityProvider,
    FCM
  ]
})
export class AppModule {}
