import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';

import {TargetsPage} from '../targets/targets';
import {SchemesPage} from '../schemes/schemes';
import {RedeemRewardsPage} from '../redeem-rewards/redeem-rewards';
import { RewardcategoryPage } from '../rewardcategory/rewardcategory';
import {SecurityProvider} from '../../providers/security/security'


/**
 * Generated class for the LoyaltyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loyalty',
  templateUrl: 'loyalty.html',
})
export class LoyaltyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoyaltyPage');
  }

  target() {
    this.security.targets().subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
          this.navCtrl.push(TargetsPage);
        } 
      } 
      else if(res.status==422){
        alert(res.data.message)
      }
      else{
      }
    }, err => { 
        if(err.status == 422){
          var a =JSON.parse(err._body);
         let e= a.data.message;
         e = (e.substring(1, (e.length - 1)));
          let alert = this.alertCtrl.create({
            subTitle: e,
            
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  
                }
              }
            ],
            enableBackdropDismiss:false,
          });
          alert.present();
        }
      // alert("err=="+JSON.stringify(err)); 
    });
   }

  schemes(){
    this.navCtrl.push(SchemesPage);
  }

  reedem() {
    this.navCtrl.push(RewardcategoryPage,{LandingPage:"loyality"});  
  }

  CloseBtn() {
    this.navCtrl.pop();
  }

}