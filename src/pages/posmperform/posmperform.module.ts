import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PosmperformPage } from './posmperform';

@NgModule({
  declarations: [
    PosmperformPage,
  ],
  imports: [
    IonicPageModule.forChild(PosmperformPage),
  ],
})
export class PosmperformPageModule {}
