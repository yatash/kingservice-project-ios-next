import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController, Platform } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { ActivitiesPage } from '../activities/activities';
import { TabsPage } from '../tabs/tabs';
import { DashboardPage } from '../dashboard/dashboard';
import { FortunewheelPage } from '../fortunewheel/fortunewheel';
import { YouhaverecvdPage } from '../youhaverecvd/youhaverecvd';


/**
 * Generated class for the QuestionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questions',
  templateUrl: 'questions.html',
})
export class QuestionsPage implements OnInit {
  items = ['Hi, How are you!', 'Hey, are you dieting?', 'We do not have it.', 'We will get it delivered']
  task_id: any;

  question: any = null;
  questionOption1: any = null;
  questionOption2: any = null;
  questionOption3: any = null;
  questionOption4: any = null;
  subtask_ques = true;
  questionAnsOption1: boolean = false;
  questionAnsOption2: boolean = false;
  questionAnsOption3: boolean = false;
  questionAnsOption4: boolean = false;

  CorrectAnswer: boolean = false;
  CorrectAnswerOption: any = null;
  is_answer_check: boolean = false;
  PreviousOption: any = null;
  correct_option: string;
  task_question_id
  quiz_type:any;
  is_subtask: boolean = false;

  GetTaskList: boolean = true;
  GetTaskSub: boolean = false;
  questions: any = [];
  subtask_question_id: any = null;
  right_answer: any;
  selectedOption: any;
  isright: boolean = false;
  iswrong: boolean = false;
  succ_points: any;
  has_subtask: any;
  wof_enable: any;
  SelectContentArr: any = { course_title: "Quiztask" }
  disabled: boolean = false;
  disabled1: boolean = false;
  question_type: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public security: SecurityProvider, public toastController: ToastController, public modalCtrl: ModalController, platform: Platform) {
    let backAction = platform.registerBackButtonAction(() => {
    }, 1);
  }
  ngOnInit() {
    this.subtask_ques = true;
  }

  initializeValues() {
    this.question = null;
    this.questionOption1 = null;
    this.questionOption2 = null;
    this.questionOption3 = null;
    this.questionOption4 = null;

    this.questionAnsOption1 = false;
    this.questionAnsOption2 = false;
    this.questionAnsOption3 = false;
    this.questionAnsOption4 = false;

    this.CorrectAnswer = false;
    this.CorrectAnswerOption = null;
    this.is_answer_check = false;
    this.PreviousOption = null;
  }

  ionViewWillEnter() {
    this.task_id = this.navParams.get("task_id");
    //this.task_id=87;
    this.GetQuizActivityFun();
  }
  select: boolean = false
  selectedItem: any;

  selected = null;

  onClick(item, i, id) {
    

    if (this.right_answer != null  || this.right_answer != undefined) {
      if (this.question_type == 1) {
        this.isright = false;
        this.iswrong = false;
        document.getElementById("option1").classList.remove("input-correct-ans")
        document.getElementById("option1").classList.remove("input-incorrect-ans")
        document.getElementById("option2").classList.remove("input-correct-ans")
        document.getElementById("option2").classList.remove("input-incorrect-ans")

        this.selectedOption = id;
        this.selectedItem = item
        this.selected = i
      }
      else {
        this.isright = false;
        this.iswrong = false;
        document.getElementById("option1").classList.remove("input-correct-ans")
        document.getElementById("option1").classList.remove("input-incorrect-ans")
        document.getElementById("option2").classList.remove("input-correct-ans")
        document.getElementById("option2").classList.remove("input-incorrect-ans")
        document.getElementById("option3").classList.remove("input-correct-ans")
        document.getElementById("option3").classList.remove("input-incorrect-ans")
        document.getElementById("option4").classList.remove("input-correct-ans")
        document.getElementById("option4").classList.remove("input-incorrect-ans")
        this.selectedOption = id;
        this.selectedItem = item
        this.selected = i


      }
    }

    else{
       this.selectedOption = id;
        this.selectedItem = item
        this.selected = i

        
    }





    document.getElementById('option1')

  }
  GetQuizActivityFun() {
    this.security.GetQuizActivity(this.task_id).subscribe(res => {
      if (res.status == 200) {
        if (res.success) {
          this.question = res.data.quizdata.question;
          this.question_type = res.data.quizdata.question_type;
          this.questionOption1 = res.data.quizdata.option1;
          this.questionOption2 = res.data.quizdata.option2;
          this.questionOption3 = res.data.quizdata.option3;
          this.questionOption4 = res.data.quizdata.option4;
          this.is_answer_check = res.data.quizdata.is_answer_check;
          localStorage.setItem("campaign_id",res.data.quizdata.campaign_id);
    
          if (this.question_type == 1) {
            this.questions.push(this.questionOption1);
            this.questions.push(this.questionOption2);

          }
          else {
            this.questions.push(this.questionOption1);
            this.questions.push(this.questionOption2);
            this.questions.push(this.questionOption3);
            this.questions.push(this.questionOption4);
          }

          this.task_question_id = res.data.quizdata.task_question_id;
          this.subtask_question_id = res.data.quizdata.subtask_question_id;
          this.right_answer = res.data.quizdata.answer;
          this.succ_points = res.data.quizdata.success_points;
          this.has_subtask = res.data.quizdata.has_subtask;

          this.quiz_type = res.data.quizdata.quiz_type;
          this.is_subtask = false;

          if (this.is_answer_check) {
            if (res.data.quizdata.answer == "option1") {
              this.questionAnsOption1 = true;
            }
            if (res.data.quizdata.answer == "option2") {
              this.questionAnsOption2 = true;
            }
            if (res.data.quizdata.answer == "option3") {
              this.questionAnsOption3 = true;
            }
            if (res.data.quizdata.answer == "option4") {
              this.questionAnsOption4 = true;
            }
          }
          return;
        }
        else {
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
        }
      }
      else {
        this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
      }
    }, err => {
      console.error("err==", err);
      alert("err==" + JSON.stringify(err));
    });
  }

  OptionBtn1() {
    var element = document.getElementById("options1");
    var element1 = document.getElementById("options2");
    this.correct_option = "option1";
    element1.classList.remove("selected");
    element.classList.add("selected");
    // if(this.is_answer_check) {
    //   if(this.questionAnsOption1) {
    //     element.classList.add("selected");
    //     this.CorrectAnswer=true;
    //     this.CorrectAnswerOption="options1";
    //   }
    //   else {
    //     element.classList.add("input-incorrect-ans");
    //     this.CorrectAnswer=false;
    //   }
    // }
    // else {
    //   if(this.PreviousOption!=null) {
    //     var elementPre = document.getElementById(this.PreviousOption);
    //     elementPre.classList.add("input-ans");
    //   }
    //   this.PreviousOption="options1"; 
    //   element.classList.add("input-correct-ans");
    //   this.CorrectAnswer=true;
    //   this.CorrectAnswerOption="options1";
    // }
  }

  OptionBtn2() {
    var element = document.getElementById("options2");
    var element1 = document.getElementById("options1");
    this.correct_option = "option2";
    element1.classList.remove("selected");
    element.classList.add("selected");
    //   var element = document.getElementById("options2");
    //   element.classList.remove("input-ans");
    //   if(this.is_answer_check) {
    //   if(this.questionAnsOption2) {
    //     element.classList.add("selected");
    //     this.CorrectAnswer=true;
    //     this.CorrectAnswerOption="options2";
    //   }
    //   else {
    //     element.classList.add("input-incorrect-ans");
    //     this.CorrectAnswer=false;
    //   }
    // }
    // else {
    //   if(this.PreviousOption!=null) {
    //     var elementPre = document.getElementById(this.PreviousOption);
    //     elementPre.classList.add("input-ans");
    //   }
    //   this.PreviousOption="options2"; 
    //   element.classList.add("input-correct-ans");
    //   this.CorrectAnswer=true;
    //   this.CorrectAnswerOption="options2";
    // }
  }

  // OptionBtn3() {
  //   var element = document.getElementById("option3");
  //   element.classList.remove("input-ans");
  //   if(this.is_answer_check) {
  //   if(this.questionAnsOption3) {
  //     element.classList.add("input-correct-ans");
  //     this.CorrectAnswer=true;
  //     this.CorrectAnswerOption="option3";
  //   }
  //   else {
  //     element.classList.add("input-incorrect-ans");
  //     this.CorrectAnswer=false;
  //   }
  // }
  // else {
  //   if(this.PreviousOption!=null) {
  //     var elementPre = document.getElementById(this.PreviousOption);
  //     elementPre.classList.add("input-ans");
  //   }
  //   this.PreviousOption="option3"; 
  //   element.classList.add("input-correct-ans");
  //   this.CorrectAnswer=true;
  //   this.CorrectAnswerOption="option3";
  // }
  // }

  // OptionBtn4() {
  //   var element = document.getElementById("option4");
  //   element.classList.remove("input-ans");
  //   if(this.is_answer_check) {
  //   if(this.questionAnsOption4) {
  //     element.classList.add("input-correct-ans");
  //     this.CorrectAnswer=true;
  //     this.CorrectAnswerOption="option4";
  //   }
  //   else {
  //     element.classList.add("input-incorrect-ans");
  //     this.CorrectAnswer=false;
  //   }
  // }
  // else {
  //   if(this.PreviousOption!=null) {  
  //     var elementPre = document.getElementById(this.PreviousOption);
  //     elementPre.classList.add("input-ans");
  //   }
  //   this.PreviousOption="option4"; 
  //   element.classList.add("input-correct-ans");
  //   this.CorrectAnswer=true;
  //   this.CorrectAnswerOption="option4";
  // }
  // }
  getsubtaskactivity() {
    this.subtask_ques = false;
    this.security.GetSubtaskActivity(this.task_id).subscribe(res => {
      if (res.status == 200) {
        this.subtask_ques = true;
        if (res.success) {
          this.isright=false;
          
          this.question = res.data.question;
          this.question_type = res.data.question_type;
          this.questionOption1 = res.data.option1;
          this.questionOption2 = res.data.option2;
          this.questionOption3 = res.data.option3;
          this.questionOption4 = res.data.option4;
          this.is_answer_check = res.data.is_answer_check;
    
          if (this.question_type == 1) {
            this.questions=[];
            
            this.questions.push(this.questionOption1);
            this.questions.push(this.questionOption2);

          }
          else {
            this.question=[];
           
            this.questions.push(this.questionOption1);
            this.questions.push(this.questionOption2);
            this.questions.push(this.questionOption3);
            this.questions.push(this.questionOption4);
          }

          this.task_question_id = res.data.task_question_id;
          this.subtask_question_id = res.data.subtask_question_id;
          this.right_answer = res.data.answer;
          this.succ_points = res.data.success_points;
          this.has_subtask = res.data.has_subtask;

          this.quiz_type = res.data.quiz_type;
          this.is_subtask = false;
          

          if (this.is_answer_check) {
            if (res.data.answer == "option1") {
              this.questionAnsOption1 = true;
            }
            if (res.data.answer == "option2") {
              this.questionAnsOption2 = true;
            }
            if (res.data.answer == "option3") {
              this.questionAnsOption3 = true;
            }
            if (res.data.answer == "option4") {
              this.questionAnsOption4 = true;
            }
          }
          return;
          // console.log(res);
          // this.question = res.data.question;
          // this.questionOption1 = res.data.option1;
          // this.questionOption2 = res.data.option2;
          // this.questionOption3 = res.data.option3;
          // this.questionOption4 = res.data.option4;
          // this.is_answer_check = res.data.is_answer_check;

          // this.task_question_id = res.data.task_question_id;
          // this.subtask_question_id = res.data.subtask_question_id;
          // this.quiz_type = res.data.quiz_type;
          // this.is_subtask = true;

          // if (this.is_answer_check) {
          //   if (res.data.answer == "option1") {
          //     this.questionAnsOption1 = true;
          //   }
          //   if (res.data.answer == "option2") {
          //     this.questionAnsOption2 = true;
          //   }
          //   if (res.data.answer == "option3") {
          //     this.questionAnsOption3 = true;
          //   }
          //   if (res.data.answer == "option4") {
          //     this.questionAnsOption4 = true;
          //   }
          // }

        }
        else {
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
        }
      }
      else {
        this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
      }
    }, err => {
      console.error("err==", err);
      alert("err==" + JSON.stringify(err));
    });
  }

  // getsubtaskactivity() {
  //   this.subtask_ques = false;
  //   this.security.GetSubtaskActivity(this.task_id).subscribe(res => {
  //     if (res.status == 200) {
  //       this.subtask_ques = true;
  //       if (res.success) {
  //         console.log(res);
  //         this.question = res.data.question;
  //         this.questionOption1 = res.data.option1;
  //         this.questionOption2 = res.data.option2;
  //         this.questionOption3 = res.data.option3;
  //         this.questionOption4 = res.data.option4;
  //         this.is_answer_check = res.data.is_answer_check;

  //         this.task_question_id = res.data.task_question_id;
  //         this.subtask_question_id = res.data.subtask_question_id;
  //         this.quiz_type = res.data.quiz_type;
  //         this.is_subtask = true;

  //         if (this.is_answer_check) {
  //           if (res.data.answer == "option1") {
  //             this.questionAnsOption1 = true;
  //           }
  //           if (res.data.answer == "option2") {
  //             this.questionAnsOption2 = true;
  //           }
  //           if (res.data.answer == "option3") {
  //             this.questionAnsOption3 = true;
  //           }
  //           if (res.data.answer == "option4") {
  //             this.questionAnsOption4 = true;
  //           }
  //         }

  //         return;
  //       }
  //       else {
  //         this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
  //       }
  //     }
  //     else {
  //       this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
  //     }
  //   }, err => {
  //     console.error("err==", err);
  //     alert("err==" + JSON.stringify(err));
  //   });
  // }

  NextBtn1() {
    var element = document.getElementById(this.selectedOption)
    if (element == null || element == undefined) {
      console.log("hi")
    }
    else {
      if (this.right_answer!= null  || this.right_answer != undefined) {
        if (this.right_answer == this.selectedOption) {
          this.isright = true;
          this.disabled = true;
          element.classList.remove("selected")
          element.classList.add("input-correct-ans");
          this.CorrectAnswerOption = this.selectedOption;
             this.security.DoTaskActivity(this.task_id, this.is_subtask, this.quiz_type, this.task_question_id, this.CorrectAnswerOption).subscribe(res => {
              if (res.status == 200) {
                if (res.success) {
                  this.questions.pop();
                  this.wof_enable = res.data.wof_enable;
                  let points = res.data.quizdata.success_points;
                  // if (this.subtask_question_id != 0) {
                  //   this.GetTaskList = false;
                  //   this.GetTaskSub = true;
                  //   document.getElementById("option1").classList.add("input-ans");
                  //   document.getElementById("option2").classList.add("input-ans");
                  //   document.getElementById("option3").classList.add("input-ans");
                  //   document.getElementById("option4").classList.add("input-ans");
    
    
                  // }
    
                  if (this.has_subtask == false && this.wof_enable == false) {
                    // let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:'You have won', points:this.succ_points }); 
                    // modal.onDidDismiss((views) => {  
                    //   if(res.data.wof_enable) {
                    //     this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3)); 
                    //     modal.present();
                    //     this.navCtrl.setRoot(FortunewheelPage,{LandQuestion:true});  
                    let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr.course_title, points: points });
                    modal.onDidDismiss((views) => {
                      // this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true}); 
                      this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true}); 
                      //this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 4));
                    });
                    modal.present();
                  }
                  else if (this.has_subtask == true) {
                    
                    this.GetTaskSub = true;
                    //this.navCtrl.pop();
                    this.getsubtaskactivity();
                  }
                  else if (this.wof_enable == true) {
                    //this.navCtrl.pop();
                    let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: points });
                    modal.onDidDismiss((views) => {
                      this.navCtrl.setRoot(FortunewheelPage, { LandQuestion: true });
                    });
                    modal.present();
                  }
                  return;
                }
                else {
                  this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
                }
              }
              else {
                this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
              }
            }, err => {
              console.error("err==", err);
              alert("err==" + JSON.stringify(err));
            });
        }
          else {
        element.classList.remove("selected")
        element.classList.add("input-incorrect-ans")
        this.iswrong = true
      }

      }
      else{   
        this.CorrectAnswerOption = this.selectedOption;
        this.security.DoTaskActivity(this.task_id, this.is_subtask, this.quiz_type, this.task_question_id, this.CorrectAnswerOption).subscribe(res => {
          if (res.status == 200) {
            if (res.success) {

              this.wof_enable = res.data.wof_enable;
              let points = res.data.quizdata.success_points;
              // if (this.subtask_question_id != 0) {
              //   this.GetTaskList = false;
              //   this.GetTaskSub = true;
              //   document.getElementById("option1").classList.add("input-ans");
              //   document.getElementById("option2").classList.add("input-ans");
              //   document.getElementById("option3").classList.add("input-ans");
              //   document.getElementById("option4").classList.add("input-ans");


              // }

              if (this.has_subtask == false && this.wof_enable == false) {
                // let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:'You have won', points:this.succ_points }); 
                // modal.onDidDismiss((views) => {  
                //   if(res.data.wof_enable) {
                //     this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3)); 
                //     modal.present();
                //     this.navCtrl.setRoot(FortunewheelPage,{LandQuestion:true});  
                let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr.course_title, points: points });
                modal.onDidDismiss((views) => {
                  // this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true}); 
                  this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true}); 
                  //this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 4));
                });
                modal.present();
              }
              else if (this.has_subtask == true) {
                this.GetTaskSub = true;
                //this.navCtrl.pop();
                this.getsubtaskactivity();

              }
              else if (this.wof_enable == true) {
                //this.navCtrl.pop();
                let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: points });
                modal.onDidDismiss((views) => {
                  this.navCtrl.setRoot(FortunewheelPage, { LandQuestion: true });
                });
                modal.present();
              }
              return;
            }
            else {
              this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
            }
          }
          else {
            this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
          }
        }, err => {
          console.error("err==", err);
          alert("err==" + JSON.stringify(err));
        });
  
      }
      // if (this.right_answer == this.selectedOption) {
      //   this.isright = true;
      //   this.disabled = true;
      //   element.classList.remove("selected")
      //   element.classList.add("input-correct-ans");
      //   this.CorrectAnswerOption = this.selectedOption;
      //   this.security.DoTaskActivity(this.task_id, this.is_subtask, this.quiz_type, this.task_question_id, this.CorrectAnswerOption).subscribe(res => {
      //     if (res.status == 200) {
      //       if (res.success) {

      //         this.wof_enable = res.data.wof_enable;
      //         let points = res.data.quizdata.success_points;
      //         if (this.subtask_question_id != 0) {
      //           this.GetTaskList = false;
      //           this.GetTaskSub = true;
      //           document.getElementById("option1").classList.add("input-ans");
      //           document.getElementById("option2").classList.add("input-ans");
      //           document.getElementById("option3").classList.add("input-ans");
      //           document.getElementById("option4").classList.add("input-ans");


      //         }

      //         if (this.has_subtask == false && this.wof_enable == false) {
      //           // let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:'You have won', points:this.succ_points }); 
      //           // modal.onDidDismiss((views) => {  
      //           //   if(res.data.wof_enable) {
      //           //     this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3)); 
      //           //     modal.present();
      //           //     this.navCtrl.setRoot(FortunewheelPage,{LandQuestion:true});  
      //           let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr.course_title, points: points });
      //           modal.onDidDismiss((views) => {
      //             // this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true}); 
      //             this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 4));
      //           });
      //           modal.present();
      //         }
      //         else if (this.has_subtask == true) {
      //           //this.navCtrl.pop();
      //           this.getsubtaskactivity();
      //         }
      //         else if (this.wof_enable == true) {
      //           //this.navCtrl.pop();
      //           let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: points });
      //           modal.onDidDismiss((views) => {
      //             this.navCtrl.setRoot(FortunewheelPage, { LandQuestion: true });
      //           });
      //           modal.present();
      //         }
      //         return;
      //       }
      //       else {
      //         this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
      //       }
      //     }
      //     else {
      //       this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
      //     }
      //   }, err => {
      //     console.error("err==", err);
      //     alert("err==" + JSON.stringify(err));
      //   });
      // }
      // else {
      //   element.classList.remove("selected")
      //   element.classList.add("input-incorrect-ans")
      //   this.iswrong = true
      // }

    }

  }

  NextBtn2() {
    
    var element = document.getElementById(this.selectedOption);
    this.CorrectAnswerOption = this.selectedOption;
    if (element == null || element == undefined) {
      console.log("hi")
    }
    else {
    
      if (this.right_answer!= null  || this.right_answer != undefined) {
        if (this.right_answer == this.selectedOption) {
          this.isright = true;
          this.disabled = true;
          element.classList.remove("selected")
          element.classList.add("input-correct-ans");
      
          this.disabled1 = true
          this.security.DoTaskActivity(this.task_id, this.is_subtask, this.quiz_type, this.subtask_question_id, this.CorrectAnswerOption).subscribe(res => {
            if (res.status == 200) {
              if (res.success) {
                let wof_eb = res.data.wof_enable;
    
                if (wof_eb == true) {
                  let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: this.succ_points });
                  modal.onDidDismiss((views) => {
                    this.navCtrl.setRoot(FortunewheelPage, { LandQuestion: true });
                  });
                  modal.present();
                }
                else {
                  let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: this.succ_points });
                  modal.onDidDismiss((views) => {
                    //this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true}); 
                    this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true});  
                   // this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 4));
                  });
                  modal.present();
                }
                //   let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:'You have won', points:this.succ_points }); 
                //   modal.onDidDismiss((views) => {  
                //     if(res.data.wof_enable) {
                //       this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3)); 
                //       modal.present();
                //       this.navCtrl.setRoot(FortunewheelPage,{LandQuestion:true});  
                //     }
    
                //   else {
    
                //    let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:this.subtask_question_id, points:res.data.points }); 
                //    modal.onDidDismiss((views) => {   
                //      this.navCtrl.setRoot(DashboardPage,{LandQuestion:true});    
                //    });  
                //    modal.present();
                //   }
    
                //   });  
                // } 
              }
              else {
                this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
              }
            }
            else {
              this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
            }
          }, err => {
            console.error("err==", err);
            alert("err==" + JSON.stringify(err));
          });

        }
        else {
          element.classList.remove("selected")
          element.classList.add("input-incorrect-ans")
          this.iswrong = true
        }
        
      }

      else{

        this.disabled1 = true
        this.security.DoTaskActivity(this.task_id, this.is_subtask, this.quiz_type, this.subtask_question_id, this.CorrectAnswerOption).subscribe(res => {
          if (res.status == 200) {
            if (res.success) {
              let wof_eb = res.data.wof_enable;
  
              if (wof_eb == true) {
                let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: this.succ_points });
                modal.onDidDismiss((views) => {
                  this.navCtrl.setRoot(FortunewheelPage, { LandQuestion: true });
                });
                modal.present();
              }
              else {
                let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: this.succ_points });
                modal.onDidDismiss((views) => {
                  //this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true});  
                 // this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 4));
                 this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true}); 
                });
                modal.present();
              }
              //   let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:'You have won', points:this.succ_points }); 
              //   modal.onDidDismiss((views) => {  
              //     if(res.data.wof_enable) {
              //       this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3)); 
              //       modal.present();
              //       this.navCtrl.setRoot(FortunewheelPage,{LandQuestion:true});  
              //     }
  
              //   else {
  
              //    let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:this.subtask_question_id, points:res.data.points }); 
              //    modal.onDidDismiss((views) => {   
              //      this.navCtrl.setRoot(DashboardPage,{LandQuestion:true});    
              //    });  
              //    modal.present();
              //   }
  
              //   });  
              // } 
            }
            else {
              this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
            }
          }
          else {
            this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
          }
        }, err => {
          console.error("err==", err);
          alert("err==" + JSON.stringify(err));
        });

      }


    }

  }
  // NextBtn2() {

  //   if (this.correct_option == undefined || this.correct_option == null) {
  //     console.log("hi");
  //   }
  //   else {
  //     this.disabled1 = true
  //     this.security.DoTaskActivity(this.task_id, this.is_subtask, this.quiz_type, this.subtask_question_id, this.correct_option).subscribe(res => {
  //       if (res.status == 200) {
  //         if (res.success) {
  //           let wof_eb = res.data.wof_enable;

  //           if (wof_eb == true) {
  //             let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: this.succ_points });
  //             modal.onDidDismiss((views) => {
  //               this.navCtrl.setRoot(FortunewheelPage, { LandQuestion: true });
  //             });
  //             modal.present();
  //           }
  //           else {
  //             let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: this.succ_points });
  //             modal.onDidDismiss((views) => {
  //               //this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true});  
  //               this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 4));
  //             });
  //             modal.present();
  //           }
  //           //   let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:'You have won', points:this.succ_points }); 
  //           //   modal.onDidDismiss((views) => {  
  //           //     if(res.data.wof_enable) {
  //           //       this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3)); 
  //           //       modal.present();
  //           //       this.navCtrl.setRoot(FortunewheelPage,{LandQuestion:true});  
  //           //     }

  //           //   else {

  //           //    let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:this.subtask_question_id, points:res.data.points }); 
  //           //    modal.onDidDismiss((views) => {   
  //           //      this.navCtrl.setRoot(DashboardPage,{LandQuestion:true});    
  //           //    });  
  //           //    modal.present();
  //           //   }

  //           //   });  
  //           // } 
  //         }
  //         else {
  //           this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
  //         }
  //       }
  //       else {
  //         this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
  //       }
  //     }, err => {
  //       console.error("err==", err);
  //       alert("err==" + JSON.stringify(err));
  //     });
  //   }

  // }
 

  // NextBtn2() {

  //   if (this.correct_option == undefined || this.correct_option == null) {
  //     console.log("hi");
  //   }
  //   else {
  //     this.disabled1 = true
  //     this.security.DoTaskActivity(this.task_id, this.is_subtask, this.quiz_type, this.subtask_question_id, this.correct_option).subscribe(res => {
  //       if (res.status == 200) {
  //         if (res.success) {
  //           let wof_eb = res.data.wof_enable;

  //           if (wof_eb == true) {
  //             let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: this.succ_points });
  //             modal.onDidDismiss((views) => {
  //               this.navCtrl.setRoot(FortunewheelPage, { LandQuestion: true });
  //             });
  //             modal.present();
  //           }
  //           else {
  //             let modal = this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr: this.SelectContentArr, points: this.succ_points });
  //             modal.onDidDismiss((views) => {
  //               //this.navCtrl.setRoot(ActivitiesPage,{LandQuestion:true});  
  //               this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 4));
  //             });
  //             modal.present();
  //           }
  //           //   let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:'You have won', points:this.succ_points }); 
  //           //   modal.onDidDismiss((views) => {  
  //           //     if(res.data.wof_enable) {
  //           //       this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3)); 
  //           //       modal.present();
  //           //       this.navCtrl.setRoot(FortunewheelPage,{LandQuestion:true});  
  //           //     }

  //           //   else {

  //           //    let modal=this.modalCtrl.create(YouhaverecvdPage, { SelectContentArr:this.subtask_question_id, points:res.data.points }); 
  //           //    modal.onDidDismiss((views) => {   
  //           //      this.navCtrl.setRoot(DashboardPage,{LandQuestion:true});    
  //           //    });  
  //           //    modal.present();
  //           //   }

  //           //   });  
  //           // } 
  //         }
  //         else {
  //           this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
  //         }
  //       }
  //       else {
  //         this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present(); return;
  //       }
  //     }, err => {
  //       console.error("err==", err);
  //       alert("err==" + JSON.stringify(err));
  //     });
  //   }

  // }

  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }

  ionViewWillLeave() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionsPage');
  }

}