import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';
import { DomSanitizer,SafeResourceUrl } from "@angular/platform-browser";
import { QuiztaskPage } from '../quiztask/quiztask'; 
import { LoginPage } from '../login/login';
import { SecurityProvider } from '../../providers/security/security';

/**
 * Generated class for the Moduledetails1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-moduledetails1',
  templateUrl: 'moduledetails1.html',
})
export class Moduledetails1Page {

  SelectContentArr:any;
  VideoUrl:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,public sanitizer: DomSanitizer,public security:SecurityProvider,public toastController : ToastController,public alertCtrl:AlertController) {

  }
  ngOnInit() {
  this.SelectContentArr=this.navParams.get("SelectContentArr");
  console.log("this.SelectContentArr==",this.SelectContentArr);
  if(this.SelectContentArr.course_content==null) { 
    this.VideoUrl="";      
  }
  else {
    this.VideoUrl=this.SelectContentArr.course_content; 
  } 


  let self = this;
  var aud = document.getElementById("myMovie");
  aud.onended = function(): any {
      self.NextPage(); 
  }

/*
var video = <HTMLVideoElement>document.getElementById("myMovie");
var supposedCurrentTime = 0; 
video.addEventListener('timeupdate', function() {
  if (!video.seeking) {
        supposedCurrentTime = video.currentTime;
  }
});
video.addEventListener('seeking', function() {
  var delta = video.currentTime - supposedCurrentTime;
  if (Math.abs(delta) > 0.01) {
    console.log("Seeking is disabled");
    video.currentTime = supposedCurrentTime;
  }
});
video.addEventListener('ended', function() {
    supposedCurrentTime = 0;
});
*/ 


/*
  var player;
  //window['onYouTubeIframeAPIReady'] = function() {
    player = new window['YT'].Player('video', {
      height: '360',
      width: '640',
      videoId: 'xxCVVQUBWTo',
      events: {
        //'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
  //} 
  function onPlayerStateChange(event) {
    if (event.data == window['YT'].PlayerState.PAUSED) {
      console.log(player.getCurrentTime());
    }
    switch(event.data) {
      case 0:
        console.log('video ended');
        break;
      case 1:
        console.log('video playing from '+player.getCurrentTime());
        break;
      case 2:
        console.log('video paused at '+player.getCurrentTime());
        break;
    }
  } 
  function onPlayerReady(event) { 
    document.getElementById("pause").addEventListener("click", function() {
      player.pauseVideo();
    });
    document.getElementById("play").addEventListener("click", function() {
      player.playVideo();
    });
  }
  if(!window['YT']) {
    var tag = document.createElement('script');
    tag.src = "//www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }
*/
  
}

  NextPage() { 
    let alert = this.alertCtrl.create({
      title: '',  
      subTitle: 'You are done watching the video ! Click here to take the quiz',
      cssClass: 'VideoPlayMod',
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            this.closeBtn(); 
            this.navCtrl.push(QuiztaskPage,{course_id:this.SelectContentArr.course_id,SelectContentArr:this.SelectContentArr}); 
          }
        }
      ],
      enableBackdropDismiss:false,
    });
    alert.present();
    // this.security.CourseQuiz(this.SelectContentArr.course_id).subscribe(res => { 
    //   if(res.status==200) {  
    //     if(res.success) { 
    //       if(res.data.course_quiz_details.length!=0) { 
    //         this.closeBtn(); 
    //         this.navCtrl.push(QuiztaskPage,{course_id:this.SelectContentArr.course_id,SelectContentArr:this.SelectContentArr}); 
    //         return; 
    //       } 
    //     }    
    //   } 
    //   else {  
    //     this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();  
    //     return; 
    //   }
    // }, err => { 
    //   if(err.status==401) {    
    //     this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
    //     localStorage.clear();    
    //     this.navCtrl.setRoot(LoginPage);
    //     return; 
    //   }  
    //   if(err.status==422) {     
    //     this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
    //     return; 
    //   } 
    //   this.toastController.create({ message: "Please check your internet connection", duration: 5000, position: 'bottom' }).present();
    //   return;  
    // }); 
  }
  
  closeBtn() {     
      this.viewCtrl.dismiss(true);  
  }

  ionViewDidLoad() {
      console.log('ionViewDidLoad Moduledetails1Page');
  }
}