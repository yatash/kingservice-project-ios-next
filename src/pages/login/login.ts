import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController, Platform, AlertController } from 'ionic-angular'; 
import { SecurityProvider } from '../../providers/security/security';
import { OtpcodePage } from '../otpcode/otpcode';
import { LoyaltyPage } from '../loyalty/loyalty';
import { CreatepasswordPage } from '../createpassword/createpassword'; 

import { FormBuilder,AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';

declare var $: any;
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({ 
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage { 

  slideOneForm: FormGroup;
  // nameUsr: AbstractControl;

  language  
  username 
  signuplogin:boolean=false;
  LoyalityArr:any;
  program_id:any=1;
  user_role_id:any=null;
  source_from:any=2;
  Titlehead:any=null;
  mobMD
  heading: string="Earn Loyality Rewards Points and redeem them to win Exciting Prizes !";
  LoginLand:any;

  buttonDisabled:boolean=true;
  buttonDisable:boolean=true;

  BtnHide:boolean=true;
  BtnShow:boolean=false;

  constructor(public navCtrl: NavController, public navParams: NavParams,public event:Events, public security:SecurityProvider, public toastController : ToastController,public platform: Platform,public alertCtrl:AlertController, public formBuilder:FormBuilder) {

    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    // this.myForm = new FormGroup({
    //   name: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(30)]),
    // });

    // this.slideOneForm = formBuilder.group({
    //   firstName: new FormControl('',[Validators.required,Validators.minLength(10),Validators.maxLength(10)]), 
    // });

    this.slideOneForm = formBuilder.group({ 
      firstName: new FormControl('',[Validators.required]), 
    });

    // this.nameUsr=this.myForm.contains['nameUsr'];

    this.platform.ready().then((platsrc) => { 
      if (platsrc=='dom') { 
        this.source_from=2; 
      }
      else if(platsrc=='cordova') { 
        this.source_from=2; 
      } 
      if (platsrc=='ios') {
        this.source_from=3;
      }

      this.platform.registerBackButtonAction(() => { 
        let alert=this.alertCtrl.create({
          message: 'Are you sure you want to exit?',
          buttons: [
          {
          text: 'NO',
          role: 'cancel',
          handler: () => {
          }
          },
          {
          text: 'YES',
          handler: () => {
          platform.exitApp();
          }
          }
          ]
          });
          alert.present();  
      },1);

    });
    this.LoginLand=this.navParams.get("LoginLand");



    //if(this.LoginLand != undefined) {
      // this.platform.ready().then(() => { 
      //   document.addEventListener('backbutton', () => {
      //    if(this.navCtrl.canGoBack()) {
      //     let alert=this.alertCtrl.create({
      //     title: 'Exit?',
      //     message: 'Do you want to exit the app?',
      //     buttons: [
      //     {
      //     text: 'Cancel',
      //     role: 'cancel',
      //     handler: () => {
      //     }
      //     },
      //     {
      //     text: 'Exit',
      //     handler: () => {
      //     platform.exitApp();
      //     }
      //     }
      //     ]
      //     });
      //     alert.present();
      //      return;
      //    }
      //    this.navCtrl.pop();
      //  }, false);
      // });
    //} 
  }

  get errorControl() {
    if(this.slideOneForm.controls.firstName.value!=undefined) {
      let Errors=this.slideOneForm.controls.firstName.errors;
      if(Errors== null) { 
        this.buttonDisabled=false;
      }
      else {
        this.buttonDisabled=true;
      }
    }
    return this.slideOneForm.controls;
  }

  isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }
  MobCheck(vale) { 
    if(vale.target.value.length==10) {
     return false;
    }
  } 
  onKeypress(vale) {  
    console.log("vale=",vale);
  }
  onKeyUp(event) {
    //this.buttonDisabled=false;  
  }
  public changeLanguage() : void {
    this.event.publish('user:created',this.language,Date.now());
    localStorage.setItem("applanguage",this.language);
  }  
  loginbtn() { 
    this.BtnHide=false;
    this.BtnShow=true;
      if(this.username=="" || this.username==null)  {
        this.toastController.create({ message: 'Oops! Please enter a valid mobile number.', duration: 5000, position: 'bottom' }).present();
        return;
      } 
      if(this.username.length<10)  {
        this.toastController.create({ message: "Oops! Please enter a valid mobile number.", duration: 5000, position: 'bottom' }).present();
        return;
      }
      if(this.username.length>10)  {
        this.toastController.create({ message: "Oops! Please enter a valid mobile number.", duration: 5000, position: 'bottom' }).present();
        return;
      }
    this.security.login(this.username,this.program_id,this.source_from).subscribe(res =>{ 
          if(res.status==200) { 
            if(res.success){
              if(res.data.ispin) { 
                localStorage.setItem("user_id",res.data.user_id);
                localStorage.setItem("program_id",this.program_id);
                this.navCtrl.push(CreatepasswordPage,{ mobotp:res.data.otp,program_id:this.program_id,user_id:res.data.user_id,mobile_no:this.username,loginotpcode:true });
                return;
              }
              else {  
                this.toastController.create({ message: "OTP has been sent on your mobile number", duration: 5000, position: 'bottom' }).present();
                localStorage.setItem("program_id",this.program_id); 
                localStorage.setItem("mobile_no",res.data.mobile_no);
                localStorage.setItem("user_id",res.data.user_id);
                this.navCtrl.push(OtpcodePage,{ mobotp:res.data.otp,program_id:this.program_id,user_id:res.data.user_id,mobile_no:res.data.mobile_no }); 
              }
            }
           }
      }, err => { 
        this.BtnHide=true;
        this.BtnShow=false; 
        if(err.status==401) {   
          this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
          return; 
       }
       if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      if(err.status==500) {
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return;  
      } 
       this.toastController.create({ message: "No internet connection", duration: 5000, position: 'bottom' }).present();
      console.error("err==",err); 
    }); 
    
   }
   RetailerBtn() {
    this.navCtrl.setRoot(LoyaltyPage);  
   }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
}