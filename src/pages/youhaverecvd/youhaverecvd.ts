import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the YouhaverecvdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-youhaverecvd',
  templateUrl: 'youhaverecvd.html',
})
export class YouhaverecvdPage {
  CourseName:any;
  SelectContentArr:any
  points
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl:ViewController) {
    this.SelectContentArr=this.navParams.get("SelectContentArr");
    this.points=this.navParams.get("points");
    this.CourseName=this.SelectContentArr.course_title;
  }

  CloseModal() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YouhaverecvdPage');
  }

}
