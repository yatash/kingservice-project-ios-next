import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';

import {FinishedLeaderboardPage} from '../finished-leaderboard/finished-leaderboard';
import {SecurityProvider} from '../../providers/security/security'


/**
 * Generated class for the LeadersboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leadersboard',
  templateUrl: 'leadersboard.html',
})
export class LeadersboardPage implements OnInit {
data:any;
datas:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider,public platform:Platform,public alertCtrl:AlertController) {
    
  }
  ngOnInit() {   
    this.data=this.navParams.get("data");
    this.security.leaderboard(this.data).subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
          console.log(res)

          this.datas= res.data;
          console.log(this.datas)
        } 
   
      } 
 
    }, err => { 
      console.error("err==",err); 
      alert("err=="+JSON.stringify(err)); 
    });
   
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LeadersboardPage');
  }
  leader(){
    this.navCtrl.setRoot(FinishedLeaderboardPage);  
  }
  goBack() {
    console.log("Going back");
    this.navCtrl.pop();
  }

}
