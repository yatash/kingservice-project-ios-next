import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivationsPage } from './activations';

@NgModule({
  declarations: [
    ActivationsPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivationsPage),
  ],
})
export class ActivationsPageModule {}
