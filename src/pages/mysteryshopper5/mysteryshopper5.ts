import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Mysteryshopper6Page } from '../mysteryshopper6/mysteryshopper6'; 

/**
 * Generated class for the Mysteryshopper5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper5',
  templateUrl: 'mysteryshopper5.html',
})
export class Mysteryshopper5Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  NextBtn() {
    this.navCtrl.push(Mysteryshopper6Page);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Mysteryshopper5Page');
  }

}
