import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { WelcomePage } from '../welcome/welcome'; 
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';


/**
 * Generated class for the TermsandconditionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-termsandconditions',
  templateUrl: 'termsandconditions.html',
})
export class TermsandconditionsPage { 
  termsCons:number=0;
  terms_conditions:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public toastController:ToastController,public security:SecurityProvider, private sanitized: DomSanitizer) { 
    this.terms_conditions= this.sanitized.bypassSecurityTrustHtml(this.navParams.get("terms_conditions"));
      let txttd="";
      console.log("terms_conditions=",this.terms_conditions); 
  }

  SaveBtn() {
    this.termsCons=1;
    this.security.TermsUpdate(this.termsCons).subscribe(res =>{
      if(res.status==200) { 
        if(res.success) { 
          this.navCtrl.push(WelcomePage,{intro_video_link:res.data.intro_video_link});
          return;
        }
       } 
       else {  
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'top' }).present();
          return;
      }
  }, err => { 
      console.error("err==",err); 
  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsandconditionsPage');
  }

}
