import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewardstorePage } from './rewardstore';

@NgModule({
  declarations: [
    RewardstorePage,
  ],
  imports: [
    IonicPageModule.forChild(RewardstorePage),
  ],
})
export class RewardstorePageModule {}
