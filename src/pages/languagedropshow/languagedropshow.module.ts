import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LanguagedropshowPage } from './languagedropshow';

@NgModule({
  declarations: [
    LanguagedropshowPage,
  ],
  imports: [
    IonicPageModule.forChild(LanguagedropshowPage),
  ],
})
export class LanguagedropshowPageModule {}
