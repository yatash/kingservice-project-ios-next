import { Component } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard'; 
import { MoredetailsPage } from '../moredetails/moredetails'; 
import { UsrprofilePage } from '../usrprofile/usrprofile'; 
import { RewardcategoryPage } from '../rewardcategory/rewardcategory';
import {PointsSummaryPage} from '../points-summary/points-summary'
//import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {  
    tab1Root=DashboardPage; 
    tab4Root=UsrprofilePage; 
    tab3Root=RewardcategoryPage; 
    tab2Root = PointsSummaryPage; 
    tabsHideOnSubPages: boolean=false;
  constructor(public event:Events, public navCtrl:NavController) { 

  }
    
  myMethod() {
    this.navCtrl.setRoot(DashboardPage);
  }
  
}
