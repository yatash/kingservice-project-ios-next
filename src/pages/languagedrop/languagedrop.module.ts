import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LanguagedropPage } from './languagedrop';

@NgModule({
  declarations: [
    LanguagedropPage,
  ],
  imports: [
    IonicPageModule.forChild(LanguagedropPage),
  ],
})
export class LanguagedropPageModule {}
