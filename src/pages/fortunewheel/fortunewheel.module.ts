import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FortunewheelPage } from './fortunewheel';

@NgModule({
  declarations: [
    FortunewheelPage,
  ],
  imports: [
    IonicPageModule.forChild(FortunewheelPage),
  ],
})
export class FortunewheelPageModule {}
