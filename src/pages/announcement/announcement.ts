import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AnnouncementsPage } from '../announcements/announcements'; 

/**
 * Generated class for the AnnouncementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-announcement',
  templateUrl: 'announcement.html',
})
export class AnnouncementPage {
  
  items=[{stores:"DMart Stores"},{stores:"DMart Stores"},{stores:"DMart Stores"},{stores:"DMart Stores"},{stores:"DMart Stores"},{stores:"DMart Stores"}
  ,{stores:"DMart Stores"},{stores:"DMart Stores"},{stores:"DMart Stores"},{stores:"DMart Stores"}];

  eligibilities=[{options:"Our market representative must have given you a manual to understand the onboarding process for customers."},
  {options:"If you have read the manual you are good to take this task."},{options:"You cannot skip any question and all question are mandatory."},
{options:"There is no negative marking for wrong answers."},{options:"If you have read the manual you are good to take this task. "},
{options:"You cannot skip any question and all question are mandatory. "},{options:"There is no negative marking for wrong answers."},
{options:"There is no negative marking for wrong answers."}];

terms=[{conditions:" Our market representative must have given you a manual to understand the onboarding process for customers."},
{conditions:"If you have read the manual you are good to take this task."},{conditions:"You cannot skip any question and all question are mandatory."},
{conditions:" There is no negative marking for wrong answers."},{conditions:"If you have read the manual you are good to take this task."},
{conditions:"You cannot skip any question and all question are mandatory."},{conditions:"There is no negative marking for wrong answers."},
{conditions:"There is no negative marking for wrong answers."}]
 

constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ClickBtn(txt){
    this.navCtrl.setRoot(AnnouncementsPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnnouncementPage');
  }

}
