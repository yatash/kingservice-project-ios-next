import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';

import { LoginPage } from '../login/login';
import { QuiztaskPage } from '../quiztask/quiztask'; 
import { YouhaverecvdPage } from '../youhaverecvd/youhaverecvd';

/**
 * Generated class for the ModuledetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moduledetails',
  templateUrl: 'moduledetails.html',
})
export class ModuledetailsPage {  
  SelectContentArr:any;
  total_quiz_count:any;
  course_quiz_details:any;
  CourseID:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController : ToastController,public modalCtrl:ModalController) {
    this.SelectContentArr=this.navParams.get("SelectContentArr");
    this.CourseID=this.SelectContentArr.course_id; 
  }

  backBtn() {
    this.navCtrl.pop();
  }

  StartQuizBtn() { 
    this.navCtrl.push(QuiztaskPage,{course_id:this.CourseID,SelectContentArr:this.SelectContentArr});     
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModuledetailsPage');
  }

}
