import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { Mysteryshopper7Page } from '../mysteryshopper7/mysteryshopper7'; 
import { Mysteryshopper1Page } from '../mysteryshopper1/mysteryshopper1';
import { SecurityProvider } from '../../providers/security/security';
import { LoginPage } from '../login/login';  

/**
 * Generated class for the Mysteryshopper6Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper6',
  templateUrl: 'mysteryshopper6.html',
})
export class Mysteryshopper6Page {
  outlet_datas:any;
  outlet_user_id:any;
  MysteryAuditData:any=[];
  ImgData1:boolean=true;
  ImgData2:boolean=true; 
  MysteryQuestRate:any=[];
  MysteryQuestType3:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public modalCtrl: ModalController, public toastController: ToastController) {
    this.outlet_datas=this.navParams.get("outlet_datas");
    if(this.outlet_datas == undefined) {
      //this.outlet_user_id=61170; 
    }
    else {
      this.outlet_user_id=this.outlet_datas.user_id;
    }
  }

  NextBtn() {  
    let modal=this.modalCtrl.create(Mysteryshopper7Page); 
    modal.onDidDismiss((views) => { 
      if(views) {
        this.navCtrl.setRoot(Mysteryshopper1Page);
      }    
    });  
    modal.present();
  }

  
  ionViewWillEnter() {
    this.GetQuizActivityFun();
  }

  GetQuizActivityFun() { 
    this.security.MysteryAuditSummary(this.outlet_user_id).subscribe(res => {
      if(res.status==200) { 
        if(res.success) { 
          if(!res.data.mystery_audit_complete) {
            let MysteryQuestOption=[]; 
            let MysteryQuest=[];
            let AuditNumber=1;
            let RateMysteryQest=1;
            for(let i=0;i<res.data.length;i++) {
                for(let subi=0;subi<res.data[i].mystery_questions.length;subi++) {
                  if(res.data[i].mystery_questions[subi].question_type==4) {
                    for(let subsubi=0;subsubi<res.data[i].mystery_questions[subi].mystery_question_options.length;subsubi++) {
                       if(res.data[i].mystery_questions[subi].mystery_question_options[subsubi].selected_answer=="true") { 
                        this.MysteryQuestRate.push({ name:"ios-star" });
                        RateMysteryQest=3; 
                       }
                       else {
                         if(RateMysteryQest==1) {
                          RateMysteryQest=2;
                         }  
                       }
                       if(RateMysteryQest==2) {
                        this.MysteryQuestRate.push({ name:"ios-star" });
                       }
                    }
                  }
                  if(res.data[i].mystery_questions[subi].question_type==3) {
                    this.MysteryQuestType3=res.data[i].mystery_questions[subi].mystery_question_options;
                  }

                  MysteryQuestOption=res.data[i].mystery_questions[subi].mystery_question_options.filter((item) => { return item.selected_answer=="true"; });

                  MysteryQuest.push({ mystery_question_id:res.data[i].mystery_questions[subi].mystery_question_id,question:res.data[i].mystery_questions[subi].question,question_type_string:res.data[i].mystery_questions[subi].question_type_string,question_type:res.data[i].mystery_questions[subi].question_type,sub_question:res.data[i].mystery_questions[subi].sub_question,no_of_options:res.data[i].mystery_questions[subi].no_of_options,sub_question_id:res.data[i].mystery_questions[subi].sub_question_id,mystery_question_options:MysteryQuestOption,MysteryQuestRate:this.MysteryQuestRate,MysteryQuestType3:this.MysteryQuestType3 });  
                  MysteryQuestOption=[]; 
                }
                this.MysteryAuditData.push({ template_id:res.data[i].template_id,template_name:res.data[i].template_name,group_id:res.data[i].group_id,group_name:res.data[i].group_name,mystery_questions:MysteryQuest });
                MysteryQuest=[];
            }
          }
        } 
        else { 
       }  
      } 
       else { 
      }
    }, err => { 
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
        return; 
      } 
      else {  
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return;  
      }
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Mysteryshopper6Page');
  }

}