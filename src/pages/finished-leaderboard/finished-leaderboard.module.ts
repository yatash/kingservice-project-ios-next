import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinishedLeaderboardPage } from './finished-leaderboard';

@NgModule({
  declarations: [
    FinishedLeaderboardPage,
  ],
  imports: [
    IonicPageModule.forChild(FinishedLeaderboardPage),
  ],
})
export class FinishedLeaderboardPageModule {}
