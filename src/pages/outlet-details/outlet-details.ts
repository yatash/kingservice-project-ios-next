import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActivationsPage } from '../activations/activations'; 

/**
 * Generated class for the OutletDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-outlet-details',
  templateUrl: 'outlet-details.html',
})
export class OutletDetailsPage {
  items=['Select store'];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  backBtn(){
    this.navCtrl.setRoot(ActivationsPage);
  }
 
  itemSelected(item: string) {
    console.log("Selected Item", item);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OutletDetailsPage');
  }

}
