import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { TabsPage } from '../tabs/tabs';
import { DomSanitizer,SafeResourceUrl } from "@angular/platform-browser";
import { Mysteryshopper1Page } from '../mysteryshopper1/mysteryshopper1';
import { DashboardPage } from '../dashboard/dashboard';

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {
  intro_video_link:any;
  urlSafe: SafeResourceUrl;
  MsgTxt:boolean=false;  
  usrName:any;
  current_role_full_name:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public toastController:ToastController,public security:SecurityProvider,public sanitizer: DomSanitizer) {
    this.intro_video_link=this.navParams.get("intro_video_link");
    this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(this.intro_video_link);
    this.usrName= localStorage.getItem("username");
    this.current_role_full_name= localStorage.getItem("current_role_full_name");
  }

  NextBtn() {
    if(this.current_role_full_name=='Outlet Manager') {    
      localStorage.setItem("userlogin","userlogin");     
      this.navCtrl.setRoot(DashboardPage);   
      return;
    }
    if(this.current_role_full_name=='Mystery Shopper') {    
      localStorage.setItem("userlogin","userlogin");     
      this.navCtrl.setRoot(Mysteryshopper1Page);   
      return;
    }
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

}
