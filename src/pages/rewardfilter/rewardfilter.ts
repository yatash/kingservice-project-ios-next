import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { LoginPage } from '../login/login';
/**
 * Generated class for the RewardfilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-rewardfilter',
  templateUrl: 'rewardfilter.html',
})
export class RewardfilterPage {
  productlistbycat:any=[];
  FilterSubCat:boolean=false;
  SelectFilter:any="";
  PreviousIndex:any=-1;
  product_lists:any=[];
  popupArr:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public viewCtrl: ViewController,public toastController:ToastController) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardfilterPage');
  }
  redeemBtn() {
    if(this.SelectFilter=="") {
      this.toastController.create({ message: "Please select one category.", duration: 5000, position: 'bottom' }).present();
      return false;
    }
    this.popupArr=[];
    this.popupArr.push({ status:true,SelectFilter:this.SelectFilter,product_lists:this.product_lists });
    this.viewCtrl.dismiss(this.popupArr); 
  }
  ClearAll() {  
     this.SelectFilter="";
     this.popupArr=[];
     document.getElementById("catname_"+this.PreviousIndex).style.backgroundColor='rgba(255, 255, 255, 0.05)';
     document.getElementById("catname_"+this.PreviousIndex).style.color='#F4F8FB';  
  }
  closeBtn() {
    this.popupArr=[];
    this.popupArr.push({ status:false,SelectFilter:this.SelectFilter,product_lists:this.product_lists });
    this.viewCtrl.dismiss(this.popupArr); 
  }
  SubCategoryBtn(index) {
    if(this.PreviousIndex == -1) { 
      this.SelectFilter=this.productlistbycat[index];   
      document.getElementById("catname_"+index).style.backgroundColor='#F4F8FB !important';
      document.getElementById("catname_"+index).style.color='#535353';
      this.PreviousIndex=index;
    }
    else {
      this.SelectFilter=this.productlistbycat[index]; 
      document.getElementById("catname_"+this.PreviousIndex).style.backgroundColor='rgba(255, 255, 255, 0.05)';
      document.getElementById("catname_"+this.PreviousIndex).style.color='#F4F8FB';  
      document.getElementById("catname_"+index).style.backgroundColor='#F4F8FB';
      document.getElementById("catname_"+index).style.color='#535353';
      this.PreviousIndex=index; 
    }
    if(this.productlistbycat[index].subcategories.length!=0) {
      this.FilterSubCat=true;
    }
  }
  ionViewWillEnter() {  
    this.security.productlisthome().subscribe(res => { 
      if(res.status==200) {  
        if(res.success) {  
        this.productlistbycat=res.data.categorywith_subcat_data;
        this.product_lists=res.data.product_lists;
        console.log("this.productlistbycat==",this.productlistbycat);
        return; 
        }    
      } 
    }, err => {
        console.error("err==",err);
        if(err.status==401) {    
          this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
          localStorage.clear();    
          this.navCtrl.setRoot(LoginPage);
          return; 
        }
        if(err.status==422) {    
          this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
          return; 
        }  
        else {
          this.toastController.create({ message: "No internet connection!", duration: 5000, position: 'bottom' }).present();
          return;  
        } 
    });  
  }
}