import { Component ,OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Mysteryshopper2Page } from '../mysteryshopper2/mysteryshopper2'; 
import { LoginPage } from '../login/login'; 
import { SecurityProvider } from '../../providers/security/security';
import * as moment from 'moment'; 
import { OutletnotificationPage } from '../outletnotification/outletnotification';
import { UsrprofilePage } from '../usrprofile/usrprofile';

/**
 * Generated class for the Mysteryshopper1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper1',
  templateUrl: 'mysteryshopper1.html',
})
export class Mysteryshopper1Page {
  outlet_datas:any=[]; 
  MysteryShopName:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController: ToastController, public event:Events) {
    this.MysteryShopName="Siva Kumar";
    event.subscribe('user_detail_name:created',(user,time)=>{
      this.MysteryShopName=user; 
    });
    if(localStorage.getItem("user_detail_name") != null ) { 
      this.MysteryShopName=localStorage.getItem("user_detail_name");
    } 
  }

  FirstCharStr(str) {
    return str.charAt(0);
  }

  ServerTimestamp(nowDate) { 
    return moment(nowDate).format("MMM DD, YYYY");
  }
  
  ProfileBtn() {
    this.navCtrl.push(UsrprofilePage);
  }

  Notification() {
   this.navCtrl.push(OutletnotificationPage);
  }

  ionViewWillEnter() {   
    this.GetQuizActivityFun();
  }

  NextBtn(index) {
    this.navCtrl.push(Mysteryshopper2Page, { outlet_datas:this.outlet_datas[index] });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Mysteryshopper1Page');
  }

  GetQuizActivityFun() {
    this.security.outletdatabymystery().subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
            this.outlet_datas=res.data.outlet_data;
        } 
        else { 
       }  
      } 
       else { 
      }
    }, err => { 
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      else {
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return;  
      }
    });
  }


    
  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
        Object.keys(tabs).map((key) => {
            tabs[key].style.display = 'none';
        });
    }
  }
  ionViewWillLeave() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
        Object.keys(tabs).map((key) => {
            tabs[key].style.display = 'flex';
        });
    }
  } 



}