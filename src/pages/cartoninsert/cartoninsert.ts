import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ToastController, Platform,LoadingController, ActionSheetController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file'; 
import { BarcodeScanner } from '@ionic-native/barcode-scanner'; 
import { Geolocation } from '@ionic-native/geolocation'; 
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { PosmpopupPage } from '../posmpopup/posmpopup'; 

/**
 * Generated class for the CartoninsertPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cartoninsert',
  templateUrl: 'cartoninsert.html',
})
export class CartoninsertPage {
  isEnabled:boolean=false;
  EnableStore:boolean=false;
  EnableRegion:boolean=false;
  store:any=null;
  region:any=null;

  RegionList=[];
  StoreList=[];

  imageData; 
  selectAuditData:any=[]; 
  longitude:any=0;
  latitude:any=0;
  posm_type:any=1;

  imageMain:any;

  constructor(public navCtrl: NavController,public navParams: NavParams,public modalCtrl:ModalController,public security:SecurityProvider, public toastController : ToastController,private file: File,private camera: Camera,public barcodeScanner:BarcodeScanner,public platform:Platform,public geolocation:Geolocation,public loadingCtrl:LoadingController,public actionSheetCtrl:ActionSheetController,public toastCtrl:ToastController) { 
    platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    },1);
    platform.ready().then((readySource) => {  
      if(readySource=='dom') {    
      }
      else {
        this.geolocation.getCurrentPosition().then((position)=>{
        this.latitude=position.coords.latitude;
        this.longitude=position.coords.longitude;
        });
      }
    });
    
     this.selectAuditData=this.navParams.get("auditdata");
     this.imageMain= this.selectAuditData.posm_image_banner; 
  }

  CloseBtn() {
    this.navCtrl.pop();
  }

  ionViewDidEnter() {
   // this.GetData();
  }

  GetData() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present(); 
    this.security.ChillerAuditList().subscribe(res =>{ 
      loading.dismiss();
      if(res.status==200) { 
        if(res.success) {
          this.selectAuditData=res.data.AuditData[0];
          this.imageMain= this.selectAuditData.posm_image_banner;  
        }
       }
  }, err => { 
      loading.dismiss();
      if(err.status==500) {  
        this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return;
      }
      if(err.status==422) {   
        this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
     } 
     if(err.status==401) {   
      this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
      return; 
      } 
      this.toastCtrl.create({message:"No internet connection.",duration:5000,position:'bottom'}).present();
      return; 
  }); 
  }

  ActionSheetFile() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Choose a Picture',
      buttons: [
        {
          text: 'From Gallery',
          role: 'destructive',
          handler: () => {
            this.gallery();
          }
        },{
          text: 'From Camera',
          handler: () => {
            this.cameratest();
          }
        }
      ]
    });
    actionSheet.present();
  }

  gallery() {
    this.camera.getPicture({
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL, 
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 1100,
      targetWidth: 1100,
      saveToPhotoAlbum: true,
      correctOrientation: true
    }).then((imageData) => {
      this.imageData='data:image/jpeg;base64,' + imageData; 
      this.OpenPopUp();
    }, (err) => {
    });
  }

  cameratest() {
    this.camera1();
  } 

  camera1() { 
    this.camera.getPicture({
      quality: 100,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 1100,
      targetWidth: 1100,
      saveToPhotoAlbum: true,
      correctOrientation: true
    }).then((imageData) => {
      this.imageData='data:image/jpeg;base64,' + imageData; 
      this.OpenPopUp();
    }, (err) => {
    });
  }

    OpenPopUp() {  
    let logmodal1=this.modalCtrl.create(HomePage,{selectAuditData:this.selectAuditData,imageData:this.imageData,successmsg:false,posmBool:true,latitude:this.latitude,longitude:this.longitude,DataTransaction:false,POSMCooler:'Cooler'});  
    logmodal1.onDidDismiss((views) => { 
      if(views) { 
        let logmodal2=this.modalCtrl.create(PosmpopupPage,{selectAuditData:this.selectAuditData,imageData:this.imageData,successmsg:false,posmBool:true,latitude:this.latitude,longitude:this.longitude,DataTransaction:false}); 
        logmodal2.onDidDismiss((views) => { 
          if(views) { 
            this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 2)); 
          } 
        });  
        logmodal2.present();
      } 
    });  
    logmodal1.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartoninsertPage');
  }

}
