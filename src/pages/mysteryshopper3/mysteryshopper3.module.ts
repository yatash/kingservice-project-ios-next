import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mysteryshopper3Page } from './mysteryshopper3';

@NgModule({
  declarations: [
    Mysteryshopper3Page,
  ],
  imports: [
    IonicPageModule.forChild(Mysteryshopper3Page),
  ],
})
export class Mysteryshopper3PageModule {}
