import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform ,AlertController } from 'ionic-angular';

import {FinishedLeaderboardPage} from '../finished-leaderboard/finished-leaderboard';
import {SecurityProvider} from '../../providers/security/security'
import { DashboardPage } from '../dashboard/dashboard';
import { RewardcategoryPage } from '../rewardcategory/rewardcategory';
import { UsrprofilePage } from '../usrprofile/usrprofile';
import {SettingsPage} from '../settings/settings'

/**
 * Generated class for the PointsSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-points-summary',
  templateUrl: 'points-summary.html',
})
export class PointsSummaryPage {


  data:any;
  datas:any;
  total_points:any;

  TabIndex
  BackBtnShow:boolean=false;
    constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider,public platform:Platform,public alertCtrl:AlertController) {
      this.TabIndex=this.navParams.get("TabIndex");
      if(this.TabIndex == "2") {
        this.BackBtnShow=true;
        this.platform.registerBackButtonAction(() => {
          this.navCtrl.setRoot(DashboardPage,{TabIndex:1});
        },1);
      }

    }

    CloseBtn() { 
      this.navCtrl.setRoot(DashboardPage,{TabIndex:1});  
    }
    ngOnInit() {   
      this.data=this.navParams.get("data");
      this.security.mywinnings().subscribe(res => {
        if(res.status==200) { 
          if(res.success) {
            this.datas= res.data.mywinningsdata;
            this.total_points=res.data.total_points;
          } 
     
        } 
   
      }, err => { 
        console.error("err==",err); 
        alert("err=="+JSON.stringify(err)); 
      });
     
    }

    TabIcon(TabIndex) {  
      if(TabIndex == 1) {
        this.navCtrl.setRoot(DashboardPage,{ TabIndex:TabIndex });
      }
      if(TabIndex == 2) {
       //this.navCtrl.setRoot(PointsSummaryPage,{ TabIndex:TabIndex });
      }
      if(TabIndex == 3) {
       this.navCtrl.setRoot(RewardcategoryPage,{ TabIndex:TabIndex });
      }
      if(TabIndex == 4) {
       this.navCtrl.setRoot(UsrprofilePage,{ TabIndex:TabIndex });
      }
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad LeadersboardPage');
    }
    
  od(points){
      if(points=="redeem_minus"){
        this.navCtrl.push(SettingsPage);
      }
      else{
        console.log('hi')
      }
    }

}
