import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyWinningsPage } from './my-winnings';

@NgModule({
  declarations: [
    MyWinningsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyWinningsPage),
  ],
})
export class MyWinningsPageModule {}
