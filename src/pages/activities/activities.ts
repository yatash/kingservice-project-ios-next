import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { TabsPage } from '../tabs/tabs';
import { QuizTaskPage } from '../quiz-task/quiz-task';  
import {StartQuizPage} from '../start-quiz/start-quiz';
import {ImagetaskPage} from '../imagetask/imagetask'
import { DashboardPage } from '../dashboard/dashboard';
import { QuizDescPage} from '../quiz-desc/quiz-desc'; 

/**
 * Generated class for the ActivitiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var $:any;
@IonicPage()
@Component({
  selector: 'page-activities',
  templateUrl: 'activities.html',
})
export class ActivitiesPage implements OnInit {
  current_date:any;
  pending_task_count:number=0;
  complete_task_count:number=0;
  pendingactivities_count:number=0;
  todayactivities_count:number=0; 
  todayactivities:any=[];
  pendingactivities:any=[];
  completedactivities:any[];
  expiredactivities:any[];
  segment:any;
  ac_pr:any;
  date:string;
  all_data:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController : ToastController, platform: Platform) {
    let backAction = platform.registerBackButtonAction(() => { 
      this.navCtrl.setRoot(DashboardPage,{TabIndex:1});   
    },1);
  }
  
  CloseBtn() {   
    this.navCtrl.setRoot(DashboardPage,{TabIndex:1});  
  }

 ngOnInit() {           
  var date = String(new Date());
console.log(date);

// console.log(typeof(date))
this.date = date.slice(0, 10);

    this.todayactivities=[]; 
    this.pendingactivities=[];
    this.security.TaskList().subscribe(async(res) => {
      if(res.status==200) { 
        if(res.success) {  
          this.pending_task_count=res.data.tasksummary.pending_task_count;
          this.complete_task_count=res.data.tasksummary.complete_task_count;
          this.todayactivities=res.data.todayactivities;
          this.pendingactivities=res.data.pendingactivities;
          this.pendingactivities_count=res.data.pendingactivities_count; 
          this.todayactivities_count=res.data.tasksummary.total_activities_count;
          this.completedactivities=res.data.completedactivities;  
          this.expiredactivities=res.data.expiredactivities;
          this.todayactivities.reverse();
          this.segment='activity';
          this.current_date=res.data.current_date;
          this.all_data=res.data;


         
            
          this.ac_pr= await(Math.floor((this.complete_task_count/(this.pending_task_count+this.complete_task_count))*100));
          if(this.complete_task_count==null || this.complete_task_count==undefined|| this.ac_pr==undefined || this.ac_pr==null || this.pending_task_count==undefined ||this.pending_task_count==null ){
              this.ac_pr=0;
          }
        this.dataprogress( await(Math.floor((this.complete_task_count/(this.pending_task_count+this.complete_task_count))*100)));
       
          console.log(this.ac_pr)

          return;  
        } 
        else { 
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();  return; 
       }  
      } 
       else { 
         this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();  return; 
      } 
    }, err => { 
      console.error("err==",err); 
      alert("err=="+JSON.stringify(err)); 
    });
   }
  
nextTodayBtn(task_id,title,success_points,task_type,instructions) {  
 console.log(task_type)
 if(task_type==1){
    this.navCtrl.push(QuizDescPage,{task_id:task_id,title:title,success_points:success_points,inst:instructions});
 }
 else{
  this.navCtrl.push(ImagetaskPage,{task_id:task_id,title:title,success_points:success_points});
 }
}

  pendTodayBtn(task_id,title,success_points,task_type,instructions){
    let task_status=2;
    console.log(task_type)
    if(task_type==1){
       this.navCtrl.push(QuizDescPage,{task_id:task_id,title:title,success_points:success_points,task_status,inst:instructions});
    }
    else{
     this.navCtrl.push(ImagetaskPage,{task_id:task_id,title:title,success_points:success_points,task_status});
    }
  }

  nextPendingBtn(task_id,title,success_points) {   
    this.navCtrl.push(QuizTaskPage,{ task_id:task_id,title:title,success_points:success_points ,data:this.all_data});
  } 

  
  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
        Object.keys(tabs).map((key) => {
            tabs[key].style.display = 'none';
        });
    }
    

}

dataprogress(x){

  $(".progress").each(function() {
    console.log("progress called ")

var value = x;
var left = $(this).find('.progress-left .progress-bar');
var right = $(this).find('.progress-right .progress-bar');

if (value > 0) {
  if (value <= 50) {
   
    right.css('transform', 'rotate(' + eval("x/100 * 360")  + 'deg)')
  } else {
    right.css('transform', 'rotate(180deg)')
    left.css('transform', 'rotate(' +eval("(x -50)/100 * 360") + 'deg)')
  }
}

})


}


ionViewWillLeave() {
  let tabs = document.querySelectorAll('.show-tabbar');
  if (tabs !== null) {
      Object.keys(tabs).map((key) => {
          tabs[key].style.display = 'flex';
      });

  }
} 
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivitiesPage');
  }

}
