import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController,ToastController } from 'ionic-angular'; 
import { VoucherPage } from '../voucher/voucher';    
import { BarcodeScanner } from '@ionic-native/barcode-scanner';  
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file'; 
import { RevenuePage } from '../revenue/revenue';  

/**
 * Generated class for the LogsalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-logsales',
  templateUrl: 'logsales.html',
})
export class LogsalesPage {  

  VoucherCode:boolean=false;
  QRCode:boolean=false;
  BillCode:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public barcodeScanner:BarcodeScanner,private file: File,private camera: Camera,public actionsheetCtrl:ActionSheetController,public toastCtrl:ToastController) { 

  }

  closemodal(){
    this.navCtrl.setRoot(RevenuePage);
  }

  VoucherCodeBtn() {  
    this.VoucherCode=true;
    this.navCtrl.setRoot(VoucherPage,{ Landing:1 }); 
  }

  BillCodeBtn() {       
    this.BillCode=true;  
    this.uploadpicture();   
  }

  qrscanner() {  
    this.QRCode=true;
    this.barcodeScanner.scan().then(barcodeData => {
       this.barcode(barcodeData);
     }).catch(err => {
       this.barcodeErr(err);
     });
  }
  
  barcode(barcodeData) { 
      var code=barcodeData.text; 
      this.navCtrl.setRoot(VoucherPage,{ Landing:2,code:code });
  }

  barcodeErr(barcodeData) {
    var code=barcodeData.text;
    alert("err=="+code);
  }

uploadpicture() {
  let actionsheet = this.actionsheetCtrl.create({
    title: 'Image Upload!',
    buttons: [{
      text: 'Upload From Gallery',
      handler: () => {
        this.gallery()
      },
    },
    {
      text: 'Take A Snap',
      handler: () => {
        this.camera1()
      }
    }]
  })
  actionsheet.present(); 
}

gallery() {
  this.camera.getPicture({
    quality: 90,
    destinationType: this.camera.DestinationType.FILE_URI,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    mediaType: this.camera.MediaType.PICTURE,
    saveToPhotoAlbum: false,
    correctOrientation: true
  }).then((imageData) => {
    this.navCtrl.setRoot(VoucherPage,{ Landing:3,code:imageData });

    /*
    this.file.resolveLocalFilesystemUrl(imageData).then(fileEntry => { 
      fileEntry.getMetadata((metadata) => {
             //metadata.size is the size in bytes
          if(metadata.size <= 282600){    
            this.ProfileImageUp(imageData)   
            //this.eleRef.nativeElement.querySelector('#imgfile').dispatchEvent(new Event('click')); 
          }
          else{ 
            this.toastCtrl.create({ message: `Please upload file less than 300KB. Thanks!!`, duration: 4000, position: 'top' }).present();
          }
      })
  })
  */
  }, (err) => {
    this.toastCtrl.create({ message:"Image upload fail, Please try again.", duration: 3000, position: 'top' }).present(); 
  })
}

camera1() {
this.camera.getPicture({
  quality: 75,
  destinationType:this.camera.DestinationType.DATA_URL,
  sourceType:this.camera.PictureSourceType.CAMERA,
  encodingType: this.camera.EncodingType.JPEG,
  targetHeight: 500,
  targetWidth: 500,
  saveToPhotoAlbum: false,
  correctOrientation: true
}).then((imageData) => {
  this.navCtrl.setRoot(VoucherPage,{ Landing:3,code:'data:image/jpeg;base64,' + imageData });
}, (err) => {
  this.toastCtrl.create({ message:"Image upload fail, Please try again.", duration: 3000, position: 'top' }).present(); 
})
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogsalesPage');
  }

}