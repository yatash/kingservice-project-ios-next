import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewarddetailPage } from './rewarddetail';

@NgModule({
  declarations: [
    RewarddetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RewarddetailPage),
  ],
})
export class RewarddetailPageModule {}
