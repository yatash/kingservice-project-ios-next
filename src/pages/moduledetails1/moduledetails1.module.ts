import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Moduledetails1Page } from './moduledetails1';

@NgModule({
  declarations: [
    Moduledetails1Page,
  ],
  imports: [
    IonicPageModule.forChild(Moduledetails1Page),
  ],
})
export class Moduledetails1PageModule {}
