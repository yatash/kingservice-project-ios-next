import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewarddescPage } from './rewarddesc';

@NgModule({
  declarations: [
    RewarddescPage,
  ],
  imports: [
    IonicPageModule.forChild(RewarddescPage),
  ],
})
export class RewarddescPageModule {}
