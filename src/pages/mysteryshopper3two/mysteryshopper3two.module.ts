import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mysteryshopper3twoPage } from './mysteryshopper3two';

@NgModule({
  declarations: [
    Mysteryshopper3twoPage,
  ],
  imports: [
    IonicPageModule.forChild(Mysteryshopper3twoPage),
  ],
})
export class Mysteryshopper3twoPageModule {}
