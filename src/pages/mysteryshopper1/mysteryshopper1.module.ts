import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mysteryshopper1Page } from './mysteryshopper1';

@NgModule({
  declarations: [
    Mysteryshopper1Page,
  ],
  imports: [
    IonicPageModule.forChild(Mysteryshopper1Page),
  ],
})
export class Mysteryshopper1PageModule {}
