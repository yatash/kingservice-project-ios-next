import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MobilenoPage } from './mobileno';

@NgModule({
  declarations: [
    MobilenoPage,
  ],
  imports: [
    IonicPageModule.forChild(MobilenoPage),
  ],
})
export class MobilenoPageModule {}
