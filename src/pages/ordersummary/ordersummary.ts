import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { RevieworderPage } from '../revieworder/revieworder';
import { RewardcategoryPage } from '../rewardcategory/rewardcategory';
import { SecurityProvider } from '../../providers/security/security';
import { LoginPage } from '../login/login';

/**
 * Generated class for the OrdersummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ordersummary',
  templateUrl: 'ordersummary.html',
})
export class OrdersummaryPage { 
  products:any=[];
  total_points:any=0; 
  current_points:any=0;
  points_to_be_redeem:any=0;
  balance_points:any=0;  
  ItemListTotal:number=0;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl:ModalController,public security:SecurityProvider, public toastController:ToastController) {
    this.security.OrderConfirm().subscribe(res => { 
      if(res.status==200) {  
        if(res.success) {
        this.products=res.data.products;
        this.total_points=res.data.total_points;
        this.current_points=res.data.current_points;
        this.points_to_be_redeem=res.data.points_to_be_redeem;
        this.balance_points=res.data.balance_points;

        for(let i=0;i<this.products.length;i++) { 
        this.ItemListTotal=this.ItemListTotal+(parseInt(this.products[i].qty)*parseInt(this.products[i].mrp)); 
        }

        return;  
        }    
      } 
    }, err => {
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.setRoot(LoginPage);
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      else {
        this.toastController.create({ message: "No internet connection!", duration: 5000, position: 'bottom' }).present();
        return;  
      } 
    }); 
  }

  
  CartAmount(qty,points) {
    return parseInt(qty)*parseInt(points);
  }
  
  redeemBtn() {
    this.security.Orders().subscribe(res => { 
          let modal=this.modalCtrl.create(RevieworderPage,{productsLen:this.products.length }); 
          modal.onDidDismiss((views) => {   
            this.navCtrl.setRoot(RewardcategoryPage,{ TabIndex:3 });   
          });  
        modal.present();
        return;  
    }, err => {
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.setRoot(LoginPage);
        return; 
      }
      if(err.status==422) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      }  
      if(err.status==500) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      } 
        this.toastController.create({ message: "No internet connection!", duration: 5000, position: 'bottom' }).present();
        return;  
    }); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrdersummaryPage');
  }

}