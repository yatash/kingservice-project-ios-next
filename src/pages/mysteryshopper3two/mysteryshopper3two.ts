import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the Mysteryshopper3twoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper3two',
  templateUrl: 'mysteryshopper3two.html',
})
export class Mysteryshopper3twoPage {
  posmBool:boolean=true;
  imageData:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.imageData=this.navParams.get("imageData");  
  }

  GobackToCamera() {
    this.viewCtrl.dismiss('GoBackCamera');
  }

  BackBtn() {
    this.viewCtrl.dismiss(false);
  }

  NextBtn() {
    this.viewCtrl.dismiss(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Mysteryshopper3twoPage');
  }

}
