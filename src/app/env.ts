
const HTTP_TIMEOUT: number = 60000;
export interface Enviroment {
mainApi:string,
mainApiNew:string,
timeout:number  
}

export const Live:Enviroment= { 
	//mainApi:'http://staging15api.bigcityexperiences.co.in',
	mainApi:'https://api1.staging1.bigcityvoucher.co.in',
	//mainApiNew:'https://api1.staging3.bigcityvoucher.co.in',   
	mainApiNew:'https://api1.staging4.bigcityvoucher.co.in', 
	timeout: HTTP_TIMEOUT  
}

export const ENV:Enviroment=Live;