import { NgModule } from '@angular/core';
import { IonicPageModule,IonicModule  } from 'ionic-angular';
import { StartQuizPage } from './start-quiz';

@NgModule({
  declarations: [
    StartQuizPage,
  ],
  imports: [
    IonicModule ,
    IonicPageModule.forChild(StartQuizPage),
  ],
})
export class StartQuizPageModule {}
