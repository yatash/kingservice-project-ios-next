import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import {StartQuizPage} from '../start-quiz/start-quiz';
/**
 * Generated class for the QuizDescPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz-desc',
  templateUrl: 'quiz-desc.html',
})
export class QuizDescPage {
  task_id: any;
  instructions:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizDescPage');
  }
  ionViewWillEnter() { 
    this.task_id=this.navParams.get("task_id");
    console.log('ionViewDidLoad StartQuizPage');
    this.instructions=this.navParams.get("inst");
    console.log(this.instructions)
  }

  next(){
    this.navCtrl.push(StartQuizPage,{task_id:this.task_id,inst:this.instructions});
  }
  


}
