import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the EnterotpPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'enterotp',
})
export class EnterotpPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    return value.toLowerCase();
  }
}
