import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Schme1Page } from './schme1';

@NgModule({
  declarations: [
    Schme1Page,
  ],
  imports: [
    IonicPageModule.forChild(Schme1Page),
  ],
})
export class Schme1PageModule {}
