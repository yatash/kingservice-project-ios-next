import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogsalesPage } from './logsales';

@NgModule({
  declarations: [
    LogsalesPage,
  ],
  imports: [
    IonicPageModule.forChild(LogsalesPage),
  ],
})
export class LogsalesPageModule {}
