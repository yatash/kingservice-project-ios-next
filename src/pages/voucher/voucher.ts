import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LogsalesPage } from '../logsales/logsales'; 

/**
 * Generated class for the VoucherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-voucher',
  templateUrl: 'voucher.html',
})
export class VoucherPage {
  Landing:any;
  Usercode:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.Landing=this.navParams.get("Landing");
    if(this.Landing == 2){
      this.Usercode=this.navParams.get("code");
    }
    if(this.Landing == 3){  
      this.Usercode=this.navParams.get("code");
    }
  }
  closemodal() {    
    this.navCtrl.setRoot(LogsalesPage); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoucherPage');
  }

}
