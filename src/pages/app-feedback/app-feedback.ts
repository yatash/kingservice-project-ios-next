import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { UsrprofilePage } from '../usrprofile/usrprofile';

/**
 * Generated class for the AppFeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-feedback',
  templateUrl: 'app-feedback.html',
})
export class AppFeedbackPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public platform:Platform) {
    this.platform.registerBackButtonAction(() => { 
      this.navCtrl.pop();   
    },1);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppFeedbackPage');
  }

}
