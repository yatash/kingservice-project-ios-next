import { Component, OnInit,ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController, ToastController, Platform  } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';  
import { DashboardPage } from '../dashboard/dashboard'; 
import { ActivitiesPage } from '../activities/activities'; 


/**
 * Generated class for the FortunewheelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var Winwheel:any; 
@IonicPage()
@Component({
  selector: 'page-fortunewheel',
  templateUrl: 'fortunewheel.html',
})
export class FortunewheelPage implements OnInit {  
 
  

  stop=[];
  wheelPower    = 0;
  wheelSpinning = false;
  wheel:any;
  alertprize:any
  _errorMessage: string;
  public audio:any;
  private  result;
  public numSegments:number;
  private access_token :any ;
  wof_button: any;
campaign_id:any;


  prize: any;
  text: any;
  wining_amount: any;
  title: string;
 
constructor(public navCtrl: NavController,public alertCtrl: AlertController,public navParams: NavParams,public security:SecurityProvider, public toastController : ToastController,public platform: Platform) {

   
 }
  

ngOnInit() {
  this.campaign_id=localStorage.getItem("campaign_id");
  window.scroll(0, 0);
  this.wheelSpinning = false;
  this.access_token = localStorage.getItem('access_token');  
  this.wheel_data();   
    }


public wheel_data() {

  this.security.GetCurrentWOF(this.campaign_id)    
      .subscribe(
          result => {
              
              if (result.success = true) {
                  
                  console.log(result)
                  this.numSegments=result.data.wof.length;
                  this.result=result;
                  this._errorMessage = '';
                  this.initWheel();
                  this.wof_button=result.data.wof_enable;
              }
          },
          error => {
              // unauthorized access
              if (error.status == 401 || error.status == 403) {
                
              } else {
                  this._errorMessage = error.data.message;
              }
          }
      );
}

initWheel() {
   
  let segments = [];

  for (let i=0; i<this.result.data.wof.length; i++) {
      let segmentData = {
          'fillStyle': this.result.data.wof[i].reward_color,
          'text': this.result.data.wof[i].reward_name,
          'strokeStyle' : 'white', 
          'lineWidth'   : 0.1,
           'textOrientation' : 'horizontal',
          'textDirection'   : 'reversed',
          'textAlignment' : 'outer',
          'textMargin' : 25,
          'textFillStyle' : 'black'
      };  
      segments.push(segmentData);
  }
   
  var theWheel = new Winwheel( 
  

    {
  
    'outerRadius'     : 212,        // Set outer radius so wheel fits inside the background.
    'innerRadius'     : 0,         // Make wheel hollow so segments don't go all way to center.
    'textFontSize'    : 24,         // Set default font size for the segments.
     // Make text vertial so goes down from the outside of wheel.
    'textAlignment'   : 'center',    // Align text to outside of wheel.
    'numSegments'     :   this.numSegments,         // Specify number of segments.
    'segments'        :   segments, 
    'textDirection'   : 'reversed',
    'animation' :           // Specify the animation to use.
    
    {
        'type'     : 'spinToStop',
        'duration' : 8,     // Duration in seconds.
        'spins'    : 3,     // Default number of complete spins.
        'callbackSound'    : this.playSound,   // Called when the tick sound is to be played.
        'callbackFinished' : this.alertPrize,          
        'soundTrigger'     : 'pin'  
    }
}
);

this.wheel=theWheel;

}


//    powerSelected(powerLevel)
//   {
//       // Ensure that power can't be changed while wheel is spinning.
//       if (this.wheelSpinning == false)
//       {
//           // Reset all to grey incase this is not the first time the user has selected the power.
      
//           document.getElementById('pw1').className = "";
//           document.getElementById('pw2').className = "";
//           document.getElementById('pw3').className = "";

//           // Now light up all cells below-and-including the one selected by changing the class.
//           if (powerLevel >= 1)
//           {
//               document.getElementById('pw1').className = "pw1";
//           }

//           if (powerLevel >= 2)
//           {
//               document.getElementById('pw2').className = "pw2";
//           }

//           if (powerLevel >= 3)
//           {
//               document.getElementById('pw3').className = "pw3";
//           }

//           // Set wheelPower var used when spin button is clicked.
//           this.wheelPower = powerLevel;

//           // Light up the spin button by changing it's source image and adding a clickable class to it.
   
//           document.getElementById('spin_button').className = "clickable";
//       }
//   }



 startSpin() {    

        if (this.wheelSpinning == false) {
            
          this.wheelSpinning = true;         
          this.wheel.startAnimation(); 

          this.security.GetWOF(this.campaign_id)   
        
      .subscribe(
          result => {
              
              if (result.success = true) {
                 let stopIndex = result.data.rewardsetting_id;
                 console.log(result)
                  console.log(stopIndex)
                  // for(let i=0; i<this.result.data.wof.length; i++){
                  //     
                  //     var stop  = this.result.data.wof[i].indexOf(stopIndex);
                  //     console.log(stop);
                  // }
                  


                  let found = this.result.data.wof.findIndex(s => s.rewardsetting_id == stopIndex);

                  let reward = this.result.data.wof[found].reward_name;

                  if(reward=="0"){
                    this.title="OOPS BETTER LUCK NEXT TIME"
                  }
                  else{
                    this.title="CONGRATULATIONS"
                  }

                      this.wining_amount=reward;
                      // Get random angle inside specified segment of the wheel.
                      let stopAt = this.wheel.getRandomForSegment(found+1);
       
                      // Important thing is to set the stopAngle of the animation before stating the spin.
                      this.wheel.animation.stopAngle = stopAt;
       
                      // Start the spin animation here.
                      this.wheel.startAnimation();
              


                
              }
          },
          error => {
              // unauthorized access
              if (error.status == 401 || error.status == 403) {
                
              } else {
                  this._errorMessage = error.data.message;
              }
          }
      );
     
         
       
    }
}


 resetWheel()
{
    this.wheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
    this.wheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
    this.wheel.draw();                // Call draw to render changes to the wheel.  
    this.wheelSpinning = false; 
    return false;         // Reset to false to power buttons and spin can be clicked again.
}


 alertPrize(indicatedSegment)
  { 
      document.getElementById('success').click();
  }
 

  
playSound()
{  
    // Stop and rewind the sound if it already happens to be playing.
   this.audio.pause();
   this.audio.currentTime = 0;
    // Play the sound.
  this.audio.play();
}

confirm(){
  this.navCtrl.push(ActivitiesPage); 
}
presentAlert() {
  let alert = this.alertCtrl.create({
    title: this.title,
    subTitle: 'You Have Received <br> <br>'+'<p class="sub_title">'+this.wining_amount+ '</p>',
    
    buttons: [
      {
        text: 'Continue',
        handler: () => {
          this.navCtrl.setRoot(ActivitiesPage);  
        }
      }
    ],
    enableBackdropDismiss:false,
  });
  alert.present();
}

}
