import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PosmpopupPage } from './posmpopup';

@NgModule({
  declarations: [
    PosmpopupPage,
  ],
  imports: [
    IonicPageModule.forChild(PosmpopupPage),
  ],
})
export class PosmpopupPageModule {}
