import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Content } from 'ionic-angular';
import {QuestionsPage} from "../questions/questions"

/**
 * Generated class for the StartQuizPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-start-quiz',
  templateUrl: 'start-quiz.html',
})
export class StartQuizPage {
  @ViewChild(Content) content: Content;
  task_id: any;
  instructions:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter() { 
    this.task_id=this.navParams.get("task_id");
    this.instructions=this.navParams.get("inst");
    console.log(this.instructions);
    console.log('ionViewDidLoad StartQuizPage');
  }

  detectBottom(){
    console.log(this.content.scrollTop);
    console.log(this.content.scrollHeight);
    console.log(this.content.contentHeight);

    if(this.content.scrollTop == this.content.scrollHeight - this.content.contentHeight){
    console.log("bottom was reached!");
    }
}
ques(){
  // this.navCtrl.setRoot(QuestionsPage); 
  this.navCtrl.push(QuestionsPage,{task_id:this.task_id});
}
}
