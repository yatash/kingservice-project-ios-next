import { Component, ViewChild } from '@angular/core';
import { Platform, Events,NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AndroidPermissions } from '@ionic-native/android-permissions'; 
import { TranslateService } from '@ngx-translate/core'; 
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Geolocation } from '@ionic-native/geolocation'; 
import { FCM } from '@ionic-native/fcm';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';  
import { LanguagedropPage } from '../pages/languagedrop/languagedrop'; 
import { OtpcodePage } from '../pages/otpcode/otpcode';
import { LoyaltyPage } from '../pages/loyalty/loyalty'; 
import { HomePage } from '../pages/home/home';  // unused
import { MoredetailsPage } from '../pages/moredetails/moredetails'; 
import { LanguagedropshowPage } from '../pages/languagedropshow/languagedropshow';  
import { MobilenoPage } from '../pages/mobileno/mobileno'; 
import { TermsandconditionsPage } from '../pages/termsandconditions/termsandconditions';
import { WelcomePage } from '../pages/welcome/welcome'; 

import { StartedPage } from '../pages/started/started';   

import { ActivateOutletPage } from '../pages/activate-outlet/activate-outlet';    
import { ClickPicturePage } from '../pages/click-picture/click-picture';  
import { QuestionsPage } from '../pages/questions/questions';    
import { QuizTaskPage } from '../pages/quiz-task/quiz-task';     
import { RetailerOutletPage } from '../pages/retailer-outlet/retailer-outlet'; 

import { ActivationsPage } from '../pages/activations/activations';      
import { OutletDetailsPage } from '../pages/outlet-details/outlet-details'; 

import { FortunewheelPage } from '../pages/fortunewheel/fortunewheel';   
import { CreatepasswordPage } from '../pages/createpassword/createpassword';    


import { ActivitiesPage } from '../pages/activities/activities';  

import { PosmauditPage } from '../pages/posmaudit/posmaudit';  
import { PosmperformPage } from '../pages/posmperform/posmperform';  
import { PosmperformpopPage } from '../pages/posmperformpop/posmperformpop';   
import { ChillerputtyPage } from '../pages/chillerputty/chillerputty'; 

import { UsrprofilePage } from '../pages/usrprofile/usrprofile'; 

import { LearningdevelopmentPage } from '../pages/learningdevelopment/learningdevelopment';
import { ModuledetailsPage } from '../pages/moduledetails/moduledetails';
import { Moduledetails1Page } from '../pages/moduledetails1/moduledetails1';
import { QuiztaskPage } from '../pages/quiztask/quiztask';
import { YouhaverecvdPage } from '../pages/youhaverecvd/youhaverecvd';

import { DashboardPage } from '../pages/dashboard/dashboard';
import { OutletnotificationPage } from '../pages/outletnotification/outletnotification';

import { AnnouncementPage } from '../pages/announcement/announcement'; 
import { AnnouncementsPage } from '../pages/announcements/announcements'; 

import { VisitDetails1Page } from '../pages/visit-details1/visit-details1'; 
import { VisitDetailsPage } from '../pages/visit-details/visit-details'; 
import { SelectStorePage } from '../pages/select-store/select-store'; 

import { RevenuePage } from '../pages/revenue/revenue';      
import { LogsalesPage } from '../pages/logsales/logsales';   
import { VoucherPage } from '../pages/voucher/voucher';  

import { RewarddetailPage } from '../pages/rewarddetail/rewarddetail';  // For Etap

import { RewardcategoryPage } from '../pages/rewardcategory/rewardcategory';
import { RewarddescPage } from '../pages/rewarddesc/rewarddesc'; 
import { RewardstorePage } from '../pages/rewardstore/rewardstore'; 
import { RewardfilterPage } from '../pages/rewardfilter/rewardfilter';
import { EgiftvoucherPage } from '../pages/egiftvoucher/egiftvoucher';
import { RewardscartPage } from '../pages/rewardscart/rewardscart'; 
import { OrdersummaryPage } from '../pages/ordersummary/ordersummary';
import { RevieworderPage } from '../pages/revieworder/revieworder';

import { MysteryshopperPage } from '../pages/mysteryshopper/mysteryshopper';  
import { Mysteryshopper1Page } from '../pages/mysteryshopper1/mysteryshopper1'; 
import { Mysteryshopper2Page } from '../pages/mysteryshopper2/mysteryshopper2'; 
import { Mysteryshopper3Page } from '../pages/mysteryshopper3/mysteryshopper3'; 
import { Mysteryshopper3onePage } from '../pages/mysteryshopper3one/mysteryshopper3one'; 
import { Mysteryshopper3twoPage } from '../pages/mysteryshopper3two/mysteryshopper3two'; 

import { Mysteryshopper4Page } from '../pages/mysteryshopper4/mysteryshopper4'; 
import { Mysteryshopper5Page } from '../pages/mysteryshopper5/mysteryshopper5'; 
import { Mysteryshopper6Page } from '../pages/mysteryshopper6/mysteryshopper6'; 
import { Mysteryshopper7Page } from '../pages/mysteryshopper7/mysteryshopper7'; 

@Component({
  templateUrl: 'app.html'
})

export class MyApp {            
  rootPage:any;
  @ViewChild('myNav') nav: NavController

  constructor(public platform: Platform,statusBar: StatusBar, splashScreen: SplashScreen,  private _translate : TranslateService,public event:Events,public androidPermissions: AndroidPermissions,private screenOrientation: ScreenOrientation,public geolocation:Geolocation,private fcm:FCM) { 
    platform.ready().then((readySource) => { 
      statusBar.styleDefault();
      splashScreen.hide(); 
      if(localStorage.getItem("userlogin") != null && localStorage.getItem("current_role_full_name") =='Outlet Manager' ) {  
        this.rootPage=DashboardPage;                           
       }
       else if(localStorage.getItem("userlogin") != null && localStorage.getItem("current_role_full_name") =='Mystery Shopper' ){
        this.rootPage=Mysteryshopper1Page;    
       }
       else {
         localStorage.clear();     
         this.rootPage=LoginPage;                     
       }
       if(readySource=='dom') { 
       }
      else if(readySource=='cordova') {

        // Fcm notification 
        this.fcm.getToken().then(token => {
          localStorage.setItem("fcm_token",token)
          console.log("Token"+token);
         });
        this.fcm.onNotification().subscribe(data => {

          console.log("Here....",data)
          if(data.wasTapped){
            
            //  this.rootPage=data.lpage; 
            this.nav.push(ActivationsPage);
            console.log("Received in background"+data);
          } else {
            console.log("Received in foreground",data);
          };
        });
        // end 

         // set to PORTRAIT
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
         // set to SMS permission
        this.checkPermission();
        this.ExternalPermission(); 
      }
      else  {

        
        // Fcm notification 
        this.fcm.getToken().then(token => {
          localStorage.setItem("fcm_token",token)
          console.log("Token"+token);
         });
        this.fcm.onNotification().subscribe(data => {
          console.log("Here....",data)
          if(data.wasTapped){
            console.log("Received in background"+data);
          } else {
            console.log("Received in foreground",data);
          };
        });
        // end 

          // set to PORTRAIT
          this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
          // set to SMS permission
         this.checkPermission();
         this.ExternalPermission(); 

      }
      event.subscribe('user:created',(user,time)=>{
        _translate.setDefaultLang(user); 
      }) 
      if(localStorage.getItem("applanguage") != null) { 
        _translate.setDefaultLang(localStorage.getItem("applanguage")); 
      }
      else {
        _translate.setDefaultLang('en'); 
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

    });
  }

  checkPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
      success => {
      },
    err =>{
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS).
      then(success=>{
      },
    err=>{
      console.log("cancelled");
    });
    });
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
    );
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
    );
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
  }

ExternalPermission() {
this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
  success => {
    console.log("success=",success);
  },
  err =>{
  this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).
  then(success=>{
    console.log("success1=",success);
  },
  err=>{  
  console.log("cancelled");
});
});
this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE]);

this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
  success => {
    console.log("success=",success);
  },
  err =>{
  this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).
  then(success=>{
    console.log("success1=",success);
  },
  err=>{  
  console.log("cancelled");
});
});
this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE]);
} 


}