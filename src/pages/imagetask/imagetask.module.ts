import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImagetaskPage } from './imagetask';

@NgModule({
  declarations: [
    ImagetaskPage,
  ],
  imports: [
    IonicPageModule.forChild(ImagetaskPage),
  ],
})
export class ImagetaskPageModule {}
