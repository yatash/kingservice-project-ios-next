import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TargetsPage } from './targets';

@NgModule({
  declarations: [
    TargetsPage,
  ],
  imports: [
    IonicPageModule.forChild(TargetsPage),
  ],
})
export class TargetsPageModule {}
