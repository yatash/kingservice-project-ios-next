import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizTaskPage } from './quiz-task';

@NgModule({
  declarations: [
    QuizTaskPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizTaskPage),
  ],
})
export class QuizTaskPageModule {}
