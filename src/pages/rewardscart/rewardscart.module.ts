import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewardscartPage } from './rewardscart';

@NgModule({
  declarations: [
    RewardscartPage,
  ],
  imports: [
    IonicPageModule.forChild(RewardscartPage),
  ],
})
export class RewardscartPageModule {}
