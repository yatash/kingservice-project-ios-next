import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OutletDetailsPage } from '../outlet-details/outlet-details';  

/**
 * Generated class for the ActivationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activations',
  templateUrl: 'activations.html',
})
export class ActivationsPage {

  configureTile

 items=[{rday:"Today,",date:"22 May 2018",address:"Gupta Grocery Stores, Govindpuri circle, Kalyan",duration:"Just now"},
  {rday:"This week,",date:"22 May 2018",address:"Dmart Grocery Stores, Govindpuri circle, Dadar",day:"Wednesday",time:"13:45 Hrs"},
  {address:"Gupta Grocery Stores, Govindpuri circle, Kalyan",duration:"24 minutes ago"},
  {address:"Gupta Grocery Stores, Govindpuri circle, Kalyan",duration:"24 minutes ago"},
  {address:"Gupta Grocery Stores, Govindpuri circle, Kalyan",duration:"24 minutes ago"}]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.configureTile="ActivateOutlet";
  }

  onSegmentChange(ev){ 
  if(ev.value=='ActivateOutlet') {
    this.configureTile="ActivateOutlet";
  }
  if(ev.value=='EnrollOutlet') {
    this.configureTile="EnrollOutlet";  
  }
  }

  nextBtn(){
    this.navCtrl.setRoot(OutletDetailsPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivationsPage');
  }

}
