import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mysteryshopper4Page } from './mysteryshopper4';

@NgModule({
  declarations: [
    Mysteryshopper4Page,
  ],
  imports: [
    IonicPageModule.forChild(Mysteryshopper4Page),
  ],
})
export class Mysteryshopper4PageModule {}
