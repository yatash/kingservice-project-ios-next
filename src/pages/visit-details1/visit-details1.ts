import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VisitDetailsPage } from '../visit-details/visit-details'; 

import { RevenuePage } from '../revenue/revenue';  


/**
 * Generated class for the VisitDetails1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-visit-details1',
  templateUrl: 'visit-details1.html',
})
export class VisitDetails1Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  } 

  BackBtn() { 
    this.navCtrl.setRoot(VisitDetailsPage);  
  }

  nextBtn(){ 
   this.navCtrl.setRoot(RevenuePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VisitDetails1Page');
  }

}
