import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
/**
 * Generated class for the RewardstorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-rewardstore',
  templateUrl: 'rewardstore.html',
})
export class RewardstorePage {
    bgselect1:any=0;  bgselect2:any=0;  bgselect3:any=0; bgselect4:any=0;
    itemlist:any=[]; SortItemList:any=[];
    popupArr:any=[];
    CounterValue:number=1;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController) {
    this.itemlist=this.navParams.get("itemlist");
    this.SortItemList=this.itemlist;
  }
  ArrowBackBtn() {
    if(this.CounterValue%2==1) {
      this.bgselect4=0;
      this.bgselect2=1;
      this.CounterValue++;
    }
    if(this.CounterValue%2==0) {
      this.bgselect2=0; 
      this.bgselect4=1;
      this.CounterValue++;
    } 
  }
  ArrowForwardBtn() {
    if(this.CounterValue%2==1) {
      this.bgselect4=0;
      this.bgselect2=1;
      this.CounterValue++;
    }
    if(this.CounterValue%2==0) {
      this.bgselect2=0; 
      this.bgselect4=1;
      this.CounterValue++;
    }
  }
  TxtSelect(getVal) {  
    if(getVal==1) {
      this.bgselect1=1; this.bgselect2=0; this.bgselect3=0;
    }
    if(getVal==2) {
      //this.CounterValue++;
      this.bgselect2=1; this.bgselect1=0; this.bgselect3=0;this.bgselect4=0;
      this.SortItemList.sort(function(a, b) {
          var keyA = a.points,
            keyB = b.points;
          if (keyA < keyB) return 1;
          if (keyA > keyB) return -1;
          return 0;
        }); 
    }
    if(getVal==4) { 
      //this.CounterValue++;
      this.bgselect4=1; this.bgselect2=0; this.bgselect1=0; this.bgselect3=0;
      this.SortItemList.sort(function(a, b) {
          var keyA = a.points,
            keyB = b.points;
          if (keyA < keyB) return -1;
          if (keyA > keyB) return 1;
          return 0;
        }); 
    }
    if(getVal==3) {
      this.bgselect3=1; this.bgselect1=0; this.bgselect2=0;
    }  
  }
  NextBtn() {
    this.popupArr=[]
    if(this.bgselect2==1) { 
      this.popupArr.push({ status:true,SortArr:this.SortItemList });
    }
    else {
      this.popupArr.push({ status:false,SortArr:this.SortItemList });
    }
    this.viewCtrl.dismiss(this.popupArr); 
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardstorePage');
  }
}