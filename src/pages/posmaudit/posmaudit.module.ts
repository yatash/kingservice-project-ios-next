import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PosmauditPage } from './posmaudit';

@NgModule({
  declarations: [
    PosmauditPage,
  ],
  imports: [
    IonicPageModule.forChild(PosmauditPage),
  ],
})
export class PosmauditPageModule {}
