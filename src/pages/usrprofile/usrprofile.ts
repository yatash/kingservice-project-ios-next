import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, Platform, ToastController } from 'ionic-angular';

import {EditProfilePage} from '../edit-profile/edit-profile';
import { MyWinningsPage } from '../my-winnings/my-winnings';
import {ContactusPage} from '../contactus/contactus';
import {PointsBruntPage} from '../points-brunt/points-brunt';
import {AppFeedbackPage} from '../app-feedback/app-feedback';
import {HelpPage} from '../help/help';
import {SettingsPage} from '../settings/settings';
import {SecurityProvider} from '../../providers/security/security';
import { LoginPage } from '../login/login';
import { DashboardPage } from '../dashboard/dashboard';
import { PointsSummaryPage } from '../points-summary/points-summary';
import { RewardcategoryPage } from '../rewardcategory/rewardcategory';

/**
 * Generated class for the UsrprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-usrprofile',
  templateUrl: 'usrprofile.html',
})
export class UsrprofilePage {
  data:any;
  datas:any;
  total_points:any;
  user_detail_name:any;
  displayname:any;
  channel:any;
  role_name:any;
  user_code:any;
  TabIndex
  BackBtnShow:boolean=false; 
  OutletManger:boolean=false;
  MysteryShopper:boolean=false;

  ServerButton:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public event: Events,public alertCtrl:AlertController,public platform:Platform,public toastController:ToastController) { 
    this.TabIndex=this.navParams.get("TabIndex");

    if(localStorage.getItem("current_role_full_name") =='Outlet Manager' ) {  
      this.OutletManger=true;                           
     }
     else if(localStorage.getItem("current_role_full_name") =='Mystery Shopper' ){
      this.MysteryShopper=true;    
     }

    if(this.TabIndex !=undefined) {
      if(this.TabIndex == "4") {
        this.BackBtnShow=true;
        this.platform.registerBackButtonAction(() => {
          this.navCtrl.setRoot(DashboardPage,{TabIndex:1});
        },1);
      }
    }
  }
  CloseBtn() {
    this.navCtrl.setRoot(DashboardPage,{TabIndex:1});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad UsrprofilePage');
  }
  ep(){
    this.navCtrl.push(EditProfilePage);  
  }
  mw(){
    this.navCtrl.push(MyWinningsPage); 
  }
  cu(){
    this.navCtrl.push(ContactusPage);
  }
  pb(){
    this.navCtrl.push(PointsBruntPage);
  }
  af(){
    this.navCtrl.push(AppFeedbackPage);
  }
  hp(){
    this.navCtrl.push(HelpPage);
  }
  st(){
    this.navCtrl.push(SettingsPage); 
  }

    ngOnInit() {   
      this.data=this.navParams.get("data");
      this.security.myprofile().subscribe(res => {
        if(res.status==200) { 
          if(res.success) {
            this.ServerButton=true;
            this.datas= res.data;
            this.total_points=res.data.total_points;
            this.user_detail_name=res.data.user_detail_name;
            localStorage.setItem("user_detail_name",res.data.user_detail_name);
            this.event.publish('user_detail_name:created',res.data.user_detail_name,Date.now());
            this.displayname=res.data.displayname;
            this.channel=res.data.channel;
            this.role_name=res.data.role_name;
            this.user_code=res.data.user_code;
          } 
        } 
      }, err => { 
        this.ServerButton=false;
        if(err.status==401) {    
          this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
          localStorage.clear();    
          this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
          return; 
        }
        if(err.status==422) {    
          this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
          return; 
        }  
        if(err.status==500) {
          this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
          return;  
        }
        this.toastController.create({ message: "Failed to load response data", duration: 5000, position: 'bottom' }).present();
        return; 
      });
    }

    LogoutBtn() {
      let alert=this.alertCtrl.create({
        message: 'Do you want to logout?',
        buttons: [
        {
        text: 'NO',
        role: 'cancel',
        handler: () => {
        }
        },
        {
        text: 'YES',
        handler: () => {
          let fcmtoken=localStorage.getItem("fcm_token");  
          localStorage.clear();
          localStorage.setItem("fcm_token",fcmtoken);
          this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" }); 
        }
        }
        ]
        });
        alert.present();  
    }

    TabIcon(TabIndex) {  
      if(TabIndex == 1) {
        this.navCtrl.setRoot(DashboardPage,{ TabIndex:TabIndex });
      }
      if(TabIndex == 2) {
       this.navCtrl.setRoot(PointsSummaryPage,{ TabIndex:TabIndex });
      }
      if(TabIndex == 3) {
       this.navCtrl.setRoot(RewardcategoryPage,{ TabIndex:TabIndex });
      }
      if(TabIndex == 4) {
       //this.navCtrl.setRoot(UsrprofilePage,{ TabIndex:TabIndex });
      }
    }

}