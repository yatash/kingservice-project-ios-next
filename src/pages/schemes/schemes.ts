import { Component, ɵConsole } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

import {LoyaltyPage} from '../loyalty/loyalty';
import {SecurityProvider} from '../../providers/security/security'

import {Schme1Page} from '../schme1/schme1'


/**
 * Generated class for the SchemesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schemes',
  templateUrl: 'schemes.html',
})
export class SchemesPage { 
  data:any;
  points_structure_data:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider,public platform:Platform) {
    this.platform.registerBackButtonAction(() => { 
      this.navCtrl.pop();   
    },1);
  }
  ionViewWillEnter() {  

    this.security.schemes().subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
          console.log(res)

          this.data= res.data;
          console.log(this.data)
        } 
   
      } 
 
    }, err => { 
      console.error("err==",err); 
      alert("err=="+JSON.stringify(err)); 
    });
   }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SchemesPage');
  }
  next(){
    this.navCtrl.setRoot(Schme1Page);
  }
  schemes(status,leaderboard,data){
    if(status==1)
    {
      this.navCtrl.push(Schme1Page,{data:data});
    }
    else{
      console.log("hi")
    }

  }

}
