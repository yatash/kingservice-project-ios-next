import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ClickPicturePage } from '../click-picture/click-picture';  

/**
 * Generated class for the ActivateOutletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activate-outlet',
  templateUrl: 'activate-outlet.html',
})
export class ActivateOutletPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivateOutletPage');
  }
  ClickBtn(){
    this.navCtrl.setRoot(ClickPicturePage); 
  }
}
