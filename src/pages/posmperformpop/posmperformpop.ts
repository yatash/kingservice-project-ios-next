import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, Platform, ToastController, LoadingController } from 'ionic-angular';
import { PosmauditPage } from '../posmaudit/posmaudit';    
import { ChillerputtyPage } from '../chillerputty/chillerputty';
import { DashboardPage } from '../dashboard/dashboard';  
import { CartoninsertPage } from '../cartoninsert/cartoninsert';
import { SecurityProvider } from '../../providers/security/security';

/**
 * Generated class for the PosmperformpopPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-posmperformpop',
  templateUrl: 'posmperformpop.html',
})
export class PosmperformpopPage {  

  popupArr
  QRCode:boolean=false;
  BillCode:boolean=false; 

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewController:ViewController, public alertCtrl:AlertController,public platform:Platform, public toastCtrl:ToastController, public loadingCtrl:LoadingController,public security:SecurityProvider ) {
    this.platform.registerBackButtonAction(() => { 
      this.navCtrl.setRoot(DashboardPage,{TabIndex:1}); 
    },1);
  }
 
 ClickDiv(Status) {
  if(Status=='posm') {
    this.navCtrl.push(PosmauditPage);
  }
  if(Status=='cooler') {  
    // let alert = this.alertCtrl.create({
    //   title: 'Oops!',
    //   subTitle: 'You don’t have access to this module',
      
    //   buttons: [
    //     {
    //       text: 'BACK',
    //       handler: () => {
            
    //       }
    //     }
    //   ],
    //   enableBackdropDismiss:false,
    // });
    // alert.present();
    //this.navCtrl.push(ChillerputtyPage);
    //this.navCtrl.push(CartoninsertPage);
    this.GetData(); 
  }
 }

 GetData() {
  let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  loading.present(); 
  this.security.ChillerAuditList().subscribe(res =>{ 
    loading.dismiss();
    if(res.status==200) { 
      if(res.success) {
        if(res.data.AuditData[0].status_code==0 || res.data.AuditData[0].status_code==2) {
          this.navCtrl.push(CartoninsertPage,{ auditdata:res.data.AuditData[0] });
        }
      if(res.data.AuditData[0].status_code==1 || res.data.AuditData[0].status_code==3) {
      let alert = this.alertCtrl.create({
      subTitle: res.data.AuditData[0].posm_status,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
          }
        }
      ],
      enableBackdropDismiss:false,
      });
      alert.present();
     }
      }
     }
}, err => { 
    loading.dismiss();
    if(err.status==500) {  
      this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
      return;
    }
    if(err.status==422) {   
      this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
      return; 
   } 
   if(err.status==401) {   
    this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
    return; 
    } 
    this.toastCtrl.create({message:"No internet connection.",duration:5000,position:'bottom'}).present();
    return; 
}); 
}

  CloseBtn() {  
    this.navCtrl.setRoot(DashboardPage); 
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad PosmperformpopPage');
  }

}
