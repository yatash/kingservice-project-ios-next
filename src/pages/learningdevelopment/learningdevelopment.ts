import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController, Platform, AlertController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { LoginPage } from '../login/login';
import * as moment from 'moment'; 

import { ModuledetailsPage } from '../moduledetails/moduledetails';
import { Moduledetails1Page } from '../moduledetails1/moduledetails1';
import { QuiztaskPage } from '../quiztask/quiztask';  
import { DashboardPage } from '../dashboard/dashboard';
import { TabsPage } from '../tabs/tabs';
import { YouhaverecvdPage } from '../youhaverecvd/youhaverecvd';

/**
 * Generated class for the LearningdevelopmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-learningdevelopment',
  templateUrl: 'learningdevelopment.html',
})
export class LearningdevelopmentPage {
  ContentArr:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController : ToastController, public modalCtrl:ModalController, platform:Platform, public alertCtrl:AlertController) {  
    let backAction = platform.registerBackButtonAction(() => { 
      this.navCtrl.setRoot(DashboardPage);   
    },1);
  }

  CloseBtn() {   
    this.navCtrl.pop(); 
  }
  ServerTimestamp(nowDate) { 
    return moment(nowDate).format("DD-MM-YYYY");
  }
  ContentBtn(index) {  
    if(this.ContentArr[index].course_type=="video") {
      let modal=this.modalCtrl.create(Moduledetails1Page,{ SelectContentArr : this.ContentArr[index] }); 
      modal.present(); 
      modal.onDidDismiss((views) => {
        this.GetData();  
        if(views)  {
        }
      }); 
    }
    else  { 
      this.navCtrl.push(ModuledetailsPage,{ SelectContentArr : this.ContentArr[index]});
    }
  }

  ContentBtnCom(index) { 
    this.toastController.create({ message: "You can't play "+this.ContentArr[index].course_title+", you have already played.", duration: 5000, position: 'bottom' }).present();
  }

  ionViewWillEnter() {   
      this.GetData();    
  }

  GetData() {
    this.security.Coursedetails().subscribe(res => { 
      if(res.status==200) {  
        if(res.success) { 
          this.ContentArr= res.data;
          return; 
        }    
      } 
      else { 
        this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();  
        return; 
      }
    }, err => { 
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.setRoot(LoginPage);
        return; 
      } 
      if(err.status==500) {     
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      } 
      if(err.status==422) {     
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
      } 
      this.toastController.create({ message: "No internet connection.", duration: 5000, position: 'bottom' }).present();
      return; 
    });
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad LearningdevelopmentPage');
  }

}