import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MobilenoPage } from '../mobileno/mobileno';   

/**
 * Generated class for the StartedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-started',
  templateUrl: 'started.html',
})
export class StartedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  SaveBtn() { 
    this.navCtrl.setRoot(MobilenoPage);  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartedPage');
  }

}
