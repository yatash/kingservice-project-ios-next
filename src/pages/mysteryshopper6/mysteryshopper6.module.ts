import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mysteryshopper6Page } from './mysteryshopper6';

@NgModule({
  declarations: [
    Mysteryshopper6Page,
  ],
  imports: [
    IonicPageModule.forChild(Mysteryshopper6Page),
  ],
})
export class Mysteryshopper6PageModule {}
