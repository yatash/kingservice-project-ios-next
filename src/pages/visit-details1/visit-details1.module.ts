import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisitDetails1Page } from './visit-details1';

@NgModule({
  declarations: [
    VisitDetails1Page,
  ],
  imports: [
    IonicPageModule.forChild(VisitDetails1Page),
  ],
})
export class VisitDetails1PageModule {}
