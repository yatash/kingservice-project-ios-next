import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PosmperformpopPage } from './posmperformpop';

@NgModule({
  declarations: [
    PosmperformpopPage,
  ],
  imports: [
    IonicPageModule.forChild(PosmperformpopPage),
  ],
})
export class PosmperformpopPageModule {}
