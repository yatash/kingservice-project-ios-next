import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RevieworderPage } from './revieworder';

@NgModule({
  declarations: [
    RevieworderPage,
  ],
  imports: [
    IonicPageModule.forChild(RevieworderPage),
  ],
})
export class RevieworderPageModule {}
