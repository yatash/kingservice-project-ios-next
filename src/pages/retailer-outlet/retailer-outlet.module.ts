import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RetailerOutletPage } from './retailer-outlet';

@NgModule({
  declarations: [
    RetailerOutletPage,
  ],
  imports: [
    IonicPageModule.forChild(RetailerOutletPage),
  ],
})
export class RetailerOutletPageModule {}
