import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VisitDetailsPage } from '../visit-details/visit-details';  


/**
 * Generated class for the RetailerOutletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-retailer-outlet',
  templateUrl: 'retailer-outlet.html',
})
export class RetailerOutletPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  AddPic() {
    this.navCtrl.setRoot(VisitDetailsPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RetailerOutletPage');
  }

}
