import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrdersummaryPage } from './ordersummary';

@NgModule({
  declarations: [
    OrdersummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(OrdersummaryPage),
  ],
})
export class OrdersummaryPageModule {}
