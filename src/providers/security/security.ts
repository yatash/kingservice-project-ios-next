
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import{ENV}from'../../app/env';  
import{Observable}from'rxjs/Rx'; 
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/catch';

/*
  Generated class for the SecurityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class SecurityProvider {
  constructor(public http: Http) {
    console.log('Hello SecurityProvider Provider');
  }

  load() {
    return new Promise(resolve => {
      this.http.get(ENV.mainApi+'/v1/user/home?program_id=1')
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          resolve(data);
        });
    });
  }

  posauditlist() {
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    return this.http.get(ENV.mainApiNew+'/v1/pointofsale/posmauditlist?program_id='+localStorage.getItem("program_id"), requestOptions).map((data)=>{
      return data.json()
    })
  }

postmantrans(lat,lng,posm_type,posm_data,posm_audit_typeid,posm_image) {  
  let headers = new Headers({ 'Content-Type':'application/json' }); 
  headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
  let requestOptions=new RequestOptions({ headers:headers });
  let params={
    program_id:localStorage.getItem("program_id"),
    lat:lat.toString(),
    lng:lng.toString(),
    posm_type:posm_type,
    posm_data:posm_data,
    posm_audit_typeid:posm_audit_typeid,
    posm_image:posm_image
  }
  return this.http.post(ENV.mainApiNew+'/v1/pointofsale/posmtrans',params,requestOptions).map((data)=>{
    return data.json()
  })
}


// Start Chiller Here
 
ChillerAuditList() {
  let headers = new Headers({ 'Content-Type':'application/json' }); 
  headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
  let requestOptions=new RequestOptions({ headers:headers });
  return this.http.get(ENV.mainApiNew+'/v1/pointofsale/chillerauditlist?program_id='+localStorage.getItem("program_id"), requestOptions).map((data)=>{
    return data.json()
  })
}

ChillerTrans(lat,lng,posm_type,posm_data,posm_audit_typeid,posm_image) {  
let headers = new Headers({ 'Content-Type':'application/json' }); 
headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
let requestOptions=new RequestOptions({ headers:headers });
let params={
  program_id:localStorage.getItem("program_id"),
  lat:lat.toString(),
  lng:lng.toString(),
  posm_type:posm_type,
  posm_data:posm_data,
  posm_audit_typeid:posm_audit_typeid,
  posm_image:posm_image
}
return this.http.post(ENV.mainApiNew+'/v1/pointofsale/chillertrans',params,requestOptions).map((data)=>{
  return data.json()
})
}

// End Chiller Here 

  Home() {   
    let program_id=1;
    let options = new RequestOptions();
    options.headers = new Headers();
    options.headers.append('Content-Type', 'application/json');
    
    let headers = new Headers({ 'Content-Type':'application/json' });  
    let requestOptions=new RequestOptions({ headers:headers });
    return this.http.get(ENV.mainApi+'/v1/user/home?program_id='+program_id, requestOptions).map((data)=>{
      return data.json()
    })
    // let param=({
    //   "email":'email',
    //   "password":'password'
    //   })
    //   return this.http.post(ENV.mainApi+'/login',param)
    //   .map((data)=>{
    //     alert('data'+data)
    //     return data.json()
    //   },
    //     err => {
          
    //       console.error('Oops:', err.message);
    //     })
  } 

  login(Username,program_id,source_from) {        
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    let requestOptions=new RequestOptions({ headers:headers }); //1 - Web , 2 - Android , 3 - IOS
    let param=JSON.stringify({ program_id:program_id,source_from:source_from,mobile_no:Username });
    return this.http.post(ENV.mainApiNew+'/v1/user/login',param,requestOptions).map((data)=>{
    return data.json()
    })
  }

  verifyotp(program_id,user_id,otp,fcmtoken) {     
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    let requestOptions=new RequestOptions({ headers:headers });
    let param=JSON.stringify({ program_id:program_id,user_id:user_id,otp:otp,fcm_token:fcmtoken });
    return this.http.post(ENV.mainApiNew+'/v1/user/verifyotp',param,requestOptions).map((data)=>{
    return data.json()
    })
  }

  profileupdate(user_detail_name) {    
    let headers = new Headers({ 'Content-Type':'application/json' });  
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"),user_detail_name:user_detail_name });
    return this.http.post(ENV.mainApiNew+'/v1/user/profileupdate',param,requestOptions).map((data)=>{
    return data.json()
    })
  }

  SetPin(program_id,pin,token) {       
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+token); 
    let requestOptions=new RequestOptions({ headers:headers });
    let param=JSON.stringify({ program_id:program_id,pin:pin });   
    return this.http.post(ENV.mainApi+'/v1/user/setpin',param,requestOptions).map((data)=>{
    return data.json()
    })
  }


  checksupervisor(program_id,mdNo,token){ 
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+token); 
    let requestOptions=new RequestOptions({ headers:headers });
    let param=JSON.stringify({ program_id: localStorage.getItem("program_id"),supervisor_phone_number:mdNo });  
    return this.http.post(ENV.mainApi+'/v1/user/checksupervisor',param,requestOptions).map((data)=>{
    return data.json()
  })
  }  

  LangUpdate(default_lang) { 
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"),default_lang:default_lang });  
    return this.http.post(ENV.mainApiNew+'/v1/user/langupdate',param,requestOptions).map((data)=>{
    return data.json()
  })
  } 


  UserDashboardNew() {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    return this.http.get(ENV.mainApiNew+'/v1/user/userdashboard?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  } 

  GetDefaultLang() { 
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    return this.http.get(ENV.mainApi+'/v1/user/getdefaultlang?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  }  

  TermsUpdate(is_terms) {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers }); 
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"),is_terms:is_terms });  
    return this.http.post(ENV.mainApiNew+'/v1/user/termsupdate',param,requestOptions).map((data)=>{
    return data.json()
  })
  }  


  VerifyPin(pin) {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    let requestOptions=new RequestOptions({ headers:headers });
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"),user_id:localStorage.getItem("user_id"),pin:pin });  
    return this.http.post(ENV.mainApi+'/v1/user/verifypin',param,requestOptions).map((data)=>{
    return data.json()
  })
  }

  UserDashboard() {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApi+'/v1/user/userdashboard?program_id='+localStorage.getItem("program_id")+'&user_role_id='+localStorage.getItem("user_role_id"),requestOptions).map((data)=>{
    return data.json()
  })
  }


  Getposmauditlist(store_id) {     
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApi+'/v1/pointofsale/posmauditlist?program_id='+localStorage.getItem("program_id")+"&store_id="+store_id,requestOptions).map((data)=>{
    return data.json()
  })
  }

  posmTrans(lat,lng,posm_type,posm_audit_type_id,campaign_id,posmimage,posm_data) {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });   
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"),lat:lat.toString(),lng:lng.toString(),posm_type:posm_type,posm_audit_typeid:posm_audit_type_id,campaign_id:campaign_id,posm_image:posmimage,posm_data:posm_data });  
    return this.http.post(ENV.mainApi+'/v1/pointofsale/posmtrans',param,requestOptions).map((data)=>{
    return data.json()
  })
  }  
  

  Getstorelist(region_id) {     
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApi+'/v1/pointofsale/storelist?program_id='+localStorage.getItem("program_id")+"&region_id="+region_id,requestOptions).map((data)=>{
    return data.json()
  })
  }

  Getregion() {      
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApi+'/v1/pointofsale/region?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  } 
  /*Start Reward Module Here */
  categories() {       
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/reward/categories?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  } 
  productlisthome() {       
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/reward/productlisthome?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  }
 
  productlistbycategory(category_id) {
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/reward/productlistbycategory?program_id='+localStorage.getItem("program_id")+"&category_id="+category_id,requestOptions).map((data)=>{
    return data.json()
  }) 
  }

  productdetails(product_id) {
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/reward/productdetails?program_id='+localStorage.getItem("program_id")+"&product_id="+product_id,requestOptions).map((data)=>{
    return data.json()
  })
  }
  
  SaveCart(cartdetails) {  
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });   
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"), cartdetails:cartdetails });   
    return this.http.post(ENV.mainApiNew+'/v1/reward/savecart',param,requestOptions).map((data)=>{
    return data.json()
  })
  }

  ViewCart() {  
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/reward/viewcart?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
    })
  }

  DeleteCartByProd(reward_product_id) {  
     let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });   
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"), reward_product_id:reward_product_id });   
    return this.http.post(ENV.mainApiNew+'/v1/reward/deletecartbyprod',param,requestOptions).map((data)=>{
    return data.json()
  })
  }

  Checkout() {  
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });      
    return this.http.get(ENV.mainApiNew+'/v1/reward/checkout?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
    })
  }

  OrderConfirm() {  
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });      
    return this.http.get(ENV.mainApiNew+'/v1/reward/orderconfirm?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
    })
  }

  Orders() {  
    let headers = new Headers({ 'Content-Type':'application/json' }); 
   headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
   let requestOptions=new RequestOptions({ headers:headers });   
   let param=JSON.stringify({ program_id:localStorage.getItem("program_id") });   
   return this.http.post(ENV.mainApiNew+'/v1/reward/orders',param,requestOptions).map((data)=>{
   return data.json()
 })
 }

/* end Reward Module */


  //Start of Learning and development 
  Coursedetails() {
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    return this.http.get(ENV.mainApiNew+'/v1/content/coursedetails?program_id='+localStorage.getItem("program_id"), requestOptions).map((data)=>{
      return data.json()
    })
  }

  CourseQuiz(course_id) {   
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    return this.http.get(ENV.mainApiNew+'/v1/content/coursequiz?program_id='+localStorage.getItem("program_id")+'&course_id='+course_id, requestOptions).map((data)=>{
      return data.json()
    })
  }
  
  DoCourseQuiz(course_id,course_quiz_id,course_quiz_option_id) {   
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"),course_id:course_id,course_quiz_id:course_quiz_id,course_quiz_option_id:course_quiz_option_id }); 
    return this.http.post(ENV.mainApiNew+'/v1/content/docoursequiz',param, requestOptions).map((data)=>{
      return data.json()
    })
  }
  // End of Learning and development 


  // Start of Edit Profile  
  
  MyProfile() {  
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    return this.http.get(ENV.mainApiNew+'/v1/user/myprofile?program_id='+localStorage.getItem("program_id"), requestOptions).map((data)=>{
      return data.json()
    })
  }
  
  UpdateProfile(user_detail_name,address,state_code,city,pincode,gender,date_of_birth,profile_pic,document_no,document_file) {   
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"),user_detail_name:user_detail_name,address:address,state_code:state_code,city:city,pincode:pincode,gender:gender,date_of_birth:date_of_birth,profile_pic,document_no,document_file }); 
    return this.http.post(ENV.mainApiNew+'/v1/user/updateprofile',param, requestOptions).map((data)=>{
      return data.json()
    })
  }

  // End of Edit Profile

   

// Start Question..
  
  TaskListDesc(task_id) {     
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/task/tasklistdesc?program_id='+localStorage.getItem("program_id")+"&task_id="+task_id,requestOptions).map((data)=>{
    return data.json()
  })
  }

  TaskList() {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/task/tasklist?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  }

  GetQuizActivity(task_id) {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/task/getquizactivity?program_id='+localStorage.getItem("program_id")+"&task_id="+task_id,requestOptions).map((data)=>{
    return data.json()
  })
  }

  GetSubtaskActivity(task_id) {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/task/getsubtaskactivity?program_id='+localStorage.getItem("program_id")+"&task_id="+task_id,requestOptions).map((data)=>{
    return data.json()
  })
  }

  DoTaskActivity(task_id,is_subtask,quiz_type,question_id,answer) {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });   
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"),task_id:task_id,is_subtask:is_subtask,quiz_type:quiz_type,question_id:question_id,answer:answer });  
    return this.http.post(ENV.mainApiNew+'/v1/task/dotaskactivity',param,requestOptions).map((data)=>{
    return data.json()
  })
  }

  GetCurrentWOF(campaign_id) {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/task/currentwof?program_id='+localStorage.getItem("program_id")+"&campaign_id="+campaign_id,requestOptions).map((data)=>{
    return data.json()
  })
  }

  GetWOF(campaign_id) {      
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/task/wof?program_id='+localStorage.getItem("program_id")+"&campaign_id="+campaign_id,requestOptions).map((data)=>{
    return data.json()
  })
  }

  leaderboard(scheme_id){
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/loyalty/schemeleaderboard?program_id='+localStorage.getItem("program_id")+'&scheme_id='+scheme_id,requestOptions).map((data)=>{
    return data.json()
  })
  }

  schemes(){
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/loyalty/schemes?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  }

  targets(){
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/loyalty/targetvsachievements?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  }

  

  mywinnings(){
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/user/mywinnings?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  }

  myprofile(){
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/user/myprofile?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  }

  // Start of MySTery Shopper Audit

  outletdatabymystery() {
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/mystery/outletdatabymystery?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
    return data.json()
  })
  }

  getmysteryauditquestion(outlet_user_id) {
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/mystery/getmysteryauditquestion?program_id='+localStorage.getItem("program_id")+"&outlet_user_id="+outlet_user_id,requestOptions).map((data)=>{
    return data.json()
  })
  }


  domysteryaudit(outlet_user_id,group_id,mystery_question_options) {    
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });   
    let param=JSON.stringify({ program_id:localStorage.getItem("program_id"),outlet_user_id:outlet_user_id,group_id:group_id,mystery_question_options:mystery_question_options });  
    return this.http.post(ENV.mainApiNew+'/v1/mystery/domysteryaudit',param,requestOptions).map((data)=>{
    return data.json()
  })
  }

  MysteryAuditSummary(outlet_user_id) {
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });     
    return this.http.get(ENV.mainApiNew+'/v1/mystery/mysteryauditsummary?program_id='+localStorage.getItem("program_id")+"&outlet_user_id="+outlet_user_id,requestOptions).map((data)=>{
    return data.json()
  })
  }

  MysteryOutletView(MysteryOutletQuery,NavStatus) {
    let headers = new Headers({ 'Content-Type':'application/json' }); 
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
    let requestOptions=new RequestOptions({ headers:headers });
    if(!NavStatus) {
      return this.http.get(ENV.mainApiNew+'/v1/mystery/mysteryoutletview?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
        return data.json()
      });
    }
    else { 
      return this.http.get(ENV.mainApiNew+'/v1/mystery/mysteryoutletview?program_id='+localStorage.getItem("program_id")+MysteryOutletQuery,requestOptions).map((data)=>{
        return data.json()
      });
    } 
  }

// End of MySTery Shopper Audit

 
// Notification here 
 
notifications() {      
  let headers = new Headers({ 'Content-Type':'application/json' }); 
  headers.append('Authorization', "Bearer "+localStorage.getItem("access_token")); 
  let requestOptions=new RequestOptions({ headers:headers });     
  return this.http.get(ENV.mainApiNew+'/v1/notification/getnotification',requestOptions).map((data)=>{
  return data.json()
})
} 


orderhistory() {
  let headers = new Headers({ 'Content-Type':'application/json' });
  headers.append('Authorization', "Bearer "+localStorage.getItem("access_token"));
  let requestOptions=new RequestOptions({ headers:headers });
  return this.http.get(ENV.mainApiNew+'/v1/reward/orderhistory?program_id='+localStorage.getItem("program_id"),requestOptions).map((data)=>{
  return data.json()
  })
  }
  
  orderlist(orderdetails) {
    let headers = new Headers({ 'Content-Type':'application/json' });
    headers.append('Authorization', "Bearer "+localStorage.getItem("access_token"));
    let requestOptions=new RequestOptions({ headers:headers });
    return this.http.get(ENV.mainApiNew+'/v1/reward/getorderoffercode?program_id='+localStorage.getItem("program_id")+"&orderdetail_id="+orderdetails,requestOptions).map((data)=>{
    return data.json()
    })
    }

}
