import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClickPicturePage } from './click-picture';

@NgModule({
  declarations: [
    ClickPicturePage,
  ],
  imports: [
    IonicPageModule.forChild(ClickPicturePage),
  ],
})
export class ClickPicturePageModule {}
