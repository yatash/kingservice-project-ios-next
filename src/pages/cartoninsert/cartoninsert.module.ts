import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartoninsertPage } from './cartoninsert';

@NgModule({
  declarations: [
    CartoninsertPage,
  ],
  imports: [
    IonicPageModule.forChild(CartoninsertPage),
  ],
})
export class CartoninsertPageModule {}
