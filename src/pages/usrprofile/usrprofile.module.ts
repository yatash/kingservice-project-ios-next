import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UsrprofilePage } from './usrprofile';

@NgModule({
  declarations: [
    UsrprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(UsrprofilePage),
  ],
})
export class UsrprofilePageModule {}
