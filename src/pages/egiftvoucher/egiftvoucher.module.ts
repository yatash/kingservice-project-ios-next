import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EgiftvoucherPage } from './egiftvoucher';

@NgModule({
  declarations: [
    EgiftvoucherPage,
  ],
  imports: [
    IonicPageModule.forChild(EgiftvoucherPage),
  ],
})
export class EgiftvoucherPageModule {}
