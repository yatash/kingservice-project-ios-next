import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';  
import { Mysteryshopper6Page } from '../mysteryshopper6/mysteryshopper6'; 
import { LoginPage } from '../login/login';  
import { SecurityProvider } from '../../providers/security/security';
import { Mysteryshopper3onePage } from '../mysteryshopper3one/mysteryshopper3one'; 
import { Mysteryshopper3twoPage } from '../mysteryshopper3two/mysteryshopper3two'; 

/**
 * Generated class for the Mysteryshopper3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper3',
  templateUrl: 'mysteryshopper3.html',
})
export class Mysteryshopper3Page {
  outlet_datas:any;
  MysteryAuditQuest:any=[]; 
  MysteryAuditQuestSub:any=[]; 
  mystery_questions:any=[];
  //items: any =  { 'id': 0, 'rating': 0, 'name': 'Creativity', 'company': 'PROFLEX' };
  ImgData1:boolean=false;
  ImgData2:boolean=false;
  ImgCheckYes:boolean=false; ImgCheckNo:boolean=false;
  MysteryTitle:any;
  indexSelectArr:number=0;  

  QuestAnswerArr:any=[];
  txtAreaType3:any="";
  outlet_user_id:any;
  group_id:any="";
  buttonDisabled:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public modalCtrl: ModalController, public toastController: ToastController) { 
    this.outlet_datas=this.navParams.get("outlet_datas");
    if(this.outlet_datas == undefined) {
      //this.outlet_user_id=2200;
    }
    else {
      this.outlet_user_id=this.outlet_datas.user_id;
    }
    this.QuestAnswerArr=[];
   
  }

  ionViewWillEnter() {
    this.GetQuizActivityFun();
  }
 
  GetQuizActivityFun() { 
    this.security.getmysteryauditquestion(this.outlet_user_id).subscribe(res => {
      if(res.status==200) { 
        if(res.success) { 
          if(!res.data.mystery_audit_complete) {
            this.MysteryAuditQuest=res.data.myster_audit_questions;
              this.GetSubMysteryAdudit();
          }
        } 
        else { 
       }  
      } 
       else { 
      }
    }, err => { 
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
        return; 
      } 
      else {  
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return;  
      }
    });
  }

  GetSubMysteryAdudit() { 
    this.MysteryAuditQuestSub=this.filterAudit(this.MysteryAuditQuest,this.indexSelectArr);
    this.MysteryTitle=this.MysteryAuditQuestSub[0].group_name;
    this.group_id=this.MysteryAuditQuestSub[0].group_id;
    this.mystery_questions=this.MysteryAuditQuestSub[0].mystery_questions; 
  }

  filterAudit(FinalArr,indexSelect) {
    return FinalArr.filter((item,index) => {
      return index==indexSelect;
    });
  }

  radioChecked(mystery_question_id,question_type,quest_option_id) {
    if(this.QuestAnswerArr.length==0) {
      this.QuestAnswerArr.push({ mystery_question_id:mystery_question_id.toString(),question_type:question_type.toString(), mystery_question_answer:quest_option_id.toString() });
    }
    else { 
      let indexValue=-1; 
      for(let i=0;i<this.QuestAnswerArr.length;i++) {
        if(this.QuestAnswerArr[i].mystery_question_id==mystery_question_id) {
          indexValue=i;
        }
      }
      if(indexValue==-1) {
        this.QuestAnswerArr.push({ mystery_question_id:mystery_question_id.toString(),question_type:question_type.toString(), mystery_question_answer:quest_option_id.toString() });
      }
      else {
        this.QuestAnswerArr[indexValue].mystery_question_answer=quest_option_id.toString();
      }
    }
  }

  logRatingChange(ev,mystery_question_id,question_type,mystery_question_options) {
    let quest_option_id;
    for(let i=0;i<mystery_question_options.length;i++) {
      if(ev==i+1) { 
        quest_option_id=mystery_question_options[i].id.toString();
      }
    }
    if(this.QuestAnswerArr.length==0) {
      this.QuestAnswerArr.push({ mystery_question_id:mystery_question_id.toString(),question_type:question_type.toString(), mystery_question_answer:quest_option_id.toString() });
    }
    else { 
      let indexValue=-1; 
      for(let i=0;i<this.QuestAnswerArr.length;i++) {
        if(this.QuestAnswerArr[i].mystery_question_id==mystery_question_id) {
          indexValue=i;
        }
      }
      if(indexValue==-1) {
        this.QuestAnswerArr.push({ mystery_question_id:mystery_question_id.toString(),question_type:question_type.toString(), mystery_question_answer:quest_option_id.toString() });
      }
      else {
        this.QuestAnswerArr[indexValue].mystery_question_answer=quest_option_id.toString();
      }
    }
  }

  radioCheckedOne(mystery_question_id,question_type,quest_option_id) {
    if(this.QuestAnswerArr.length==0) { 
      this.QuestAnswerArr.push({mystery_question_id:mystery_question_id.toString(),question_type:question_type.toString(),mystery_question_answer:quest_option_id.toString()});
    }
    else { 
      let indexValue=-1; 
      for(let i=0;i<this.QuestAnswerArr.length;i++) {
        if(this.QuestAnswerArr[i].mystery_question_id==mystery_question_id) {
          indexValue=i;
        }
      } 
      if(indexValue==-1) {
        this.QuestAnswerArr.push({ mystery_question_id:mystery_question_id.toString(),question_type:question_type.toString(), mystery_question_answer:quest_option_id.toString() });
      }
      else {
        this.QuestAnswerArr[indexValue].mystery_question_answer=quest_option_id.toString();
      }
    }
  } 

  TxtAreaTypeThree(mystery_question_id,question_type,quest_option_id) {
    if(this.QuestAnswerArr.length==0) {
      this.QuestAnswerArr.push({ mystery_question_id:mystery_question_id.toString(),question_type:question_type.toString(), mystery_question_answer:quest_option_id.toString() });
    }
    else { 
      let indexValue=-1; 
      for(let i=0;i<this.QuestAnswerArr.length;i++) {
        if(this.QuestAnswerArr[i].mystery_question_id==mystery_question_id) {
          indexValue=i;
        }
      } 
      if(indexValue==-1) {
        this.QuestAnswerArr.push({ mystery_question_id:mystery_question_id.toString(),question_type:question_type.toString(), mystery_question_answer:quest_option_id.toString() });
      }
      else {
        this.QuestAnswerArr[indexValue].mystery_question_answer=quest_option_id.toString();
      }
    }
  }

  YesNoBtn(ev) {
    if(ev) { 
      this.ImgCheckYes=true;
      this.ImgCheckNo=false;
      let PopStatus ='FirstPop';
      let modal=this.modalCtrl.create(Mysteryshopper3onePage, { PopStatus: PopStatus }); 
         modal.onDidDismiss((views) => {
           if(views[0].status==PopStatus) {
             if(views[0].popupValue) {
              let ImgPreview=this.modalCtrl.create(Mysteryshopper3twoPage, { imageData: "assets/imgs/budweiser12.png" }); 
              ImgPreview.onDidDismiss((views) => {
                  if(views) {
                    this.ImgData1=true; 
                    this.AnotherBtnModal(); 
                  }
                  else if(views=="GoBackCamera") {
                  }
                  else {
                  }
              });  
              ImgPreview.present();
            }
           }
         });  
      modal.present(); 
    }
    else {
      this.ImgCheckNo=true;
      this.ImgCheckYes=false;
      this.ImgData1=false; 
      this.ImgData2=false;
    }     
  }

  AnotherBtnModal() {
    let PopStatus ='SecPop';
    let modal1=this.modalCtrl.create(Mysteryshopper3onePage, { PopStatus: PopStatus }); 
    modal1.onDidDismiss((views) => {
      if(views[0].status==PopStatus) {
        if(views[0].popupValue) { 
          let ImgPreview=this.modalCtrl.create(Mysteryshopper3twoPage, { imageData: "assets/imgs/bud3.png" }); 
          ImgPreview.onDidDismiss((views) => {
              if(views) {
                this.ImgData2=true; 
              }
              else if(views=="GoBackCamera") {
              }
              else {
              }
          });  
          ImgPreview.present(); 
        }
        if(!views[0].popupValue) {
        }
      }
    });  
    modal1.present();
  }

  DelBtnModal(ev) {
    let PopStatus ='DelPop';
    let modal1=this.modalCtrl.create(Mysteryshopper3onePage, { PopStatus: PopStatus }); 
    modal1.onDidDismiss((views) => {
      if(views[0].status==PopStatus) {
        if(views[0].popupValue) { 
          if(ev==1) {    
            this.ImgData1=false;
          }
          if(ev==2) {
            this.ImgData2=false; 
          }
        }
        if(!views[0].popupValue) {
        }
      }
    });  
    modal1.present();
  }

  NextBtn() {
    if(this.MysteryTitle!='Waiter') { 
      if(this.QuestAnswerArr.length == 0) {
        this.toastController.create({ message: 'All questions are mandatory.', duration: 5000, position: 'bottom' }).present();
        return false; 
      }
      if(this.mystery_questions.length > this.QuestAnswerArr.length) {
        this.toastController.create({ message: 'All questions are mandatory.', duration: 5000, position: 'bottom' }).present();
        return false; 
      }
    }
    if(this.MysteryTitle=='Waiter') {
      if(this.txtAreaType3==null || this.txtAreaType3==undefined || this.txtAreaType3=="") {
        this.toastController.create({ message: 'Please enter additional comments', duration: 5000, position: 'bottom' }).present();
        return false; 
      }
    } 
    this.buttonDisabled=true;   
      this.security.domysteryaudit(this.outlet_user_id,this.group_id,this.QuestAnswerArr).subscribe(res => {
        if(res.status==200) {  
          if(res.success) { 
            if(this.indexSelectArr+1 < this.MysteryAuditQuest.length ) {
              this.buttonDisabled=false; 
              this.indexSelectArr++;
              this.QuestAnswerArr=[];
              this.txtAreaType3="";
              this.GetSubMysteryAdudit();
              return;
            }
            else { 
              this.buttonDisabled=true; 
              this.navCtrl.pop();
              this.navCtrl.push(Mysteryshopper6Page,{ outlet_datas : this.outlet_datas });
            }
          } 
          else { 
         }  
        } 
         else { 
        }
      }, err => { 
        this.buttonDisabled=false; 
        if(err.status==401) {    
          this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
          localStorage.clear();    
          this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
          return; 
        } 
        if(err.status==422) {    
          this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
          return; 
        } 
        else {  
          this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 10000, position: 'bottom' }).present();
          return;  
        }
      });  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Mysteryshopper3Page');
  }

}
