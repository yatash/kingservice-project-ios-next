import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { QuestionsPage } from '../questions/questions';   
import { ActivitiesPage } from '../activities/activities';  

/**
 * Generated class for the QuizTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz-task',
  templateUrl: 'quiz-task.html',
})
export class QuizTaskPage { 
  task_id:any;
  title:any;
  success_points:any;
  campaign_id;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController : ToastController)  {

  }

  ionViewWillEnter() {  
    this.task_id=this.navParams.get("task_id");
    this.title=this.navParams.get("title");
    this.success_points=this.navParams.get("success_points");
    this.security.TaskListDesc(this.task_id).subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
          this.campaign_id=  res.data.campaign_id;
          return;  
        } 
        else { 
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'top' }).present();  return; 
       }  
      } 
       else { 
         this.toastController.create({ message: res.data.message, duration: 5000, position: 'top' }).present();  return; 
      }
    }, err => { 
      console.error("err==",err); 
      alert("err=="+JSON.stringify(err)); 
    });
   }

  clickBtn() {
    this.navCtrl.push(QuestionsPage,{task_id:this.task_id});    
  }

  BackBtn() {      
    this.navCtrl.setRoot(ActivitiesPage);
  }

  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
        Object.keys(tabs).map((key) => {
            tabs[key].style.display = 'none';
        });
    }
}

ionViewWillLeave() {
  let tabs = document.querySelectorAll('.show-tabbar');
  if (tabs !== null) {
      Object.keys(tabs).map((key) => {
          tabs[key].style.display = 'flex';
      });

  }
} 

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizTaskPage');
  }

}
