import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizDescPage } from './quiz-desc';

@NgModule({
  declarations: [
    QuizDescPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizDescPage),
  ],
})
export class QuizDescPageModule {}
