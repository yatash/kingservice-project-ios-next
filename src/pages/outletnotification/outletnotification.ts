import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';

import {LoyaltyPage} from '../loyalty/loyalty';
import {ActivitiesPage} from '../activities/activities' ;
import {SecurityProvider} from '../../providers/security/security'
import { TargetsPage } from '../targets/targets';
import { PointsSummaryPage } from '../points-summary/points-summary';

/**
 * Generated class for the OutletnotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-outletnotification',
  templateUrl: 'outletnotification.html',
})
export class OutletnotificationPage implements OnInit {
  notificationArr:any=[];
  data:any;
  GetDataServer:boolean=false;
  GetDataServerBlank:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider) {
  
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OutletnotificationPage');
  }
  ngOnInit() {  
console.log('hi')
    this.security.notifications().subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
          this.GetDataServer=true;
          if(res.data== 0) {
            this.GetDataServerBlank=true;
            return false;
          }
          this.GetDataServerBlank=false;
          this.data= res.data.Notifications;
         
        } 
   
      } 
 
    }, err => { 
      console.error("err==",err); 
      alert("err=="+JSON.stringify(err)); 
    });
   }
   navigate(page){
if(page==null){
  console.log("page")
}
else{
  this.navCtrl.push(page);
}

    // if(page=='TargetsPage'){
    //   this.navCtrl.push(TargetsPage);
    // }
    // else if(page=='ActivitiesPage'){
    //   this.navCtrl.push(ActivitiesPage);
    // } 

    // else if(page=='PointsSummaryPage'){
    //   this.navCtrl.push(PointsSummaryPage);
    // }
    // else{
    //   console.log("hi")
    // }
   }
}