import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionsPage } from '../questions/questions';    
import { QuizTaskPage } from '../quiz-task/quiz-task';   

/**
 * Generated class for the ClickPicturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-click-picture',
  templateUrl: 'click-picture.html',
})
export class ClickPicturePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  clickBtn(){
    this.navCtrl.setRoot(QuizTaskPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ClickPicturePage');
  }

}
