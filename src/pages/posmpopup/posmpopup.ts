import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, Platform, LoadingController } from 'ionic-angular'; 
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file'; 
import { Geolocation } from '@ionic-native/geolocation';
import { SecurityProvider } from '../../providers/security/security';
import { LoginPage } from '../login/login';  

/**
 * Generated class for the PosmpopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-posmpopup',
  templateUrl: 'posmpopup.html',
})
export class PosmpopupPage {
  imageData:any; 

  posmBool:boolean=false;
  successmsg:boolean=false;
  selectAuditData

  longitude:any=0;
  latitude:any=0;
  posm_type:number=2;
  posm_audit_type_id:number;
  campaign_id:number;
  points:number=0;

  posm_data:any="";
  DataTransaction:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewController:ViewController,private file: File,private camera: Camera,public toastCtrl:ToastController,public geolocation:Geolocation,platform:Platform,public toastController:ToastController,public security:SecurityProvider,public loadingCtrl:LoadingController) {
    this.posm_type=2;  
    this.imageData="Select a file or drop your image here"; 
    this.imageData=this.navParams.get("imageData");
    this.selectAuditData=this.navParams.get("selectAuditData");
    this.successmsg=this.navParams.get("successmsg");  
    this.posmBool=this.navParams.get("posmBool"); 
    this.DataTransaction=this.navParams.get("DataTransaction");
    console.log("this.selectAuditData==",this.selectAuditData); 
    this.posm_audit_type_id=this.selectAuditData.posm_audit_typeid;
    this.points=this.selectAuditData.points; 

    this.latitude=this.navParams.get("latitude"); 
    this.longitude=this.navParams.get("longitude"); 

  //   platform.ready().then((readySource) => { 
  //   if(readySource=='dom') {    
  //   }
  //   else if(readySource=='cordova') {
  //     this.geolocation.getCurrentPosition().then((position)=>{
  //     this.latitude=position.coords.latitude;
  //     this.longitude=position.coords.longitude;
  //   });
  //   }
  //   else {
  //   }
  // });
  }

  closemodal(paramBool) {   
    this.viewController.dismiss(paramBool); 
  } 

  UploadAgain() {
    this.posmBool=false; 
  }

  NextFinalBtn()  {  
     let loading = this.loadingCtrl.create({ content: 'Please wait...' });
     loading.present();      
     this.security.postmantrans(this.latitude,this.longitude,this.posm_type,"image",this.posm_audit_type_id,this.imageData).subscribe(res =>{ 
       console.log("res==",res);  
       loading.dismiss();
       if(res.status==200) { 
        if(res.success) { 
          this.successmsg=true; 
          this.DataTransaction=res.data.Transaction;   
           return false;
         }
        }
   }, err => { 
       loading.dismiss();
      if(err.status==422) { 
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return false;
      }
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.setRoot(LoginPage);
        return; 
      }  
       console.error("err==",err); 
 });
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad PosmpopupPage');
  }

   
}
