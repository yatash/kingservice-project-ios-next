import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform ,ModalController,AlertController } from 'ionic-angular';
import { UsrprofilePage } from '../usrprofile/usrprofile';
import {SecurityProvider} from '../../providers/security/security';
import {OrdersummaryPage} from '../../pages/ordersummary/ordersummary'



/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage implements OnInit {
  langTxt:any;
  datas:any;
  old_data:any;
  show_old_data:boolean=false;
//   data:any={
//     "success": true,
//     "status": 200,
//     "data": [
//         {
//             "order_id": "OD36",
//             "id": "36",
//             "total_points": 200,
//             "program_id": 1,
//             "user_id": 2,
//             "order_internal_status": 0,
//             "order_date": "Mar 27,2020 06:03:08 PM",
//             "prod_count": "2",
//             "products": [
//                 {
//                     "qty": 1,
//                     "denominations": 100,
//                     "sku": "EGCGBSHR001",
//                     "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
//                     "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/small_image/460_microsite.png",
//                     "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
//                     "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/image/460_microsite.png",
//                     "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/mobile/460_microsite.png",
//                     "prod_name": "Sterling Holiday E-Gift card"
//                 },
//                 {
//                     "qty": 1,
//                     "denominations": 100,
//                     "sku": "EGCGBAMZ001",
//                     "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
//                     "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/small_image/135_microsite.png",
//                     "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
//                     "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/image/135_microsite.jpg",
//                     "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/mobile/135_microsite.jpg",
//                     "prod_name": "Amazon Pay E-Gift Card"
//                 }
//             ]
//         },
//         {
//             "order_id": "OD27",
//             "id": "27",
//             "total_points": 500,
//             "program_id": 1,
//             "user_id": 2,
//             "order_internal_status": 3,
//             "order_date": "Mar 26,2020 07:03:22 PM",
//             "prod_count": "2",
//             "products": [
//                 {
//                     "qty": 4,
//                     "denominations": 100,
//                     "sku": "EGCGBSHR001",
//                     "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
//                     "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/small_image/460_microsite.png",
//                     "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
//                     "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/image/460_microsite.png",
//                     "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/mobile/460_microsite.png",
//                     "prod_name": "Sterling Holiday E-Gift card"
//                 },
//                 {
//                     "qty": 1,
//                     "denominations": 100,
//                     "sku": "EGCGBAMZ001",
//                     "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
//                     "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/small_image/135_microsite.png",
//                     "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
//                     "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/image/135_microsite.jpg",
//                     "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/mobile/135_microsite.jpg",
//                     "prod_name": "Amazon Pay E-Gift Card"
//                 }
//             ]
//         }
//     ]
// }
data:any
  order_data: any;
  prod_name: any;
  order_date: any;
  order_id: any;
  offercode_validity: any;
  denominations: any;
  qty: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider,public platform:Platform,public modal:ModalController,private alertCtrl: AlertController) {
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.setRoot(UsrprofilePage,{TabIndex:4});
    },1);
  }
all_data:any;
CloseBtn() {
  this.navCtrl.setRoot(UsrprofilePage,{TabIndex:4});
}

  ionViewWillEnter() {
    this.security.orderhistory().subscribe(res => {
      if(res.status==200) { 
        if(res.success) {
          console.log(res)

          this.datas=res.data.new_order_data;
          this.old_data=res.data.existing_abinbev_data;
        } 
   
      } 
 
    }, err => { 
      console.error("err==",err); 
      alert("err=="+JSON.stringify(err)); 
    });
    // this.all_data={
    //   "success": true,
    //   "status": 200,
    //   "data": {
    //   "new_order_data": [
    //   {
    //   "order_id": "OD37",
    //   "id": "37",
    //   "total_points": 200,
    //   "program_id": 1,
    //   "user_id": 2,
    //   "order_internal_status": 0,
    //   "order_date": "Mar 30,2020 11:03 AM",
    //   "prod_count": "2",
    //   "order_status": "Pending",
    //   "products": [
    //   {
    //   "qty": 1,
    //   "denominations": 100,
    //   "sku": "EGCGBAMZ001",
    //   "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
    //   "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/small_image/135_microsite.png",
    //   "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
    //   "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/image/135_microsite.jpg",
    //   "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/mobile/135_microsite.jpg",
    //   "prod_name": "Amazon Pay E-Gift Card",
    //   "offer_code": null,
    //   "offercode_pin": null
    //   },
    //   {
    //   "qty": 1,
    //   "denominations": 100,
    //   "sku": "EGCGBSHR001",
    //   "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
    //   "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/small_image/460_microsite.png",
    //   "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
    //   "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/image/460_microsite.png",
    //   "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/mobile/460_microsite.png",
    //   "prod_name": "Sterling Holiday E-Gift card",
    //   "offer_code": null,
    //   "offercode_pin": null
    //   }
    //   ]
    //   },
    //   {
    //   "order_id": "OD36",
    //   "id": "36",
    //   "total_points": 200,
    //   "program_id": 1,
    //   "user_id": 2,
    //   "order_internal_status": 0,
    //   "order_date": "Mar 27,2020 06:03 PM",
    //   "prod_count": "2",
    //   "order_status": "Pending",
    //   "products": [
    //   {
    //   "qty": 1,
    //   "denominations": 100,
    //   "sku": "EGCGBAMZ001",
    //   "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
    //   "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/small_image/135_microsite.png",
    //   "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
    //   "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/image/135_microsite.jpg",
    //   "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/mobile/135_microsite.jpg",
    //   "prod_name": "Amazon Pay E-Gift Card",
    //   "offer_code": null,
    //   "offercode_pin": null
    //   },
    //   {
    //   "qty": 1,
    //   "denominations": 100,
    //   "sku": "EGCGBSHR001",
    //   "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
    //   "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/small_image/460_microsite.png",
    //   "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
    //   "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/image/460_microsite.png",
    //   "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/mobile/460_microsite.png",
    //   "prod_name": "Sterling Holiday E-Gift card",
    //   "offer_code": null,
    //   "offercode_pin": null
    //   }
    //   ]
    //   },
    //   {
    //   "order_id": "OD27",
    //   "id": "27",
    //   "total_points": 500,
    //   "program_id": 1,
    //   "user_id": 2,
    //   "order_internal_status": 3,
    //   "order_date": "Mar 26,2020 07:03 PM",
    //   "prod_count": "2",
    //   "order_status": "Completed",
    //   "products": [
    //   {
    //   "qty": 1,
    //   "denominations": 100,
    //   "sku": "EGCGBAMZ001",
    //   "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
    //   "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/small_image/135_microsite.png",
    //   "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/thumbnail/135_microsite.png",
    //   "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/image/135_microsite.jpg",
    //   "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBAMZ001/d/mobile/135_microsite.jpg",
    //   "prod_name": "Amazon Pay E-Gift Card",
    //   "offer_code": "1206865008077526",
    //   "offercode_pin": "EP3N9VXGVCWCRV"
    //   },
    //   {
    //   "qty": 4,
    //   "denominations": 100,
    //   "sku": "EGCGBSHR001",
    //   "thumbnail": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
    //   "small_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/small_image/460_microsite.png",
    //   "image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/thumbnail/460_microsite.png",
    //   "base_image": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/image/460_microsite.png",
    //   "mobile": "https://gbdev.s3.amazonaws.com/uat/product/EGCGBSHR001/d/mobile/460_microsite.png",
    //   "prod_name": "Sterling Holiday E-Gift card",
    //   "offer_code": "9876541230000030",
    //   "offercode_pin": ""
    //   }
    //   ]
    //   }
    //   ],
    //   "existing_abinbev_data": [
    //   {
    //   "order_id": "405191211100341",
    //   "prod_name": "Tanishq Gold Jewellery Gift Voucher Rs.10000/-",
    //   "qty": 1,
    //   "order_status": "Cancelled",
    //   "ref_no": null,
    //   "order_date": null,
    //   "offer_code": "405191211100341",
    //   "card_pin": 505462,
    //   "courier_name": null
    //   },
    //   {
    //   "order_id": "663191211110345",
    //   "prod_name": "Tanishq Gold Jewellery Gift Voucher Rs.5000/-",
    //   "qty": 1,
    //   "order_status": "Cancelled",
    //   "ref_no": null,
    //   "order_date": null,
    //   "offer_code": "663191211110345",
    //   "card_pin": 505462,
    //   "courier_name": null
    //   },
    //   {
    //   "order_id": "667191211140334",
    //   "prod_name": "Tanishq Gold Jewellery Gift Voucher Rs.1000/-",
    //   "qty": 1,
    //   "order_status": "Cancelled",
    //   "ref_no": null,
    //   "order_date": null,
    //   "offer_code": "667191211140334",
    //   "card_pin": 505462,
    //   "courier_name": null
    //   },
    //   {
    //   "order_id": "001191211080337",
    //   "prod_name": "Tanishq Gold Jewellery Gift Voucher Rs.20000/-",
    //   "qty": 1,
    //   "order_status": "Cancelled",
    //   "ref_no": null,
    //   "order_date": null,
    //   "offer_code": "001191211080337",
    //   "card_pin": 505462,
    //   "courier_name": null
    //   },
    //   {
    //   "order_id": "200191211150331",
    //   "prod_name": "Flipkart e-Gift Voucher Rs.100/-",
    //   "qty": 4,
    //   "order_status": "Delivered",
    //   "ref_no": "SMS1475634",
    //   "order_date": "Dec 18,2019 12:12 AM",
    //   "offer_code": "200191211150331",
    //   "card_pin": 505462,
    //   "courier_name": "SMS Delivery"
    //   },
    //   {
    //   "order_id": "210191211030334",
    //   "prod_name": "TVS Customise Gift Voucher Rs.50000/-",
    //   "qty": 2,
    //   "order_status": "Cancelled",
    //   "ref_no": null,
    //   "order_date": null,
    //   "offer_code": "210191211030334",
    //   "card_pin": 505462,
    //   "courier_name": null
    //   },
    //   {
    //   "order_id": "667191211140334M",
    //   "prod_name": "Tanishq Gold Jewellery Gift Voucher Rs.1000/-",
    //   "qty": 1,
    //   "order_status": "Returned",
    //   "ref_no": "36457317890.0",
    //   "order_date": null,
    //   "offer_code": "667191211140334M",
    //   "card_pin": 505326,
    //   "courier_name": "BLUE DART"
    //   },
    //   {
    //   "order_id": "001191211080337M",
    //   "prod_name": "Tanishq Gold Jewellery Gift Voucher Rs.20000/-",
    //   "qty": 4,
    //   "order_status": "Delivered",
    //   "ref_no": "36457317934.0",
    //   "order_date": "Jan 22,2020 12:01 AM",
    //   "offer_code": "001191211080337M",
    //   "card_pin": 505326,
    //   "courier_name": "BLUE DART"
    //   },
    //   {
    //   "order_id": "405191211100341M",
    //   "prod_name": "Tanishq Gold Jewellery Gift Voucher Rs.10000/-",
    //   "qty": 2,
    //   "order_status": "Delivered",
    //   "ref_no": "X63730639",
    //   "order_date": "Jan 20,2020 12:01 AM",
    //   "offer_code": "405191211100341M",
    //   "card_pin": 505326,
    //   "courier_name": "DTDC"
    //   },
    //   {
    //   "order_id": "663191211110345M",
    //   "prod_name": "Tanishq Gold Jewellery Gift Voucher Rs.5000/-",
    //   "qty": 1,
    //   "order_status": "Delivered",
    //   "ref_no": "X63730640",
    //   "order_date": "Jan 20,2020 12:01 AM",
    //   "offer_code": "663191211110345M",
    //   "card_pin": 505326,
    //   "courier_name": "DTDC"
    //   },
    //   {
    //   "order_id": "210191211030334M",
    //   "prod_name": "TVS Customise Gift Voucher Rs.50000/-",
    //   "qty": 2,
    //   "order_status": "Delivered",
    //   "ref_no": "X63730641",
    //   "order_date": "Jan 20,2020 12:01 AM",
    //   "offer_code": "210191211030334M",
    //   "card_pin": 505326,
    //   "courier_name": "DTDC"
    //   }
    //   ]
    //   }
    //   }






  }
 
  // async presentModal() {
  //   let alert = this.alertCtrl.create({
  //     title: 'Low battery',
  //     subTitle: '10% of battery remaining',
  //     buttons: ['Dismiss']
  //   });
  //   alert.present();
  // }

  modalopp(status,orderdetails){
    if(status=="Completed"){
      this.security.orderlist(orderdetails).subscribe(res => {
        if(res.status==200) { 
          if(res.success) {
            console.log(res)
            this.order_data=res.data.offercode_data
           this.prod_name=res.data.order_data.prod_name;
           this.order_date=res.data.order_data.order_date;
           this.order_id=res.data.order_data.order_id;
           this.denominations=res.data.order_data.denominations;
           this.qty=res.data.order_data.qty;
           this.offercode_validity=res.data.order_data.offercode_validity;
            // this.old_data=res.data.existing_abinbev_data;
          } 
     
        } 
   
      }, err => { 
        console.error("err==",err); 
        alert("err=="+JSON.stringify(err)); 
      })
      setTimeout(function(){ document.getElementById("button").click(); }, 500);
     
    }
    else{
      console.log("hi");
    }
    
   
    
  }


ngOnInit(){
  console.log('ionViewDidLoad SettingsPage');
  // this.datas = this.data.data
  // console.log(this.datas)
}


toggle(){

  console.log('hi')
  this.show_old_data=true;
}

toggle1(){

  console.log('hi')
  this.show_old_data=false;
}
}
