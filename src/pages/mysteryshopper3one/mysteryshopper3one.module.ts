import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mysteryshopper3onePage } from './mysteryshopper3one';

@NgModule({
  declarations: [
    Mysteryshopper3onePage,
  ],
  imports: [
    IonicPageModule.forChild(Mysteryshopper3onePage),
  ],
})
export class Mysteryshopper3onePageModule {}
