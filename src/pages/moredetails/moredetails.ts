import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TermsandconditionsPage } from '../termsandconditions/termsandconditions';  
import { ToastController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { WelcomePage } from '../welcome/welcome'; 
import { LanguagedropPage } from '../languagedrop/languagedrop';  
import { LanguagedropshowPage } from '../languagedropshow/languagedropshow'; 

/**
 * Generated class for the MoredetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moredetails',
  templateUrl: 'moredetails.html',
})
export class MoredetailsPage {
  //HomeUserRole
  parent_role_name

  username  
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController : ToastController) {
      // localStorage.setItem("access_token","c068a94ce77a48d69301a17beccb1e26");  
      // localStorage.setItem("current_role_short_name","Outlet");
      // localStorage.setItem("current_role_full_name","Outlet Manager");
      
      // localStorage.setItem("program_id","1");      
      // localStorage.setItem("mobile_no","9952417915");
      // localStorage.setItem("user_id","2");   
      
      console.log(localStorage.getItem("current_role_short_name"));   
      this.parent_role_name=localStorage.getItem("current_role_short_name"); 
    //this.parent_role_name=this.HomeUserRole.parent_role_name; 
  }

  Continue() {
    if(this.username == "" || this.username == undefined) {
      this.toastController.create({ message: `Username is required.`, duration: 6000, position: 'bottom' }).present(); return;
    }     
    this.security.profileupdate(this.username).subscribe(res =>{
      if(res.status==200) {  
        if(res.success) {  
            this.navCtrl.push(LanguagedropshowPage,{ languages:res.data.languages });  
            return;
        }
       }
       else {  
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();
          return;
      }
  }, err => { 
    if(err.status==422) {   
      this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
      return; 
   } 
   if(err.status==401) {   
    this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
    return; 
  } 
  if(err.status==500) {   
    this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
    return; 
  } 
      console.error("err==",err); 
  }); 
  }

  nextBtn() {
    this.navCtrl.setRoot(TermsandconditionsPage);  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoredetailsPage');
  }

}
