import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PointsSummaryPage } from './points-summary';

@NgModule({
  declarations: [
    PointsSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(PointsSummaryPage),
  ],
})
export class PointsSummaryPageModule {}
