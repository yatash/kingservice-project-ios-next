import { Component ,OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import {LeadersboardPage} from '../leadersboard/leadersboard'

/**
 * Generated class for the Schme1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schme1',
  templateUrl: 'schme1.html',
})
export class Schme1Page implements OnInit {
  data:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Schme1Page');
  }

  next(data){
    // this.navCtrl.setRoot(LeadersboardPage);
    this.navCtrl.push(LeadersboardPage,{data:data});
  }
  ngOnInit() {   
    this.data=this.navParams.get("data");
    //this.task_id=87;
    console.log(this.data)
   
  }

}
