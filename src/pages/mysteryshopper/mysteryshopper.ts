import { Component, ɵConsole } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard'; 
import { SecurityProvider } from '../../providers/security/security';
import { LoginPage } from '../login/login';  
import * as moment from 'moment'; 

/**
 * Generated class for the MysteryshopperPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper',
  templateUrl: 'mysteryshopper.html',
})
export class MysteryshopperPage {
  DataArr:any=[];
  MysteryOutletQuery:any="";
  NavStatus:boolean=false;
  StartDate:any=null;
  EndDate:any=null;
  constructor(public navCtrl: NavController, public navParams: NavParams,public toastController:ToastController,public security:SecurityProvider, public alertCtrl:AlertController) { 
  }
  GetInfoBtn() { 
    this.MysteryOutletQuery="";
    if(this.StartDate !=null) {
      this.NavStatus=true;
      this.MysteryOutletQuery +="&start_date="+this.StartDate;
    }
    else {
      this.NavStatus=false;
    }
    if(this.EndDate !=null) {
      this.NavStatus=true;
      this.MysteryOutletQuery +="&end_date="+this.EndDate;
    }
    else {
      this.NavStatus=false;
    } 
    if(this.NavStatus) { 
      this.DataArr=[];
      this.GetData();
    }  
  }
  ServerTimestamp(nowDate) { 
    return moment(nowDate).format("dddd, MMM DD");
  }
  ionViewWillEnter() {   
    this.GetData();
  } 
  GetData() {
    this.security.MysteryOutletView(this.MysteryOutletQuery,this.NavStatus).subscribe(res => {
      if(res.status==200) {  
        if(res.success) { 
          let mystery_audit_ratingsArr=[];
          let ratingArr=[];
          for(let i=0;i<res.data.length;i++) {
            for(let j=0;j<res.data[i].mystery_audit_ratings.length;j++) {
              if(res.data[i].mystery_audit_ratings[j].rating==0) {
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
              }
              if(res.data[i].mystery_audit_ratings[j].rating==1) {
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
              }
              if(res.data[i].mystery_audit_ratings[j].rating==2) {
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
              }
              if(res.data[i].mystery_audit_ratings[j].rating==3) {
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star-outline" });
                ratingArr.push({ name:"ios-star-outline" });
              }
              if(res.data[i].mystery_audit_ratings[j].rating==4) {
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star-outline" });
              }
              if(res.data[i].mystery_audit_ratings[j].rating==5) {
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
                ratingArr.push({ name:"ios-star" });
              }
              mystery_audit_ratingsArr.push({group_id:res.data[i].mystery_audit_ratings[j].group_id,group_name:res.data[i].mystery_audit_ratings[j].group_name,rating:res.data[i].mystery_audit_ratings[j].rating,ratingArr:ratingArr})
              ratingArr=[];
            }
            if(res.data[i].overall_rating==0) {
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
            }
            if(res.data[i].overall_rating==1) {
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
            }
            if(res.data[i].overall_rating==2) {
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
            }
            if(res.data[i].overall_rating==3) {
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star-outline" });
              ratingArr.push({ name:"ios-star-outline" });
            }
            if(res.data[i].overall_rating==4) {
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star-outline" });
            }
            if(res.data[i].overall_rating==5) { 
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
              ratingArr.push({ name:"ios-star" });
            }
            this.DataArr.push({id:res.data[i].id,assigned_on:res.data[i].assigned_on,status:res.data[i].status,status_message:res.data[i].status_message,completed_on:res.data[i].completed_on,points:res.data[i].points,overall_rating:res.data[i].overall_rating,ratingArr:ratingArr,mystery_audit_ratings:mystery_audit_ratingsArr});
            mystery_audit_ratingsArr=[];
            ratingArr=[];
          }
        return; 
        }    
      } 
    else {
      this.toastController.create({message: res.data.message,duration: 5000,position:'bottom'}).present();  
      return; 
    }
    }, err => { 
      if(err.status==401) {    
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        localStorage.clear();    
        this.navCtrl.push(LoginPage,{ LoginLand:"LoginLand" });
        return; 
      }
      if(err.status==422) {
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: JSON.parse(err._body).data.message,
          buttons: [
            {
              text: 'BACK',
              handler: () => {
              }
            }
          ],
          enableBackdropDismiss:false,
        });
        alert.present(); 
      } 
      else {
        this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
      }  
    });  
  }
  CloseBtn() {  
    this.navCtrl.setRoot(DashboardPage); 
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MysteryshopperPage');
  }
}