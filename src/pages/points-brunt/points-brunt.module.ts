import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PointsBruntPage } from './points-brunt';

@NgModule({
  declarations: [
    PointsBruntPage,
  ],
  imports: [
    IonicPageModule.forChild(PointsBruntPage),
  ],
})
export class PointsBruntPageModule {}
