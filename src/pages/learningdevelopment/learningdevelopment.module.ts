import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LearningdevelopmentPage } from './learningdevelopment';

@NgModule({
  declarations: [
    LearningdevelopmentPage,
  ],
  imports: [
    IonicPageModule.forChild(LearningdevelopmentPage),
  ],
})
export class LearningdevelopmentPageModule {}
