import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OutletnotificationPage } from './outletnotification';

@NgModule({
  declarations: [
    OutletnotificationPage,
  ],
  imports: [
    IonicPageModule.forChild(OutletnotificationPage),
  ],
})
export class OutletnotificationPageModule {}
