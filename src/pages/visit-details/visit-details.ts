import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VisitDetails1Page } from '../visit-details1/visit-details1';
import { SelectStorePage } from '../select-store/select-store'; 

/**
 * Generated class for the VisitDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-visit-details',
  templateUrl: 'visit-details.html',
})
export class VisitDetailsPage {
  
  items=['Select store'];

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VisitDetailsPage');
  }

  PicBtn(){
   
  }

  NextBtn(){
    this.navCtrl.setRoot(VisitDetails1Page);  
  }

  itemSelected(item: string) {
    this.navCtrl.setRoot(SelectStorePage);    
  }
   
}
