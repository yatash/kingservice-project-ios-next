import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, LoadingController,Platform } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { PosmpopupPage } from '../posmpopup/posmpopup'; 
import { PosmperformpopPage } from '../posmperformpop/posmperformpop'; 
import { PosmperformPage } from '../posmperform/posmperform'; 
import { BarcodeScanner } from '@ionic-native/barcode-scanner'; 
import { Geolocation } from '@ionic-native/geolocation'; 
import { DashboardPage } from '../dashboard/dashboard'; 

/**
 * Generated class for the PosmauditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-posmaudit',
  templateUrl: 'posmaudit.html',
})
export class PosmauditPage {

  store_name
  store_address
  AuditData:any=[];

  ImageUrl

  latitude
  longitude

  posm_type 
  store_id:any="1";

  auditdata:any=[];
  auditdataPending:any=[]; 
  auditdataCom:any=[];
  auditdataAwaiting:any=[];
  auditdataReject:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl:ModalController,public toastCtrl:ToastController,public security:SecurityProvider,public loadingCtrl:LoadingController,public barcodeScanner:BarcodeScanner,public platform:Platform,public geolocation:Geolocation) {
    this.platform.registerBackButtonAction(() => {
        this.navCtrl.pop();
    },1);
  }

  CloseBtn() {   
    this.navCtrl.pop(); 
  }

  doesFileExist(image_url) {
    var imageUrl=null; 
    var http=new XMLHttpRequest();
    http.open('HEAD',image_url,false);
    http.send(); 
    if(http.status==200) {
      imageUrl=image_url;
    }
    else {
      imageUrl="https://lh4.googleusercontent.com/proxy/4k_blwZpWu_Xv22L_pj9OBnYV95XCP0gN14V13sqDn408rFuopyATeMbZRJWsWol1mOqZ6aK0Mgqjax2AMF80ZUOWh2SNBWqF8a7RFOWZBmPsgL8E34ZTc0fIMzQtCVo6A";
    }
    return imageUrl;
  }

  ionViewWillEnter() {  
      this.GetData();
  } 

  NextClick(index) {
    //this.auditdata[index].posm_image_template=this.doesFileExist(this.auditdata[index].posm_image_template);
    console.log(this.auditdata[index]);   
    this.navCtrl.push(PosmperformPage,{ auditdata:this.auditdata[index] });     
  }

  NextClickBtn(index) { 
    if(this.auditdata[index].posm_type==2) {
      this.toastCtrl.create({ message: "You can't capture "+this.auditdata[index].posm_audit_type+", you have already captured", duration: 5000, position: 'bottom' }).present();
    }
    if(this.auditdata[index].posm_type==1) { 
      this.toastCtrl.create({ message: "You can't scan "+this.auditdata[index].posm_audit_type+", you have already scaned", duration: 5000, position: 'bottom' }).present();
    }
  }

ngAfterViewInit(): void { 
  let tabs = document.querySelectorAll('.show-tabbar');
  if (tabs !== null) {  Object.keys(tabs).map((key) => {   tabs[key].style.display = 'none';  });   }
}

ionViewWillLeave() {  
  let tabs = document.querySelectorAll('.show-tabbar');
  if (tabs !== null) {  Object.keys(tabs).map((key) => {     tabs[key].style.display = 'flex';   });     }
} 

GetData() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present(); 
    this.security.posauditlist().subscribe(res =>{ 
      loading.dismiss();
      if(res.status==200) { 
        if(res.success) {
          this.auditdata=res['data'].AuditData;
          let status_codePending=0;
          let status_codeCom=1; 
          let status_codeReject=2; 
          let status_codeAwaiting=3;  
          this.auditdataPending=this.auditdata.filter((mitem) => {
            return (mitem.status_code.toString().toLowerCase().indexOf(status_codePending.toString().toLowerCase()) > -1);
          });
          console.log("auditdataPending==",this.auditdataPending);
          this.auditdataCom=this.auditdata.filter((mitem) => {
            return (mitem.status_code.toString().toLowerCase().indexOf(status_codeCom.toString().toLowerCase()) > -1);
          });

          this.auditdataReject=this.auditdata.filter((mitem) => {
            return (mitem.status_code.toString().toLowerCase().indexOf(status_codeReject.toString().toLowerCase()) > -1);
          });
          console.log("auditdataRejecting==",this.auditdataReject);
          this.auditdataAwaiting=this.auditdata.filter((mitem) => {
            return (mitem.status_code.toString().toLowerCase().indexOf(status_codeAwaiting.toString().toLowerCase()) > -1);
          });

          this.auditdata=this.auditdataPending.concat(this.auditdataReject,this.auditdataAwaiting,this.auditdataCom); 
          console.log("auditdata==",this.auditdata); 
        }
       }
  }, err => { 
      loading.dismiss();
      if(err.status==500) {  
        this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return;
      }
      if(err.status==422) {   
        this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
        return; 
     } 
     if(err.status==401) {   
      this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
      return; 
   } 
     console.error("err==",err);  
}); 
}

  posmNext(index) {
    let logmodal=this.modalCtrl.create(PosmperformpopPage); 
    logmodal.present();
    logmodal.onDidDismiss((views) => {
      if(views[0].status){
        if(views[0].popupValue==1) { 
          this.qrscanner(index) 
        }
        if(views[0].popupValue==2) {
          let logmodal1=this.modalCtrl.create(PosmpopupPage,{selectAuditData:this.AuditData[index],successmsg:false});
          logmodal1.onDidDismiss((views) => {
            this.GetData(); 
          });  
          logmodal1.present();
        }  
      }
         console.log('Modal closed',views);
    });
  }

  qrscanner(index) {  
    this.barcodeScanner.scan().then(barcodeData => {
      var code=barcodeData.text; 
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present(); 
      this.posm_type=1;  
      this.security.posmTrans(this.latitude,this.longitude,this.posm_type,this.AuditData[index].posm_audit_typeid,this.AuditData[index].campaign_id,this.AuditData[index].posm_audit_type,code).subscribe(res =>{ 
        loading.dismiss();
        if(res.status==200) { 
         if(res.success) {
          let logmodal1=this.modalCtrl.create(PosmpopupPage,{selectAuditData:this.AuditData[index],successmsg:true});
          logmodal1.onDidDismiss((views) => {
              this.GetData(); 
          });  
          logmodal1.present();
          return false;
          }
         }
    }, err => { 
        loading.dismiss();
        if(err.status==422) { 
            this.toastCtrl.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
            return false;
        }
        else { 
            console.error("err==",err); 
        } 
  }); 
     }).catch(err => {
       var code=err.text;
       this.toastCtrl.create({ message: "Cancelled scan", duration: 5000, position: 'bottom' }).present();
     });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PosmauditPage');
  }
   
}