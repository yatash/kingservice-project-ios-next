import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, ActionSheetController, AlertController, Platform  } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms'; 
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file'; 
import { SecurityProvider } from '../../providers/security/security';
import { LoginPage } from '../login/login'; 
import { Base64 } from '@ionic-native/base64'; 
import { UsrprofilePage } from '../usrprofile/usrprofile';

/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage { 
  private todo : FormGroup;       
  Name:any; Address:any; State:any; City:any; PostalCode:any;
  MobileNumber:any; EmailID:any; Gender:any; DateofBirth:any; KYCDocNum:any;
  document_file:any="";
  profile_pic:any="";
  Serverdocument_file:any="";
  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder,private file: File,private camera: Camera,public toastController:ToastController,public security:SecurityProvider,public loadingCtrl:LoadingController,public actionSheetCtrl: ActionSheetController, private base64: Base64, public alertCtrl:AlertController, public platform:Platform) {  

    this.platform.registerBackButtonAction(() => { 
      this.navCtrl.pop();   
    },1);


    this.document_file="";
    this.profile_pic=""; 
    this.Serverdocument_file="";

    this.todo = this.formBuilder.group({
      Name: ['',Validators.required],
      Address: ['',Validators.required],
      State: ['',Validators.required],
      City: ['',Validators.required],
      PostalCode: ['',Validators.required],
      MobileNumber: ['',Validators.required],
      EmailID: ['',Validators.required],
      Gender: ['',Validators.required],
      DateofBirth: ['',Validators.required],
      KYCDocNum: ['',Validators.required]
    });

    let loading = this.loadingCtrl.create({ content: 'Please wait...' });
    loading.present(); 
    this.security.MyProfile().subscribe(res =>{ 
      console.log("res==",res);  
      loading.dismiss();
      if(res.status==200) { 
       if(res.success) { 
          console.log(res); 
          if(res.data.document_file!=null) {
            this.Serverdocument_file="";
            this.ConvertintoBase64(res.data.document_file); 
            //this.Serverdocument_file=res.data.document_file;
          }
          if(res.data.document_file==null) {
            this.Serverdocument_file="";
          }
         // this.document_file=res.data.user_detail_name; document_file
        //this.profile_pic=res.data.profile_pic;  profile_pic
          this.todo = this.formBuilder.group({
            Name: [res.data.user_detail_name],
            Address: [res.data.address],
            State: [res.data.state_code],
            City: [res.data.city],
            PostalCode: [res.data.pincode],
            MobileNumber: [res.data.mobile],
            EmailID: [res.data.email],
            Gender: [res.data.gender],
            DateofBirth: [res.data.date_of_birth],
            KYCDocNum: [res.data.document_no]
          });
          return false;
        }
       }
    }, err => { 
      loading.dismiss();
      console.error("err==",err); 
     if(err.status==422) { 
       this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
       return false;
     }
     if(err.status==401) {    
       this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
       localStorage.clear();    
       this.navCtrl.setRoot(LoginPage);
       return; 
     }  
  }); 

  }

  ActionSheetFile() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Choose a Picture',
      buttons: [
        {
          text: 'From Gallery',
          role: 'destructive',
          handler: () => {
            this.gallery();
            console.log('Destructive clicked');
          }
        },{
          text: 'From Camera',
          handler: () => {
            this.camerashow();
            console.log('Archive clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  camerashow() { 
    this.camera.getPicture({
      quality: 75,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 500,
      targetWidth: 500,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }).then((imageData) => {
        console.log("imageData==",imageData);  
      this.document_file='data:image/jpeg;base64,' + imageData; 
    }, (err) => {
      this.toastController.create({ message:"Image upload fail, Please try again.", duration: 3000, position: 'bottom' }).present(); 
    })
  }
  
  gallery() {
    this.camera.getPicture({
      quality: 90,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }).then((imageData) => {
      console.log("imageData==",imageData); 
      this.document_file='data:image/jpeg;base64,' + imageData;
    }, (err) => {
      this.toastController.create({ message:"Image upload fail, Please try again.", duration: 3000, position: 'bottom' }).present(); 
    })
  }

  logForm() {
    // this.Serverdocument_file=this.ImageBase64();
    // this.document_file=this.ImageBase64();
    console.log(this.todo.value);  
    if(this.todo.value.EmailID == "") { 
      this.toastController.create({ message:"Email ID is required.", duration: 3000, position: 'bottom' }).present(); 
      return false; 
    }
    // if(this.Serverdocument_file=="") {
    //   if(this.document_file=="") {
    //     this.toastController.create({ message:"KYC Document is required.", duration: 3000, position: 'bottom' }).present(); 
    //     return false;
    //    } 
    // }
    let loading = this.loadingCtrl.create({ content: 'Please wait...' });
    loading.present(); 
    this.security.UpdateProfile(this.todo.value.Name,this.todo.value.Address,this.todo.value.State,this.todo.value.City,this.todo.value.PostalCode,this.todo.value.Gender,this.todo.value.DateofBirth,this.profile_pic,this.todo.value.KYCDocNum,this.document_file).subscribe(res =>{ 
      loading.dismiss();
      if(res.status==200) { 
       if(res.success) { 
        let alert = this.alertCtrl.create({
          subTitle: 'Profile updated successfully!',
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.navCtrl.pop();
              }
            }
          ],
          enableBackdropDismiss:false,
        });
        alert.present();
        }
       }
    }, err => { 
      loading.dismiss();
      console.error("err==",err); 
     if(err.status==422) { 
       this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
       return false;
     }
     if(err.status==401) {    
       this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
       localStorage.clear();    
       this.navCtrl.setRoot(LoginPage);
       return; 
     }  
  });  
  }

  ConvertintoBase64(filePath) {
      this.base64.encodeFile(filePath).then((base64File: string) => {
        console.log(base64File);
      }, (err) => {
        console.log(err);
      });
  }

  ImageBase64() {
     var temimg="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAYAAAAehFoBAAAFuElEQVRYR+1Ya0hUaxRdNmFMNjSlScEtLbylJtVlehMyGhFEDyp64POHpVSTL8wxG9QUTB2mgdDM1LEu1e1hYbciiigl+pNhlzQSzR8imZZklm/Mfdnnotg4c+acyeAGnt9rr73O/va39/62C36xz+UX04tJwT/7xCYjPBlhqwhMWEqUlZVRY2Mjmpub0dnZiW/fvkGlUmHBggVYtGgRdDrdhPj6IZK/rl6lPy9exLNnz/Dlyxe4uLgIIt3c3DBlyhT09vbi69evGBoagqurKzQaDfbs2YPExESn/TptuHHjRnr8+DGUSiWCgoKwefNmxMbG2uQrLS2lqqoq3L9/Hx0dHfD19cWJEwaEhYXK9i/bwGQy0cmTJ9Hf34+QkBBcuHBBFofBYKDCwkJ8/vwZBw4cQFFRkSx7WeBjx46RyWTCwoUL0dTUJMvWunoEBgbS06dPsXXrVty5c0cyl2Rgamoq5eTkCHlYXV0t2U6szIWGhtKVK1ewY8cOVFRUSOKUBOIKEB0dDT8/P7x69UqSjdR6vHv3brp16xYSEhJw+vRph9wOAezYy8uLenp6+MJIwksVO4LTaDRUV1eHgYEBh/wOATqdjgoKCpCVlQWDwTAOv3LlSurq6oJWq0VxcbFNPqPRSBaLRSh7MTExiIuLG4ebNm0arV+/Hk+ePBHV5FDwvHnzyN3dHXV1deOwfn5+1Nb2DjNnqtHS8g6nTp1CcnKyDTGu5OHhDiJCZ2cX+vr6xmH2799PN27c4IbjvOD8/HzS6XTgMpaenj6OaO7cueTmpsTUqQq0t3cITcE6ykVFhZSQEC90PP5aWlpx7tw5hIeHj+NTKBQUGRkJi8ViV7To34SEhFB5eTkGBwdt4vbu3Us3b5ZDpXLD0BChu7vbJs7X15e4ZQ8PD8PHxwevX7+2i+OW3tjY6Jxgf39/4mN88+aNXQKz2UStra0wGk2iP5+WlkbcrjMyMuzidu3aRQ8fPrT743xCok44fxcvXoyqqiqHuS63MtjC6/V6MhqNfBLORXj27Nm0bt063Lt3T5LggoICoTx9/PhRmNbUarUwN+j1ekn2ubm5pNfrRQMpSqRWq2nDhg24e/euKO7QoRiqqPgbHz58ECrB9x9BrZ6JwECtw26Wl5dHycnJzgvmlFiyZAkqKyvtCtZo/qCXL//BnDmeWLFiBQICAjBr1iyh5vJ42dBQz61cKHuenp54//69XS5Oiby8PP5p51Ji6dKlxDfb3qVbu3YtvXhRjYiIcFgs4lNbRkYGnTlzBgqFglPGpiBu0w8ePHD+0omVtZSUFMrNzUVOTjb0+uOScpTPWqVS0Zo1a/Do0SObjYhTqr6+3rkI8yU6cuQIMjMzkZaW9h0JX8idO3eitLRUslgWXFxcLAxSto79hxsHO+A89vDwQG1t7aiwkpISSkxM5GeRLLFjh51ly5ahrKxs1J5b8/Xr10VLmsM6zICjR49Sfn4+srOzcfz4f0e/fft24kt1+/ZtpwSbzWYhndra2kbtlUolrV692mHNl+TQ29uburu7R8dL7lqZmZmSbO01lMOHD9PZs2cFDh4va2tr7Y4AYzkkOeUB/uDBg+BjrKmpkWQjtfNxKly7dg3x8fEwm80OuR0CRhzzE4nHx1WrVuH58+eS7cSEh4WFCU+kbdu2SU4vWY6TkpLIbDYLj9C3b9/KsrUWHhQURJWVldiyZYvk1i/p0lk74tcDz8eDg4OIiIhASUmJLOHp6emcu/j06ROioqJw/vx5WfaywGPFa7VaYTnCW57g4GBs2rSJK4pNPr4DI4uU9vZ2+Pv780CEyMhI2f5lG4wVfenSZbp8+dLoqorb7vTp04V1FZc9Xrbwe29kVbV8+XLs27cPSUlJTvt12tA6VTiKDQ0No8tAnkFmzJiB3+bPx+8+Pv+PZaDU0jWRuAmL8ESKEuOaFPyzIz0Z4ckIW0XgX1B/bku0cXLLAAAAAElFTkSuQmCC";
    return temimg
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

}