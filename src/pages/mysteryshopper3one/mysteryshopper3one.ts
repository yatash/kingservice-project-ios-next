import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the Mysteryshopper3onePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper3one',
  templateUrl: 'mysteryshopper3one.html',
})
export class Mysteryshopper3onePage {
  popupArr:any=[];
  PopStatus:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
    this.PopStatus=this.navParams.get("PopStatus");
    this.popupArr=[];
    this.popupArr.push({ status:this.PopStatus, popupValue:true });
  }

  NextBtn() { 
    this.popupArr[0].status=this.PopStatus;
    this.popupArr[0].popupValue=true;  
    this.viewCtrl.dismiss(this.popupArr);
  }

  NextAnotherBtn() {
    this.popupArr[0].status=this.PopStatus;
    this.popupArr[0].popupValue=true;  
    this.viewCtrl.dismiss(this.popupArr);  
  }

  NoAnotherBtn() {
    this.popupArr[0].status=this.PopStatus;
    this.popupArr[0].popupValue=false; 
    this.viewCtrl.dismiss(this.popupArr); 
  }

  NextDelBtn() {
    this.popupArr[0].status=this.PopStatus;
    this.popupArr[0].popupValue=true;  
    this.viewCtrl.dismiss(this.popupArr);  
  }

  NoDelBtn() {
    this.popupArr[0].status=this.PopStatus;
    this.popupArr[0].popupValue=false;  
    this.viewCtrl.dismiss(this.popupArr);  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Mysteryshopper3onePage');
  }

}
