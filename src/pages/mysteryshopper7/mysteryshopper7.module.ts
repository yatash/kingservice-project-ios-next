import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mysteryshopper7Page } from './mysteryshopper7';

@NgModule({
  declarations: [
    Mysteryshopper7Page,
  ],
  imports: [
    IonicPageModule.forChild(Mysteryshopper7Page),
  ],
})
export class Mysteryshopper7PageModule {}
