import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { WelcomePage } from '../welcome/welcome'; 
import { LanguagedropPage } from '../languagedrop/languagedrop';   

/**
 * Generated class for the MobilenoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({ 
  selector: 'page-mobileno',
  templateUrl: 'mobileno.html',
})
export class MobilenoPage {

  mobMD
  program_id
  token
  user_id

  HomeUserRole:any;
  parent_role_name
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController : ToastController) {
     this.program_id=localStorage.getItem("program_id");
     this.user_id=localStorage.getItem("user_id"); 
      this.token=localStorage.getItem("access_token");   
      
      this.HomeUserRole=JSON.parse(localStorage.getItem("LoyalityArr"));
      this.parent_role_name=this.HomeUserRole.parent_role_name;  

  }

  SaveBtn() {  
    if(this.mobMD == "" || this.mobMD == undefined) {
      this.toastController.create({ message: `Mobile number of MD/PSR is required.`, duration: 6000, position: 'bottom' }).present(); return;
    }     
    this.security.checksupervisor(this.program_id,this.mobMD,this.token).subscribe(res =>{
      if(res.status==200) { 
        if(res.success){
            this.navCtrl.push(LanguagedropPage); 
            return;
        }
       }
       else {  
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();
          return;
      }
  }, err => { 
    if(err.status==422) {   
      this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
      return; 
   } 
      console.error("err==",err); 
  });    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MobilenoPage');
  }

}
