import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import { LogsalesPage } from '../logsales/logsales'; 
import { VisitDetails1Page } from '../visit-details1/visit-details1'; 


/**
 * Generated class for the RevenuePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-revenue',
  templateUrl: 'revenue.html',  
})

export class RevenuePage {    
   
  cards=[{date:"13 Aug 2018",time:"20:15:45",process:"Processing",type:"Type:",reward:"Reward Point:",revenue:"Revenue Sales(INR):",qrcode:"QR Code",points:"+100",number:"91677.89"},{date:"13 Aug 2018",time:"20:15:45",process:"Approved",type:"Type:",reward:"Reward Point:",revenue:"Revenue Sales(INR):",qrcode:"QR Code",points:"+100",number:"91677.89"},{date:"13 Aug 2018",time:"20:15:45",process:"Rejected",type:"Type:",reward:"Reward Point:",revenue:"Revenue Sales(INR):",qrcode:"QR Code",points:"+100",number:"91677.89"},{date:"13 Aug 2018",time:"20:15:45",process:"Processing",type:"Type:",reward:"Reward Point:",revenue:"Revenue Sales(INR):",qrcode:"QR Code",points:"+100",number:"91677.89"},{date:"13 Aug 2018",time:"20:15:45",process:"Approved",type:"Type:",reward:"Reward Point:",revenue:"Revenue Sales(INR):",qrcode:"QR Code",points:"+100",number:"91677.89"}]

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl:ModalController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RevenuePage');
  }

  BackBtn(){
    this.navCtrl.setRoot(VisitDetails1Page);
  }

  logsales() {    
    //this.navCtrl.setRoot(LogsalesPage);
    let logmodal=this.modalCtrl.create(LogsalesPage); logmodal.present();
  }

}
