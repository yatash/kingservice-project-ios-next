import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YouhaverecvdPage } from './youhaverecvd';

@NgModule({
  declarations: [
    YouhaverecvdPage,
  ],
  imports: [
    IonicPageModule.forChild(YouhaverecvdPage),
  ],
})
export class YouhaverecvdPageModule {}
