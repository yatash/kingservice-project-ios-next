import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mysteryshopper2Page } from './mysteryshopper2';

@NgModule({
  declarations: [
    Mysteryshopper2Page,
  ],
  imports: [
    IonicPageModule.forChild(Mysteryshopper2Page),
  ],
})
export class Mysteryshopper2PageModule {}
