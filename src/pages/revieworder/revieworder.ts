import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';

/**
 * Generated class for the RevieworderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-revieworder',
  templateUrl: 'revieworder.html',
})
export class RevieworderPage {
  NoItems:number=0;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController) {
    this.NoItems=this.navParams.get("productsLen");
  }

  NextOrderBtn() {
    this.navCtrl.push(SettingsPage);
  }

  NextBtn() {     
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RevieworderPage');
  }

}
