import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

import {FinishedLeaderboardPage} from '../finished-leaderboard/finished-leaderboard';
import {SecurityProvider} from '../../providers/security/security';
import { UsrprofilePage } from '../usrprofile/usrprofile';

/**
 * Generated class for the PointsBruntPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-points-brunt',
  templateUrl: 'points-brunt.html',
})
export class PointsBruntPage {

  date:any;
  data:any;
  datas:any;
  total_points:any;
    constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider,public platform:Platform) {
      this.platform.registerBackButtonAction(() => { 
        this.navCtrl.pop();    
      },1);
    }
    ngOnInit() {   
      this.data=this.navParams.get("data");
      this.security.mywinnings().subscribe(res => {
        if(res.status==200) { 
          if(res.success) {
            console.log(res)
  
            this.datas= res.data.mywinningsdata;
            this.date=res.data.mywinningsdata.cretaed_on;
            this.total_points=res.data.total_points;
            console.log(this.datas)
          } 
     
        } 
   
      }, err => { 
        console.error("err==",err); 
        alert("err=="+JSON.stringify(err)); 
      });
     
    }
    ionViewDidLoad() {
      console.log('ionViewDidLoad LeadersboardPage');
    }
}
