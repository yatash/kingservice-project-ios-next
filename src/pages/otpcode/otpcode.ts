import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, LoadingController } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';

import { HomePage } from '../home/home'; 
import { LanguagedropPage } from '../languagedrop/languagedrop';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CreatepasswordPage } from '../createpassword/createpassword'; 
import { AndroidPermissions } from '@ionic-native/android-permissions'; 
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { MoredetailsPage } from '../moredetails/moredetails'; 
import { TabsPage } from '../tabs/tabs';
import { Mysteryshopper1Page } from '../mysteryshopper1/mysteryshopper1'; 
import { DashboardPage } from '../dashboard/dashboard';

declare var SMS:any;
declare var document:any;

/**
 * Generated class for the OtpcodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otpcode',
  templateUrl: 'otpcode.html',
})
export class OtpcodePage {
  
  otp1
  otp2
  otp3
  otp4
  otp5
  otp6

  program_id
  mobile_no
  user_id

  hide:boolean=true;
  timer:any;
  final_otp
  loading:any;

  MoveForward:boolean=true; 
  buttonDisabled:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder,public security:SecurityProvider, public toastController : ToastController,public androidPermissions: AndroidPermissions,public platform:Platform,public loadingCtrl:LoadingController) {  
    this.program_id=this.navParams.get("program_id");
    this.mobile_no=this.navParams.get("mobile_no");
    this.user_id=this.navParams.get("user_id"); 
  }

ionViewWillEnter() { 
  this.final_otp=this.navParams.get("mobotp"); 
  this.platform.ready().then((readySource) => { 
    if(readySource=='dom') { 
      if(this.final_otp=="123456") {
        this.otp1="1";
        this.otp2="2";  
        this.otp3="3";
        this.otp4="4";
        this.otp5="5";
        this.otp6="6"; 
        this.verifybtn(); 
       }
       else {  
         
       }   
    }
    else if(readySource=='cordova') {
      //if(this.final_otp=="123456") {
        //this.otp1="1";
        //this.otp2="2";  
        //this.otp3="3";
        //this.otp4="4";
        //this.otp5="5";
        //this.otp6="6";
      // this.verifybtn(); 
      //}
      //else {
        this.ReadSMSList();
      //}
    }
    else {
      //if(this.final_otp=="123456") {
        //this.otp1="1";
        //this.otp2="2";  
        //this.otp3="3";
        //this.otp4="4";
        //this.otp5="5";
        //this.otp6="6";
        //this.verifybtn(); 
       //}
      // else {
         this.ReadSMSList();
       //}
    }
  }); 
} 

checkPermission() {
  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
    success => {
      this.ReadSMSList();
    },
  err =>{
    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS).
    then(success=>{
      this.ReadSMSList();
    },
  err=>{
  });
  });
  this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]); 
}

ReadSMSList() {
  this.loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  this.loading.present(); 
  this.timer =TimerObservable.create(0,(2*1000)).subscribe(t => { 
    this.ReadInbox();
    if(t==10) {
      this.stopSMSTimer(); 
    }
  }); 
}

ReadInbox() { 
  let filter = {
    box : 'inbox', // 'inbox' (default), 'sent', 'draft'
    indexFrom : 0, // start from index 0
    maxCount : 1, // count of SMS to return each time
    };  
    if(SMS)SMS.listSMS(filter, (ListSms)=>{               
    if(ListSms[0].address=='VXBIGCTY') {
      if(ListSms[0].body.substr(12,18)==this.final_otp) {
        this.stopSMSTimer();
        this.otp1=ListSms[0].body.substr(12,1);
        this.otp2=ListSms[0].body.substr(13,1);
        this.otp3=ListSms[0].body.substr(14,1);
        this.otp4=ListSms[0].body.substr(15,1);
        this.otp5=ListSms[0].body.substr(16,1);
        this.otp6=ListSms[0].body.substr(17,1);
        this.verifybtn();
      }  
     }
    },   
    Error=>{ 
      this.stopSMSTimer();
    this.toastController.create({ message: "Please allow read sms permission.", duration: 5000, position: 'bottom' }).present();
    }); 
}

stopSMSTimer() {
  this.loading.dismiss();
  this.timer.unsubscribe();
}

receiveSMS() {
  if(SMS)SMS.startWatch(function(){ 
    document.addEventListener('onSMSArrive', (e: any) => {
      var IncomingSMS = e.data;
    });
  }, function(){
  });
}

stopSMS() {
  if(SMS)SMS.stopWatch(function(){
  }, function(){
  });
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpcodePage'); 
  }

  verifybtn() {
    if(this.otp1=="" || this.otp1==null || this.otp2=="" || this.otp2==null || this.otp3=="" || this.otp3==null || this.otp4=="" || this.otp4==null || this.otp5=="" || this.otp5==null || this.otp6=="" || this.otp6==null)  { 
      this.toastController.create({ message: "Please enter correct OTP to proceed", duration: 5000, position: 'bottom' }).present();
      return;
    }
    this.buttonDisabled=true; 
    let mobInput=this.otp1+this.otp2+this.otp3+this.otp4+this.otp5+this.otp6;
    let fcmtoken=localStorage.getItem("fcm_token"); 
    this.security.verifyotp(this.program_id,this.user_id,mobInput,fcmtoken).subscribe(res =>{ 
      //this.security.verifyotp(this.program_id,this.user_id,mobInput).subscribe(res =>{ 
      if(res.status==200) { 
        if(res.success) {
          if(res.data.verified) {
          if(res.data.is_terms) {
              if(res.data.current_role_full_name=='Outlet Manager') {    
                localStorage.setItem("access_token",res.data.access_token);
                localStorage.setItem("current_role_short_name",res.data.current_role_short_name);
                localStorage.setItem("current_role_full_name",res.data.current_role_full_name); 
                localStorage.setItem("userlogin","userlogin");     
                this.navCtrl.setRoot(DashboardPage);  
                return;
               }
               if(res.data.current_role_full_name=='Mystery Shopper') {  
                 localStorage.setItem("access_token",res.data.access_token);
                 localStorage.setItem("current_role_short_name",res.data.current_role_short_name);
                 localStorage.setItem("current_role_full_name",res.data.current_role_full_name);
                 localStorage.setItem("userlogin","userlogin");     
                 this.navCtrl.push(Mysteryshopper1Page); 
               }
            }
          else { 
            localStorage.setItem("access_token",res.data.access_token);
            localStorage.setItem("current_role_short_name",res.data.current_role_short_name);
            localStorage.setItem("current_role_full_name",res.data.current_role_full_name);
            this.navCtrl.push(MoredetailsPage); 
            return;
          }
        }
        else { 
          this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();
          return; 
        }
        }
       }
  }, err => { 
    this.buttonDisabled=false; 
    if(err.status==422) {   
      this.toastController.create({ message: JSON.parse(err._body).data.message, duration: 5000, position: 'bottom' }).present();
      return; 
    } 
}); 
}

gotoNextField(nextElement) { 
  if(this.MoveForward) {
    nextElement.setFocus();
  }
}

finishFunction(evt) {
    if(evt.which==13) {
      this.verifybtn();
    }
}

MobCheck(vale) { 
  if(vale.length==1) {
    return false;
  }
}

MobCheck1(vale,evt) {  
  if(evt.keyCode==46) {
    this.MoveForward=false;
    return false;
  }
  if(evt.key=='Unidentified') {
    this.MoveForward=false;
    return false;
  }
  if(evt.which==229) {
    this.MoveForward=false;
    return false;
  }
  this.MoveForward=true; 
  if(vale.length==1) {
    return false;
  }
}


}