import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Mysteryshopper5Page } from '../mysteryshopper5/mysteryshopper5';

/**
 * Generated class for the Mysteryshopper4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper4',
  templateUrl: 'mysteryshopper4.html',
})
export class Mysteryshopper4Page {
  items: any =  { 'id': 0, 'rating': 0, 'name': 'Creativity', 'company': 'PROFLEX' }
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  NextBtn() {
    this.navCtrl.push(Mysteryshopper5Page);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Mysteryshopper4Page');
  }

}
