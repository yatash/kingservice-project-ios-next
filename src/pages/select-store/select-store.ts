import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VisitDetailsPage } from '../visit-details/visit-details'; 

/**
 * Generated class for the SelectStorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-store',
  templateUrl: 'select-store.html',
})
export class SelectStorePage {
items=[{heading:"Dmart Grocery Stores, Govindpuri circle, Dadar",date:"Wednesday, 13:45 Hrs"},{heading:"Dmart Grocery Stores, Govindpuri circle, Dadar",date:"Wednesday, 13:45 Hrs"},
{heading:"Dmart Grocery Stores, Govindpuri circle, Dadar",date:"Wednesday, 13:45 Hrs"},{heading:"Dmart Grocery Stores, Govindpuri circle, Dadar",date:"Wednesday, 13:45 Hrs"},
{heading:"Dmart Grocery Stores, Govindpuri circle, Dadar",date:"Wednesday, 13:45 Hrs"},{heading:"Dmart Grocery Stores, Govindpuri circle, Dadar",date:"Wednesday, 13:45 Hrs"},
{heading:"Dmart Grocery Stores, Govindpuri circle, Dadar",date:"Wednesday, 13:45 Hrs"}]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  BackBtn() { 
    this.navCtrl.setRoot(VisitDetailsPage);  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectStorePage');
  }

}
