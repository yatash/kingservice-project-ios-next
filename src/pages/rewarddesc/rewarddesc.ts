import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ModalController  } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { RewardfilterPage } from '../rewardfilter/rewardfilter'; 
import { EgiftvoucherPage } from '../egiftvoucher/egiftvoucher';
import { RewardscartPage } from '../rewardscart/rewardscart';
import { RewardstorePage } from '../rewardstore/rewardstore'; 

/**
 * Generated class for the RewarddescPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rewarddesc',
  templateUrl: 'rewarddesc.html',
})
export class RewarddescPage {
  itemlist:any=[]; 
  CategoryID:any;
  categorylist:any;
  category_name:any;
  ImageStatus:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider,public actionSheetCtrl: ActionSheetController, public modalCtrl:ModalController) {  
    this.itemlist=[];  
    this.categorylist=this.navParams.get("categorylist");
    console.log("this.categorylist==",this.categorylist);
    this.CategoryID=this.categorylist.id;
    this.category_name=this.categorylist.category_name;
    this.security.productlistbycategory(this.CategoryID).subscribe(res => { 
      if(res.status==200) {  
        if(res.success) {
        this.itemlist=res.data;  
        return;  
        }    
      } 
    }, err => {
      console.error("err==",err); 
    }); 

  }

  cartBtn(){
    this.navCtrl.push(RewardscartPage);
  }
  filterBtn() { 
    let modal=this.modalCtrl.create(RewardfilterPage); 
    modal.onDidDismiss((views) => { 
      if(views[0].status) {
        this.ImageStatus=true;
        this.itemlist=views[0].product_lists;
      }         
    });  
    modal.present();
  }
  productdetail(productID) { 
    this.navCtrl.push(EgiftvoucherPage,{productID:productID});
  }
  SortBtn() {
    let modal=this.modalCtrl.create(RewardstorePage,{itemlist:this.itemlist});  
    modal.onDidDismiss((views) => { 
      if(views[0].status) {
        this.itemlist=views[0].SortArr;
      } 
    });  
    modal.present();
  }
  CloseBtn() {   
    this.navCtrl.pop(); 
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RewarddescPage');
  }
}