import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChillerputtyPage } from './chillerputty';

@NgModule({
  declarations: [
    ChillerputtyPage,
  ],
  imports: [
    IonicPageModule.forChild(ChillerputtyPage),
  ],
})
export class ChillerputtyPageModule {}
