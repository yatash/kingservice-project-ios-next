import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the Mysteryshopper7Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysteryshopper7',
  templateUrl: 'mysteryshopper7.html',
})
export class Mysteryshopper7Page {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {

  }

  NextBtn() {       
    this.viewCtrl.dismiss(true);
  }

  noBtn() {  
    this.viewCtrl.dismiss(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Mysteryshopper7Page');
  }

}
