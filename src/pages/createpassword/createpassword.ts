import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { SecurityProvider } from '../../providers/security/security';
import { MobilenoPage } from '../mobileno/mobileno'; 
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the CreatepasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-createpassword',
  templateUrl: 'createpassword.html',
})
export class CreatepasswordPage {
  program_id
  newpass
  confirmpass
  token
  user_id
  loginotpcode
  newpasslogin
  constructor(public navCtrl: NavController, public navParams: NavParams,public security:SecurityProvider, public toastController : ToastController,public event:Events) {
    this.program_id=this.navParams.get("program_id");
    this.token=this.navParams.get("access_token");
    this.user_id=this.navParams.get("user_id"); 
    this.loginotpcode=this.navParams.get("loginotpcode"); 
  }

  SaveBtn()  { 
    if(this.newpass == "" || this.newpass == undefined) {
      this.toastController.create({ message: `Password is required.`, duration: 6000, position: 'bottom' }).present(); return;
    } 
    if(this.confirmpass == "" || this.confirmpass == undefined) {
      this.toastController.create({ message: `Conform password is required.`, duration: 6000, position: 'bottom' }).present(); return;
    } 
    if(this.confirmpass != this.newpass) {
      this.toastController.create({ message: `Those passwords didn't match. Try again.`, duration: 6000, position: 'bottom' }).present(); return;
    } 
    this.security.SetPin(this.program_id,this.newpass,this.token).subscribe(res =>{ 
      if(res.status==200) { 
        if(res.success) {
            this.toastController.create({ message: "Password saved successfully.", duration: 5000, position: 'bottom' }).present();
            this.navCtrl.push(MobilenoPage,{program_id:this.program_id,access_token:this.token,user_id:this.user_id });
            return;
        }
       }
       else {
        this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();
        return;
       }
  }, err => { 
      console.error("err==",err); 
}); 
}

SaveBtnLogin()  { 
  if(this.newpasslogin == "" || this.newpasslogin == undefined) {
    this.toastController.create({ message: `Password is required.`, duration: 6000, position: 'bottom' }).present(); return;
  } 
  this.security.VerifyPin(this.newpasslogin).subscribe(res =>{  
    if(res.status==200) { 
      if(res.success) {
        localStorage.setItem("access_token",res.data.access_token);
        if(res.data.check_supervisor==false) {
          this.navCtrl.push(MobilenoPage,{program_id:this.program_id,access_token:this.token,user_id:this.user_id }); 
          return;
        }
        this.event.publish('user:created',res.data.default_lang,Date.now());
        localStorage.setItem("applanguage",res.data.default_lang);  
        localStorage.setItem("userlogin","userlogin");    
        this.toastController.create({ message: "You have been successfully logged in.", duration: 5000, position: 'bottom' }).present();
        this.navCtrl.setRoot(TabsPage);  
        return;
      }
     }
     else {
      this.toastController.create({ message: res.data.message, duration: 5000, position: 'bottom' }).present();
      return;
     }
}, err => { 
    console.error("err==",err); 
}); 
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatepasswordPage');
  }

}
